package com.thril.mdm;

import android.app.Activity;
import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.thril.mdm.Network.utils.Constants;
import com.thril.mdm.common.BasicImplementation;

/**
 * Created by Ram on 1/30/2018.
 */

//Base Activity class
public abstract class BaseActivity extends AppCompatActivity {


    public ImageView three_dot;
    public ImageView toolbar_logo;
    public TextView toolbar_textview;
    public LinearLayout layout_three_dot_menu;
    public LinearLayout layout_notification;
    public TextView tv_notification_count;

    public RelativeLayout toolbar;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Constants.setmContext(this);
        //startDeviceUsagePendingIntent();
        MultiDex.install(this);
        Log.d("BaseActivity", "onCreate()");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View decorView = getWindow().getDecorView();
// Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }


//method to prevent the status bar of device
    public static void preventStatusBarExpansion(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

        } else {
            WindowManager manager = ((WindowManager) context.getApplicationContext()
                    .getSystemService(Context.WINDOW_SERVICE));

            Activity activity = (Activity) context;
            WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
            localLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
            localLayoutParams.gravity = Gravity.TOP;
            localLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |

                    // this is to enable the notification to recieve touch events
                    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |

                    // Draws over status bar
                    WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;

            localLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            //https://stackoverflow.com/questions/1016896/get-screen-dimensions-in-pixels
            int resId = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
            int result = 0;
            if (resId > 0) {
                result = activity.getResources().getDimensionPixelSize(resId);
            }

            localLayoutParams.height = result;

            localLayoutParams.format = PixelFormat.TRANSPARENT;

            CustomViewGroup view = new CustomViewGroup(context);

            manager.addView(view, localLayoutParams);
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        BasicImplementation.cancelDeviceUsageAlarm(this);
        BasicImplementation.cancelServiceCallAlarm(this);
        BasicImplementation.cancelStopSettingService(this);
    }

    public static class CustomViewGroup extends ViewGroup {
        public CustomViewGroup(Context context) {
            super(context);
        }

        @Override
        protected void onLayout(boolean changed, int l, int t, int r, int b) {
        }

        @Override
        public boolean onInterceptTouchEvent(MotionEvent ev) {
            // Intercepted touch!
            return true;
        }
    }

   /* @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (!hasFocus) {
            if(frgndFlag){
                Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
                sendBroadcast(closeDialog);
            }

        }
    }*/


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Log.d("button", new Integer(keyCode).toString());
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        } else if ((keyCode == KeyEvent.KEYCODE_CALL)) {
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_SEARCH) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    //method to insitlaize tool bar
    public void setupToolBar(String title) {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            getSupportActionBar().hide();
        }
//
        toolbar = (RelativeLayout) findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);
        three_dot = (ImageView) findViewById(R.id.iv_three_dot_menu);
        toolbar_logo = (ImageView) findViewById(R.id.toolbar_logo);
        toolbar_textview = (TextView) findViewById(R.id.toolbar_textview);
        layout_three_dot_menu = (LinearLayout) findViewById(R.id.three_dot_layout);
        layout_notification = (LinearLayout) findViewById(R.id.notification_layout);
        tv_notification_count = (TextView) findViewById(R.id.tv_notification_count);
    }


    public void updateCount(int count) {
        tv_notification_count.setText("" + count);
    }
}
