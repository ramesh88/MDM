package com.thril.mdm.kiosk;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.github.clans.fab.FloatingActionButton;
import com.thril.mdm.BaseActivity;
import com.thril.mdm.BuildConfig;
import com.thril.mdm.Enum.ConfigurationType;
import com.thril.mdm.Enum.SwitchType;
import com.thril.mdm.Global.ApkDownloader;
import com.thril.mdm.Global.DateUtils;
import com.thril.mdm.Global.DeviceInfo;
import com.thril.mdm.Global.MDMSettings;
import com.thril.mdm.Network.utils.Constants;
import com.thril.mdm.PieProgressDrawable;
import com.thril.mdm.Presenter.AppPresenter;
import com.thril.mdm.Presenter.GpsPresenter;
import com.thril.mdm.Presenter.WifiPresenter;
import com.thril.mdm.R;
import com.thril.mdm.adapter.NotificationListAdapter;
import com.thril.mdm.adapter.PagerAdapter;
import com.thril.mdm.adapter.SpinnerAdapter;
import com.thril.mdm.common.BasicImplementation;
import com.thril.mdm.customviews.MyAlertDialog;
import com.thril.mdm.databaselayer.contractor.Contractor;
import com.thril.mdm.databaselayer.databasemanager.DatabaseMgr;
import com.thril.mdm.model.App;
import com.thril.mdm.model.AppConfiguration;
import com.thril.mdm.model.AppUsage;
import com.thril.mdm.model.DeviceUsage;
import com.thril.mdm.model.NotificationData;
import com.thril.mdm.model.ResponseData;
import com.thril.mdm.model.StaffInfo;
import com.thril.mdm.model.UserControl;
import com.thril.mdm.service.AppStartStopService;
import com.thril.mdm.service.StopSettingsService;
import com.thril.mdm.view.AppView;
import com.thril.mdm.view.fragments.AppListPage1;
import com.thril.mdm.view.fragments.AppListPage2;
import com.thril.mdm.view.fragments.NewContact;
import com.thril.mdm.view.fragments.OpenDialpad;
import com.thril.mdm.view.fragments.PhoneContacts;

import io.fabric.sdk.android.Fabric;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Ram on 2/3/2018.
 */

//Main activity of the application
public class MainActivity extends BaseActivity implements AppView, View.OnClickListener, SeekBar.OnSeekBarChangeListener {


    private static final String TAG = "MainActivity";

    private static String mFilePath = "";
    private AppConfiguration appConfiguration;
    private AppPresenter mAppPresenter = null;
    private GpsPresenter mGpsPresenter = null;
    private WifiPresenter mWifiPresenter = null;
    private ResponseData responseData;
    private UserControl userControl;

    private TextView tv_send_label, tv_recieve_label, tv_app_data, tv_app_files, tv_progress_update, tv_password, tv_connection_status;
    private TextView tv_imei_number, tv_network_label, tv_asset_verify, toolbar_textview, tv_hotspot_name;
    private TextView tv_staff_name, tv_staff_other_info, tv_sim2_number, tv_sim1_number, tv_round_1, tv_round_2;
    private TextView tv_device_identifier, tv_about_staff_name, tv_staff_details, tv_reg_date, tv_version, tv_ph_no, tv_email_id, tv_custm_label, tv_device_details_label, tv_device_ph_num;

    private EditText wifi_spinner, gps_mode_spinner, et_username, et_password, et_asset_id, editText_OTP;

    private ImageView btn_gps_location, btn_mobile_data, btn_areo_plane, btn_flash_light, btn_auto_rotate, btn_blue_tooth;
    private ImageView btn_brightness, btn_ringer_volume, btn_media_volume, btn_wifi_network, btn_gps_mode, iv_delete_all;
    private ImageView timeProgress, btn_send, btn_receive, tool_bar_logo;

    private Button btn_connect, btn_transfer, btn_register, btn_exit;

    private Spinner spinner_app_info, spinner_discovry_list;

    private SeekBar seekbar_brightness, seekbar_ringer_volume, seekbar_media_volume;

    private RelativeLayout rel_settings, rel_data_beam, rel_asset_info, rel_contact_view, rel_about_device, rel_notification_data, rl_app_list;
    private LinearLayout layout_staff, layout_staff_other, wifi_layout, ll_dots;

    private List<String> discoveryList, app_files_list;
    private List<android.net.wifi.ScanResult> scanResultList;
    private List<NotificationData> notificationDataList;

    private ArrayAdapter<String> discoveryArray, app_files_data_array;


    private View view_app_data, view_app_files;

    private RecyclerView notification_recycler_view;
    private PieProgressDrawable pieProgressDrawable;
    private ProgressBar mRotateProgressBar;

    private Dialog dialog, exit_dialog;
    private ViewPager view_pager;
    private CheckBox cb_verify;

    private FloatingActionButton main_fab_button;
    private BroadcastUpdates broadcastUpdates;

    private boolean wifiConnectionFlag, back_press_flag = false, initial_flag = false;
    private int seek_bar_progress = 0, index = 0, screen_navigation = 0, scanPosition = 0, check_index = 0;


    private static MainActivity inst;

    private NotificationManager notificationManager;


    //method to create instance for main atcivity
    public static MainActivity instance() {
        return inst;
    }

    @Override // overriden method of activity
    public void onStart() {
        super.onStart();
        inst = this;
    }


    @Override // overriden method of activity with parameter intent
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.e("onNewIntent", "inside");
    }

    //method to hide keybaord
    public void hideSoftKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }


    @Override
    // overriden method of activity with parameter configuration and any configuation changes will be logged here
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Log.d(TAG, "Configuration Landscape");
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Log.d(TAG, "Configuration Portrait");
        }
    }

    // overriden method of activity
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "inside");
        Fabric.with(this, new Crashlytics());
        setupToolBar("");
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        viewInitialization();
        checkInitialPermission();
    }

    // overriden method of activity
    @Override
    protected void onResume() {

        Constants.frgndFlag = true;
        mAppPresenter.getNotificationCount();
        if (MDMSettings.appPackageFlag) {
            stopService(new Intent(this, AppStartStopService.class));
            MDMSettings.appPackageFlag = false;
            MDMSettings.appPackage = "";
            Constants.app_stop_date_time = Long.toString(DateUtils.getCurrentDateGMT());
            if (!Constants.app_start_date_time.equals("") && !Constants.app_stop_date_time.equals("")) {
                AppUsage appUsage = new AppUsage();
                appUsage.setAppID(MDMSettings.appId);
                appUsage.setAppStartDateTime(Constants.app_start_date_time);
                appUsage.setAppStopDateTime(Constants.app_stop_date_time);
                mAppPresenter.saveAppUsage(appUsage);
            }

        }
        super.onResume();
    }

    // overriden method of activity
    @Override
    protected void onPause() {
        super.onPause();
        Log.e("frgndFlag in pause", String.valueOf(Constants.frgndFlag));
        ActivityManager activityManager = (ActivityManager) getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);

        if (Constants.frgndFlag && !Constants.phoneCallFlag)
            activityManager.moveTaskToFront(getTaskId(), 0);
    }

    //Alert dialog method for device identifier verification
    public void showAlertDialog() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(Constants.getmContext(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(Constants.getmContext());
        }
        builder.setTitle("Device Identifier")
                .setMessage("Are you sure you want to verify the device identifier?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        check_index = 0;
                        mAppPresenter.verifyAssetIdentifier(et_asset_id.getText().toString().trim(), new DeviceInfo().getDeviceIMEI(MainActivity.this), BuildConfig.VERSION_NAME, check_index);

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    //Dialog method to exit form application i.e. otp screen
    public void MDMUnlock() {

        exit_dialog = new Dialog(this);
        exit_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        exit_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        exit_dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        View view = LayoutInflater.from(this).inflate(R.layout.exitdialog, null);
        exit_dialog.setContentView(view);
        exit_dialog.setCanceledOnTouchOutside(false);
        exit_dialog.setCancelable(false);

        editText_OTP = ((EditText) view
                .findViewById(R.id.exitpassword));
        editText_OTP.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editText_OTP.getText().toString().length() > 0)
                    editText_OTP.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        final TextView tv_cancel = (TextView) view.findViewById(R.id.tv_cancel);
        tv_cancel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view == tv_cancel) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        exit_dialog.cancel();
                    }
                }
                return true;
            }
        });

        final TextView tv_ok = (TextView) view.findViewById(R.id.tv_ok);
        tv_ok.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view == tv_ok) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        if (editText_OTP.getText().toString().length() == 0) {
                            editText_OTP.setError("Please enter OTP");
                            editText_OTP.requestFocusFromTouch();
                            editText_OTP.requestFocus();
                        } else {
                            mAppPresenter.verifyOTP(MDMSettings.getAssetIdentifierFromPreference(), editText_OTP.getText().toString());
                            //   }
                        }
                    }
                }
                return true;
            }
        });
        exit_dialog.show();
    }

    //method to initialize setitngs view
    public void setUpSettingsViews() {

        btn_gps_location = (ImageView) findViewById(R.id.gps_location);
        btn_gps_location.setOnClickListener(this);
        btn_mobile_data = (ImageView) findViewById(R.id.mobile_data);
        btn_mobile_data.setOnClickListener(this);
        btn_areo_plane = (ImageView) findViewById(R.id.aero_plane);
        btn_areo_plane.setOnClickListener(this);
        btn_flash_light = (ImageView) findViewById(R.id.flash_light);
        btn_flash_light.setOnClickListener(this);
        btn_auto_rotate = (ImageView) findViewById(R.id.auto_rotate);
        btn_auto_rotate.setOnClickListener(this);
        btn_blue_tooth = (ImageView) findViewById(R.id.bluetooth);
        btn_blue_tooth.setOnClickListener(this);
        btn_brightness = (ImageView) findViewById(R.id.brightness);
        btn_brightness.setOnClickListener(this);
        btn_ringer_volume = (ImageView) findViewById(R.id.ringer);
        btn_ringer_volume.setOnClickListener(this);
        btn_media_volume = (ImageView) findViewById(R.id.media);
        btn_media_volume.setOnClickListener(this);
        btn_wifi_network = (ImageView) findViewById(R.id.wifi);
        btn_wifi_network.setOnClickListener(this);
        btn_gps_mode = (ImageView) findViewById(R.id.gps_mode);
        btn_gps_mode.setOnClickListener(this);
        btn_connect = (Button) findViewById(R.id.wifi_connect);
        btn_connect.setOnClickListener(this);
        wifi_spinner = (EditText) findViewById(R.id.wifi_credential_spinner);
        tv_network_label = (TextView) findViewById(R.id.tv_network_label);
        wifi_spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    index = 1;
                    if (MDMSettings.isWifiEnabled(MainActivity.this)) {
                        wifiConnectionFlag = false;
                        mAppPresenter.displayProgressDialog("Please wait...!", "Searching wifi networks...");
                        mWifiPresenter.scanHotSpots(true);
                    }

                }
                return true;
            }
        });


        gps_mode_spinner = (EditText) findViewById(R.id.gps_mode_spinner);

        gps_mode_spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    index = 2;
                  /*  List<String> network = new ArrayList<>();
                    network.add("Gps");
                    network.add("Network");
                    showSpinnerselectionDialog("Select GPS Mode", network);*/
                }
                return true;
            }
        });
        et_username = (EditText) findViewById(R.id.username);
        et_password = (EditText) findViewById(R.id.password);

        seekbar_brightness = (SeekBar) findViewById(R.id.seekbar_brightness);
        seekbar_brightness.setOnSeekBarChangeListener(this);
        MDMSettings.setKeyValue(this, MDMSettings.KEY_BRIGHTNESS, MDMSettings.getDefaultBrightness(this));
        seekbar_brightness.setProgress(MDMSettings.getKeyValue(this, MDMSettings.KEY_BRIGHTNESS));
        seekbar_ringer_volume = (SeekBar) findViewById(R.id.seekbar_ringer);
        MDMSettings.setKeyValue(this, MDMSettings.KEY_RINGER, MDMSettings.getDefalutRingerVolume(this));
        seekbar_ringer_volume.setMax(MDMSettings.getMaxRingerVolume(this));
        seekbar_ringer_volume.setProgress(MDMSettings.getKeyValue(this, MDMSettings.KEY_RINGER));
        seekbar_ringer_volume.setOnSeekBarChangeListener(this);
        seekbar_media_volume = (SeekBar) findViewById(R.id.seekbar_media);
        MDMSettings.setKeyValue(this, MDMSettings.KEY_MEDIA, MDMSettings.getDefalutMediaVolume(this));
        seekbar_media_volume.setMax(MDMSettings.getMaxMediaVolume(this));
        seekbar_media_volume.setProgress(MDMSettings.getKeyValue(this, MDMSettings.KEY_MEDIA));
        seekbar_media_volume.setOnSeekBarChangeListener(this);

    }

    //method to exit from app directly without otp
    public void directMDMUnlock() {
        Intent selector = new Intent(
                "android.intent.action.MAIN");
        selector.addCategory("android.intent.category.HOME");
        selector.setComponent(new ComponentName(
                "android",
                "com.android.internal.app.ResolverActivity"));
        startActivity(selector);
        // finish this activity
        finish();

    }


    //method to create actions for settings icons
    public void activateOverflowMenu() {
        layout_three_dot_menu.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                Context wrapper = new ContextThemeWrapper(MainActivity.this, R.style.PopupMenu);
                PopupMenu popupMenu = new PopupMenu(wrapper, view);

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {

                            case R.id.item_check_update:
                                if (Constants.isNetworkAvailable(MainActivity.this)) {
                                    check_index = 1;
                                    mAppPresenter.verifyAssetIdentifier(MDMSettings.getAssetIdentifierFromPreference().trim(), new DeviceInfo().getDeviceIMEI(MainActivity.this), BuildConfig.VERSION_NAME, check_index);
                                } else {
                                    Toast.makeText(MainActivity.this, "Please check your internet", Toast.LENGTH_SHORT).show();
                                }

                                break;

                            case R.id.item_exit_static:
                                directMDMUnlock();
                                break;
                            case R.id.item_exit:
                                layout_notification.setVisibility(View.VISIBLE);
                                iv_delete_all.setVisibility(View.GONE);
                                mAppPresenter.sendOTP(MDMSettings.getAssetIdentifierFromPreference());
                                // MDMUnlock();
                                break;
                            case R.id.item_settings:
                                layout_notification.setVisibility(View.VISIBLE);
                                iv_delete_all.setVisibility(View.GONE);
                                back_press_flag = true;
                                screen_navigation = 1;
                                if (rel_data_beam.getVisibility() == View.VISIBLE) {
                                    rel_data_beam.setVisibility(View.GONE);
                                }
                                if (rl_app_list.getVisibility() == View.VISIBLE) {
                                    rl_app_list.setVisibility(View.GONE);
                                }
                                if (rel_contact_view.getVisibility() == View.VISIBLE) {
                                    rel_contact_view.setVisibility(View.GONE);
                                }
                                if (rel_about_device.getVisibility() == View.VISIBLE) {
                                    rel_about_device.setVisibility(View.GONE);
                                }
                                if (ll_dots.getVisibility() == View.VISIBLE) {
                                    ll_dots.setVisibility(View.GONE);
                                }

                                if (rel_notification_data != null && (rel_notification_data.getVisibility() == View.VISIBLE)) {
                                    rel_notification_data.setVisibility(View.GONE);
                                }

                                if (rel_settings.getVisibility() == View.GONE) {
                                    rel_settings.setVisibility(View.VISIBLE);
                                    if (MDMSettings.isWifiEnabled(MainActivity.this)) {
                                        wifiConnectionFlag = true;
                                        mWifiPresenter.scanHotSpots(true);
                                    }
                                    displayMobileDataContent(appConfiguration, userControl);

                                }
                                toolbar_textview.setText(item.getTitle());
                                tool_bar_logo.setImageResource(R.drawable.bac_arrow);

                                break;
                            case R.id.item_data_beam:
                                layout_notification.setVisibility(View.VISIBLE);
                                iv_delete_all.setVisibility(View.GONE);
                                back_press_flag = true;
                                screen_navigation = 2;
                                if (rl_app_list.getVisibility() == View.VISIBLE) {
                                    rl_app_list.setVisibility(View.GONE);
                                }
                                if (rel_settings.getVisibility() == View.VISIBLE) {
                                    rel_settings.setVisibility(View.GONE);
                                }
                                if (rel_notification_data != null && (rel_notification_data.getVisibility() == View.VISIBLE)) {
                                    rel_notification_data.setVisibility(View.GONE);
                                }
                                if (rel_contact_view.getVisibility() == View.VISIBLE) {
                                    rel_contact_view.setVisibility(View.GONE);
                                }
                                if (rel_about_device.getVisibility() == View.VISIBLE) {
                                    rel_about_device.setVisibility(View.GONE);
                                }
                                if (ll_dots.getVisibility() == View.VISIBLE) {
                                    ll_dots.setVisibility(View.GONE);
                                }
                                if (rel_data_beam.getVisibility() == View.GONE) {
                                    rel_data_beam.setVisibility(View.VISIBLE);
                                    dataBeamScreenInitilization();
                                    dataBeamProgressBar();
                                }

                                toolbar_textview.setText(item.getTitle());
                                tool_bar_logo.setImageResource(R.drawable.bac_arrow);
                                break;

                            case R.id.item_about_device:
                                layout_notification.setVisibility(View.VISIBLE);
                                iv_delete_all.setVisibility(View.GONE);
                                back_press_flag = true;
                                screen_navigation = 5;
                                if (rl_app_list.getVisibility() == View.VISIBLE) {
                                    rl_app_list.setVisibility(View.GONE);
                                }
                                if (rel_settings.getVisibility() == View.VISIBLE) {
                                    rel_settings.setVisibility(View.GONE);
                                }
                                if (rel_notification_data != null && (rel_notification_data.getVisibility() == View.VISIBLE)) {
                                    rel_notification_data.setVisibility(View.GONE);
                                }
                                if (rel_contact_view.getVisibility() == View.VISIBLE) {
                                    rel_contact_view.setVisibility(View.GONE);
                                }
                                if (rel_data_beam.getVisibility() == View.VISIBLE) {
                                    rel_data_beam.setVisibility(View.GONE);
                                }
                                if (ll_dots.getVisibility() == View.VISIBLE) {
                                    ll_dots.setVisibility(View.GONE);
                                }
                                if (rel_about_device.getVisibility() == View.GONE) {
                                    rel_about_device.setVisibility(View.VISIBLE);
                                }
                                setAboutdevice();
                                toolbar_textview.setText(item.getTitle());
                                tool_bar_logo.setImageResource(R.drawable.bac_arrow);
                                break;

                            /*case R.id.item_shut_down:
                                Constants.Device_Switch_Off_Date_Time = Long.toString(DateUtils.getCurrentDateGMT());
                                saveDeviceUsage();
                                break;
                            case R.id.item_restart:
                                Constants.Device_Switch_Off_Date_Time = Long.toString(DateUtils.getCurrentDateGMT());
                                saveDeviceUsage();
                                break;*/
                            case R.id.item_screenshot:
                                ApkDownloader.captureScreen(MainActivity.this);
                                break;
                        }


                        return true;
                    }
                });


                popupMenu.inflate(R.menu.mainmenu);
                popupMenu.show();
                return false;
            }
        });

    }

    //method to set about device details
    public void setAboutdevice() {
        tv_device_identifier.setText(MDMSettings.getAssetIdentifierFromPreference());
        tv_about_staff_name.setText(MDMSettings.getStaffName());
        tv_staff_details.setText(MDMSettings.getStaffOtherInfo());
        tv_reg_date.setText(MDMSettings.getRegDate());
        tv_version.setText(BuildConfig.VERSION_NAME);
        tv_device_details_label.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        tv_custm_label.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        if (appConfiguration != null) {
            tv_ph_no.setText(appConfiguration.getCommandCenterPhNo());
            tv_email_id.setText(appConfiguration.getCommandcenteremail());
            tv_device_ph_num.setText(appConfiguration.getDevicePhoneNumber());
        }


    }

    //method to set gps mode
    public void setGpsMode() {
        if (appConfiguration != null) {
            Constants.gps_mode = appConfiguration.getGpsMode();
        }
    }

    @Override
    //over riden method of Appview interface with parameter of responseData from service and loading paginations of apps
    public void onAppRegistrationSuccess(ResponseData responseData) {

        if (rel_asset_info != null && rel_asset_info.getVisibility() == View.VISIBLE) {

            MDMSettings.saveAssetIdentifier(et_asset_id.getText().toString());
            rel_asset_info.setVisibility(View.GONE);
            showRegistrationSuccessAlertDialog();

        }
        MDMSettings.saveRegistrationDate(Constants.getCurrentDateToString());
        rl_app_list.setVisibility(View.VISIBLE);
        ll_dots.setVisibility(View.VISIBLE);
        toolbar.setVisibility(View.VISIBLE);
        this.responseData = responseData;
        stopService(new Intent(this, StopSettingsService.class));
        startService(new Intent(this, StopSettingsService.class));
//        AppListPage1.responseData = responseData;
//        AppListPage2.responseData = responseData;
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager());

        adapter.addFrag(new AppListPage1(), "");
        adapter.addFrag(new AppListPage2(), "");
        view_pager.setAdapter(adapter);
        view_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position == 0) {
                    tv_round_1.setBackgroundResource(R.drawable.black_round_background);
                    tv_round_2.setBackgroundResource(R.drawable.white_round_background);
                } else if (position == 1) {
                    tv_round_1.setBackgroundResource(R.drawable.white_round_background);
                    tv_round_2.setBackgroundResource(R.drawable.black_round_background);
                }

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

//        AppListAdapter appListAdapter = new AppListAdapter(MainActivity.this, responseData.getApp(), this);
//        recyclerView.setAdapter(appListAdapter);
        appConfiguration = responseData.getAppConfiguration();
        userControl = mAppPresenter.getUserControls();
        setGpsMode();
        if (!MDMSettings.checkGpsStatus(this)) {
            initial_flag = true;
            if (appConfiguration.getGps().equalsIgnoreCase(SwitchType.On.toString())) {
                mGpsPresenter.startGpsService(appConfiguration.getGpstimeinterval());
            }
            //MDMSettings.turnGPSOn(this);

        } else {
            initial_flag = false;
        }

        int syncTime = appConfiguration.getSyncTimeInterval();
        syncTime = syncTime - 2; // two minute before the appConfiguration time DeviceUsagePendingIntent will be called
        if (syncTime < 0)
            syncTime = 0;
        BasicImplementation.startDeviceUsagePendingIntent(syncTime, this);
        BasicImplementation.startServerPendingIntent(appConfiguration.getSyncTimeInterval(), this);
        enableIconBaseOnConfigurationData(appConfiguration);

        if (BasicImplementation.isGooglePlayServicesAvailable(this)) {
            if (!initial_flag) {
                initial_flag = true;
                if (appConfiguration.getGps().equalsIgnoreCase(SwitchType.On.toString())) {
                    mGpsPresenter.startGpsService(appConfiguration.getGpstimeinterval());
                }
            }
            // Toast.makeText(this, "Gps Enabled", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Google Play service not available", Toast.LENGTH_SHORT).show();
        }

    }


    //method to show alert dialog after successful registration
    public void showRegistrationSuccessAlertDialog() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(Constants.getmContext(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(Constants.getmContext());
        }
        builder.setTitle("Alert..!")
                .setMessage("First Time Set up Completed Successfully")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    //method to set app configuration to the settings page
    public void enableIconBaseOnConfigurationData(AppConfiguration configuration) {

        if (configuration.getGps().equalsIgnoreCase(SwitchType.On.toString())) {
            btn_gps_location.setImageResource(R.drawable.ic_place_white_24px);
            btn_gps_location.setTag(SwitchType.On.toString());
        } else {
            btn_gps_location.setImageResource(R.drawable.ic_place_black_24px);
            btn_gps_location.setTag(SwitchType.Off.toString());
        }

        if (configuration.getScreenOrientation().equalsIgnoreCase("Portrait")) {
            btn_auto_rotate.setImageResource(R.drawable.ic_screen_lock_rotation_black_24px);
            btn_auto_rotate.setTag(SwitchType.Off.toString());
            if (MDMSettings.getAutoOrientation(this)) {
                MDMSettings.setAutoOrientation(this, false);
            }

        } else {
            btn_auto_rotate.setTag(SwitchType.On.toString());
            btn_auto_rotate.setImageResource(R.drawable.ic_screen_rotation_white_24px);
            if (!MDMSettings.getAutoOrientation(this)) {
                MDMSettings.setAutoOrientation(this, true);
            }

            MDMSettings.setAutoOrientation(this, false);
        }
        if (configuration.getBluetooth().equalsIgnoreCase(SwitchType.On.toString())) {
            btn_blue_tooth.setImageResource(R.drawable.ic_bluetooth_white_24px);
            btn_blue_tooth.setTag(SwitchType.On.toString());
            if (!MDMSettings.isBTEnabled()) {
                MDMSettings.setBluetooth();
            }
        } else {
            if (MDMSettings.isBTEnabled()) {
                btn_blue_tooth.setImageResource(R.drawable.ic_bluetooth_white_24px);
                btn_blue_tooth.setTag(SwitchType.On.toString());
            } else {
                btn_blue_tooth.setImageResource(R.drawable.ic_bluetooth_disabled_black_24px);
                btn_blue_tooth.setTag(SwitchType.Off.toString());
                if (MDMSettings.isBTEnabled()) {
                    MDMSettings.setBluetooth();
                }
            }

        }


        if (configuration.getFlightMode().equalsIgnoreCase(SwitchType.On.toString())) {
            btn_areo_plane.setImageResource(R.drawable.ic_airplanemode_active_white_24px);
            btn_areo_plane.setTag(SwitchType.On.toString());
            if (!MDMSettings.isAirplaneMode(this)) {
                btn_areo_plane.setTag(SwitchType.Off.toString());
            } else {
                btn_mobile_data.setImageResource(R.drawable.import_export_black_24);
                btn_mobile_data.setTag(SwitchType.Off.toString());
                btn_wifi_network.setImageResource(R.drawable.ic_signal_wifi_off_black_24px);
                btn_wifi_network.setTag(SwitchType.Off.toString());
                tv_network_label.setText("No Network");
                btn_connect.setVisibility(View.GONE);
                if (wifi_layout.getVisibility() == View.VISIBLE) {
                    wifi_layout.setVisibility(View.INVISIBLE);
                }
            }
        } else {
            btn_areo_plane.setImageResource(R.drawable.ic_airplanemode_inactive_black_24px);
            btn_areo_plane.setTag(SwitchType.Off.toString());
            if (MDMSettings.isAirplaneMode(this)) {
                btn_areo_plane.setTag(SwitchType.On.toString());
                btn_mobile_data.setImageResource(R.drawable.import_export_black_24);
                btn_mobile_data.setTag(SwitchType.Off.toString());
                btn_wifi_network.setImageResource(R.drawable.ic_signal_wifi_off_black_24px);
                btn_wifi_network.setTag(SwitchType.Off.toString());
                tv_network_label.setText("No Network");
                btn_connect.setVisibility(View.GONE);
                if (wifi_layout.getVisibility() == View.VISIBLE) {
                    wifi_layout.setVisibility(View.INVISIBLE);
                }
            }
        }

        if (configuration.getFlightMode().equalsIgnoreCase(SwitchType.Off.toString())) {
            if (configuration.getWifi().equalsIgnoreCase(SwitchType.On.toString())) {
                btn_wifi_network.setImageResource(R.drawable.ic_network_wifi_white_24px);
                btn_wifi_network.setTag(SwitchType.On.toString());
                btn_connect.setVisibility(View.VISIBLE);
                if (!MDMSettings.isWifiEnabled(this)) {
                    MDMSettings.setWifi(this);
                    btn_connect.setVisibility(View.VISIBLE);
                }
                if (wifi_layout.getVisibility() == View.INVISIBLE) {
                    wifi_layout.setVisibility(View.VISIBLE);
                }
            } else {
                if (MDMSettings.isWifiEnabled(this)) {
                    btn_wifi_network.setImageResource(R.drawable.ic_network_wifi_white_24px);
                    btn_wifi_network.setTag(SwitchType.On.toString());
                    btn_connect.setVisibility(View.VISIBLE);
                    if (wifi_layout.getVisibility() == View.INVISIBLE) {
                        wifi_layout.setVisibility(View.VISIBLE);
                    }
                } else {
                    btn_wifi_network.setImageResource(R.drawable.ic_signal_wifi_off_black_24px);
                    btn_wifi_network.setTag(SwitchType.Off.toString());
                    tv_network_label.setText("No Network");
                    btn_connect.setVisibility(View.GONE);
                    if (MDMSettings.isWifiEnabled(this)) {
                        MDMSettings.setWifi(this);
                    }
                    if (wifi_layout.getVisibility() == View.VISIBLE) {
                        wifi_layout.setVisibility(View.INVISIBLE);
                    }
                }

            }
        }

        String flash = MDMSettings.getFlashValue(this, MDMSettings.KEY_FLASH_LIGHT);
        boolean hasFlash = this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        if (hasFlash) {
            if (flash.equalsIgnoreCase(SwitchType.On.toString())) {
                btn_flash_light.setImageResource(R.drawable.ic_flash_active_white24px);
                btn_flash_light.setTag(SwitchType.On.toString());
                MDMSettings.setFlashLight(true);
            } else {
                btn_flash_light.setImageResource(R.drawable.ic_flash_inactive_black_24px);
                btn_flash_light.setTag(SwitchType.Off.toString());
                MDMSettings.setFlashLight(false);
            }
        } else {
            btn_flash_light.setImageResource(R.drawable.ic_flash_inactive_black_24px);
            btn_flash_light.setTag(SwitchType.Off.toString());
        }

        btn_brightness.setImageResource(R.drawable.ic_brightness_medium_white_24px);
        btn_ringer_volume.setImageResource(R.drawable.ic_volume_up_white_24px);
        btn_media_volume.setImageResource(R.drawable.ic_volume_up_white_24px);
        if (configuration.getFlightMode().equalsIgnoreCase(SwitchType.Off.toString())) {
            if (configuration.getMobileData().equalsIgnoreCase(SwitchType.On.toString())) {

                if (!MDMSettings.isMobileDataEnable(this)) {
                    btn_mobile_data.setTag(SwitchType.Off.toString());
                    btn_mobile_data.setImageResource(R.drawable.import_export_black_24);
                    //MDMSettings.mobileDataSettings(this);
                } else {
                    btn_mobile_data.setImageResource(R.drawable.import_export_white_24);
                    btn_mobile_data.setTag(SwitchType.On.toString());
                }
            } else {

                if (MDMSettings.isMobileDataEnable(this)) {
                    btn_mobile_data.setTag(SwitchType.On.toString());
                    btn_mobile_data.setImageResource(R.drawable.import_export_white_24);
                    //MDMSettings.mobileDataSettings(this);
                } else {
                    btn_mobile_data.setImageResource(R.drawable.import_export_black_24);
                    btn_mobile_data.setTag(SwitchType.Off.toString());
                }
            }

        }

    }

    @Override //overriden method of appview which displays error message from service
    public void onAppRegistrationFailure(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override //overriden method of appview which make action of onclick of apps
    public void onAction(int position) {
        Log.d("onAction", "Clicked " + position);
        Constants.frgndFlag = false;
        List<App> app = responseData.getApp();
        String packageName = app.get(position).getPackageName();

        MDMSettings.appPackage = packageName;
        MDMSettings.appId = app.get(position).getAppid();
        String apkPath = app.get(position).getApkPath();
        String type = app.get(position).getType();
        Log.d("package name", "Clicked " + packageName);
        if (type.trim().equalsIgnoreCase("new") && ApkDownloader.appInstalledOrNot(this, packageName)) {
            Log.i(TAG, "Application is already installed.");
            ApkDownloader.launchApk(this, packageName);
        } else {
            if (apkPath.toLowerCase().indexOf("play") != -1) {
                if (Constants.isNetworkAvailable(this)) {
                    ApkDownloader.openPlaystore(this, packageName);
                } else {
                    Toast.makeText(this, "Please check your network" + position, Toast.LENGTH_LONG).show();
                }
            } else {
                String path = Constants.checkApkExist(apkPath.substring(apkPath.lastIndexOf("/") + 1).trim());
                Log.e("Path", path);
                if (!path.equals("")) {
                    ApkDownloader.installApk(this, path);
                } else {
                    if (Constants.isNetworkAvailable(this)) {

                        new ApkFileDownloader(apkPath, false, app.get(position).getTitle()).execute();


                    } else {
                        Toast.makeText(this, "Please check your network" + position, Toast.LENGTH_LONG).show();
                    }
                }
            }

        }
    }

    @Override  //overriden method of appview which make action of onclick of notification
    public void onNotificationAction(int position) {
        String title = notificationDataList.get(position).getTitle();
        String notificationContent = notificationDataList.get(position).getContent();
        showNotificationContent(title, notificationContent, this);
    }


    @Override  //overriden method of appview which show gps location
    public void gpsLocation(Location location) {
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        //  Toast.makeText(this, "Latitude :" + latitude + "\n" + "Longitude:" + longitude, Toast.LENGTH_SHORT).show();

    }

    @Override  //overriden method of appview to know status of gps service
    public void gpsServiceStatus(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

    }

    @Override //overriden method of appview to get enabled wifi list
    public void discoveryList(List<String> discoveryList, boolean flag, List<android.net.wifi.ScanResult> scanResultList) {
        Log.d("Discovery List", "Inside");
        this.discoveryList = discoveryList;
        this.scanResultList = scanResultList;
        if (discoveryArray != null && !flag) { // data beam
            discoveryArray.clear();
            discoveryArray.add("Select");
            for (String hotspotName : discoveryList) {
                Log.d("hotspot name", hotspotName);
                discoveryArray.add(hotspotName);
            }
        } else {// wifi list
            addToWifiSpinner(discoveryList);
        }
    }

    //method to add enabled wifi to the spinner item
    public void addToWifiSpinner(List<String> discoveryList) {

        if (mAppPresenter.mProgressDialog != null && mAppPresenter.mProgressDialog.isShowing()) {
            mAppPresenter.dismissProgressDialog();
        }
        if (!wifiConnectionFlag) {
            List<String> diStrings = new ArrayList<>();
            for (int i = 0; i < discoveryList.size(); i++) {
                if (discoveryList.get(i).trim().length() > 0) {
                    if (!diStrings.contains(discoveryList.get(i))) {
                        diStrings.add(discoveryList.get(i));
                    }
                }
            }
            showSpinnerselectionDialog("Select Network", diStrings);
        } else {
            mWifiPresenter.getConnectedWifi();
        }

    }


    @Override  //overriden method of appview to know name of selected wifi
    public void hotSpotName(final String hotSpotName, final String password) {
        Log.d(TAG, hotSpotName + " " + password + " " + MDMSettings.dataBeamFlag);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!MDMSettings.dataBeamFlag) {
                    tv_password.setText("Password:" + password);
                    tv_hotspot_name.setText("Current NetWork:" + hotSpotName);
                } else {
                    tv_password.setText("");
                }
            }
        });

    }

    @Override  //overriden method of appview to diable the hotspot
    public void disableHotspot(boolean flag) {

        if (flag) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivity.this, "Hot spot Disabled", Toast.LENGTH_SHORT).show();
                }
            });

        }
    }

    @Override  //overriden method of appview to know status of hotspot connecytion
    public void hopSpotConnectionStatus(final boolean flag) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (flag) {
                    Toast.makeText(MainActivity.this, "Hotspot Connected", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    @Override  //overriden method of appview to get list of files of MDM
    public void fileList(List<String> list) {
        app_files_list = list;
        app_files_data_array.clear();
        app_files_data_array.add("Select");
        for (String file : app_files_list) {
            String fileName = file.substring(file.lastIndexOf("/") + 1);
            app_files_data_array.add(fileName);
        }
    }

    @Override //overriden method of appview to get current location
    public void currentGpsLocation(Location location) {
        Toast.makeText(this, "Current Location \n" + location.getLatitude() + "-" + location.getLongitude(), Toast.LENGTH_SHORT).show();
    }

    @Override //overriden method of appview to know socket status of hotspot
    public void socketStatus(final String socketStatus, final boolean socketFlag) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (socketFlag) {
                    tv_connection_status.setText("Status : " + socketStatus);
                    btn_transfer.setEnabled(true);
                    btn_transfer.setBackgroundResource(R.drawable.bg_button);
                } else {
                    tv_connection_status.setText("Status : " + socketStatus);
                }
            }
        });
    }

    @Override //overriden method of appview to update the status of progress of file transfer
    public void updateProgress(final int percentage) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mRotateProgressBar != null && (mRotateProgressBar.getVisibility() == View.INVISIBLE)) {
                    mRotateProgressBar.setVisibility(View.VISIBLE);
                }
                setProgressToCircle(percentage);
            }
        });


    }

    @Override //overriden method of appview to display the status of tranferred file
    public void fileStatus(boolean flag) {
        if (flag) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivity.this, "File has been sent/received successfully", Toast.LENGTH_SHORT).show();

                    mRotateProgressBar.setVisibility(View.INVISIBLE);
                    pieProgressDrawable.setColor(ContextCompat.getColor(MainActivity.this, R.color.blue));
                    pieProgressDrawable.setCircleColor(ContextCompat.getColor(MainActivity.this, R.color.gray));
                    tv_progress_update = (TextView) findViewById(R.id.tv_update);
                    tv_progress_update.setText("0%");
                    setProgressToCircle(0);
                }
            });
        }
    }

    @Override
    //overriden method of appview to get the staff details after asset validation is success with server
    public void onAssetValidationSuccess(StaffInfo staffInfo) {
        if (check_index == 0) {
            Constants.asset_identifier = et_asset_id.getText().toString();
            MDMSettings.saveAssetIdentifier(Constants.asset_identifier);
            if (staffInfo.getStaffData() != null) {
                MDMSettings.saveStaffDetails(staffInfo.getStaffData().getStaffName(), staffInfo.getStaffData().getOtherInformation());
            }
            InputMethodManager imm = (InputMethodManager) MainActivity.this.getSystemService(MainActivity.this.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(et_asset_id.getWindowToken(), 0);

            tv_asset_verify.setText("Checked");
            tv_asset_verify.setTextColor(getResources().getColor(R.color.text_green_color));
            btn_register.setVisibility(View.VISIBLE);
            if (layout_staff.getVisibility() == View.GONE) {
                layout_staff.setVisibility(View.VISIBLE);
                tv_staff_name.setText(staffInfo.getStaffData().getStaffName());
            }
            if (layout_staff_other.getVisibility() == View.GONE) {
                layout_staff_other.setVisibility(View.VISIBLE);
                tv_staff_other_info.setText(staffInfo.getStaffData().getOtherInformation());
            }
        } else {
            Toast.makeText(this, "No updates found", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
//overriden method of appview to get the error message after asset validation is failure with server
    public void onAssetValidationFailure(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
        if (check_index == 0) {
            InputMethodManager imm = (InputMethodManager) MainActivity.this.getSystemService(MainActivity.this.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(et_asset_id.getWindowToken(), 0);

            cb_verify.setChecked(false);
            cb_verify.setBackgroundResource(android.R.drawable.checkbox_off_background);
            tv_asset_verify.setText("Check");
            tv_asset_verify.setTextColor(getResources().getColor(R.color.about_red));
        }

    }

    @Override //overriden method of appview to get the connected wifi name
    public void connectedWifi(String ssid) {
        if (wifi_spinner != null && wifiConnectionFlag) {
            wifi_spinner.setText(ssid);
            tv_network_label.setText(ssid);
            et_username.setText(ssid);
            et_password.setText("");

        }
    }

    @Override//overriden method of appview to get notification count
    public void notificationCount(int count) {
        //update UI
        updateCount(count);
    }

    @Override //overriden method of appview to get notification list
    public void notificationList(List<NotificationData> notificationDataList) {
        this.notificationDataList = notificationDataList;
    }

    @Override //overriden method of appview to delete particluar notifcation
    public void notificationDelete(int position) {
        NotificationData notificationData = notificationDataList.get(position);

        DeleteNotification(0, notificationData.getId());


    }

    //Alert dialog method to delete all notifications
    public void DeleteNotification(final int index, final String not_id) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(Constants.getmContext(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(Constants.getmContext());
        }
        String mesg = "";
        if (index == 0) {
            mesg = "Do you want to delete this message..?";
        } else {
            mesg = "Do you want to delete all messages..?";
        }
        builder.setTitle("Alert..!")
                .setMessage(mesg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        if (index == 0) {
                            mAppPresenter.deleteNotification(not_id);
                        } else {
                            mAppPresenter.deleteAllNotification();
                        }

                        mAppPresenter.getNotificationList();
                        mAppPresenter.getNotificationCount();

                        if (!notificationDataList.isEmpty()) {
                            NotificationListAdapter notificationListAdapter = new NotificationListAdapter(MainActivity.this, notificationDataList, MainActivity.this);
                            notification_recycler_view.setAdapter(notificationListAdapter);
                        } else {
                            layout_notification.setVisibility(View.VISIBLE);
                            iv_delete_all.setVisibility(View.GONE);
                            if (rel_notification_data.getVisibility() == View.VISIBLE) {
                                rel_notification_data.setVisibility(View.GONE);
                            }
                            if (rl_app_list.getVisibility() == View.GONE) {
                                rl_app_list.setVisibility(View.VISIBLE);
                            }
                            if (ll_dots.getVisibility() == View.GONE) {
                                ll_dots.setVisibility(View.VISIBLE);
                            }
                            back_press_flag = false;
                            toolbar_textview.setText("");
                            tool_bar_logo.setImageResource(R.drawable.toolbar_logo);
                        }
                        dialog.dismiss();


                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete

                        dialog.dismiss();


                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override //overriden method of appview to display update alert of apk if update found in server
    public void onDialogUpdateApk(String apkLink) {
        // open dialog to update the apk
        openUpdateDialog(apkLink);

    }

    @Override //overriden method of appview to shows exit dialog if otp is generated with server
    public void showExitDialog() {
        MDMUnlock();
    }

    @Override
    //overriden method of appview to exit from app after succesfull otp verification with server
    public void exitApp() {
        InputMethodManager imm = (InputMethodManager) MainActivity.this.getSystemService(MainActivity.this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText_OTP.getWindowToken(), 0);
        exit_dialog.cancel();
        Intent selector = new Intent(
                "android.intent.action.MAIN");
        selector.addCategory("android.intent.category.HOME");
        selector.setComponent(new ComponentName(
                "android",
                "com.android.internal.app.ResolverActivity"));
        startActivity(selector);
        // finish this activity
        finish();
    }

    @Override
    //overriden method of appview displays error message when otp sending failure with server
    public void onOTPSendingFailure(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    //overriden method of appview displays error message when otp verification failure with server
    public void onOTPVerifyFailure(String error) {
        editText_OTP.setError(error);
        editText_OTP.requestFocus();
        editText_OTP.requestFocusFromTouch();
        //Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }


    //method to check whether to display app list or registration
    public void appRegistration() {
        // super.appRegistration();
        if (rel_asset_info != null && rel_asset_info.getVisibility() == View.GONE && MDMSettings.getAssetIdentifierFromPreference() != null) {
            Constants.asset_identifier = MDMSettings.getAssetIdentifierFromPreference();
//            rl_app_list.setVisibility(View.VISIBLE);
//            toolbar.setVisibility(View.VISIBLE);
            mAppPresenter.getAppList();
        } else if ((rel_asset_info == null || rel_asset_info.getVisibility() == View.GONE) && MDMSettings.getAssetIdentifierFromPreference() != null) {
            Constants.asset_identifier = MDMSettings.getAssetIdentifierFromPreference();
//            rl_app_list.setVisibility(View.VISIBLE);
//            toolbar.setVisibility(View.VISIBLE);
            mAppPresenter.getAppList();
        } else {
            tv_imei_number.setText(new DeviceInfo().getDeviceIMEI(this));
            List<String> mobile = new DeviceInfo().getSim1SerialNumber(this);
            if (mobile != null) {
                if (mobile.size() == 2) {
                    tv_sim1_number.setText(mobile.get(0).substring(0, mobile.get(0).length() - 1));
                    tv_sim2_number.setText(mobile.get(1).substring(0, mobile.get(1).length() - 1));
                } else if (mobile.size() == 1) {
                    tv_sim1_number.setText(mobile.get(0).substring(0, mobile.get(0).length() - 1));
                    tv_sim2_number.setText("");
                }

            }

        }

    }

    @Override //overriden method of onclick action
    public void onClick(View v) {
        String tag = "";
        switch (v.getId()) {
            case R.id.gps_location:
                if (userControl != null && (userControl.getLocationUser() != null && userControl.getLocationUser().equalsIgnoreCase(ConfigurationType.user.toString()))) {
                    tag = btn_gps_location.getTag().toString();
                    if (tag.equals(SwitchType.On.toString())) {
                        mGpsPresenter.stopGpsService();
                        btn_gps_location.setTag(SwitchType.Off.toString());
                        btn_gps_location.setImageResource(R.drawable.ic_place_black_24px);
                    } else {
                        btn_gps_location.setTag(SwitchType.On.toString());
                        btn_gps_location.setImageResource(R.drawable.ic_place_white_24px);
                        mGpsPresenter.startGpsService(appConfiguration.getGpstimeinterval());
                    }
                    String gps = btn_gps_location.getTag().toString();
                    appConfiguration.setGps(gps);
                    mAppPresenter.updateDBColumn(Contractor.AppConfigurationTable.COLUMN_NAME_GPS, gps, true);
                }

                break;
            case R.id.mobile_data:
                if (userControl != null && userControl.getMobileUser() != null && userControl.getMobileUser().equalsIgnoreCase(ConfigurationType.user.toString())) {

                    if (btn_areo_plane.getTag().equals(SwitchType.Off.toString())) {
                        tag = btn_mobile_data.getTag().toString();
                        if (tag.equals(SwitchType.On.toString())) {
                            if (MDMSettings.isMobileDataEnable(this)) {
                                MDMSettings.mobileDataSettings(this);
                            } else {
                                btn_mobile_data.setTag(SwitchType.Off.toString());
                                btn_mobile_data.setImageResource(R.drawable.import_export_black_24);
                                String mobileDate = btn_mobile_data.getTag().toString();
                                appConfiguration.setMobileData(mobileDate);
                                mAppPresenter.updateDBColumn(Contractor.AppConfigurationTable.COLUMN_NAME_MOBILE_DATA, mobileDate, true);
                            }
                        } else {
                            if (!MDMSettings.isMobileDataEnable(this)) {
                                MDMSettings.mobileDataSettings(this);

                            } else {
                                btn_mobile_data.setTag(SwitchType.On.toString());
                                btn_mobile_data.setImageResource(R.drawable.import_export_white_24);
                                String mobileDate = btn_mobile_data.getTag().toString();
                                appConfiguration.setMobileData(mobileDate);
                                mAppPresenter.updateDBColumn(Contractor.AppConfigurationTable.COLUMN_NAME_MOBILE_DATA, mobileDate, true);
                            }
                        }
                    }
                }


                break;
            case R.id.aero_plane:
                if (userControl != null && userControl.getAeroUser() != null && userControl.getAeroUser().equalsIgnoreCase(ConfigurationType.user.toString())) {
                    tag = btn_areo_plane.getTag().toString();
                    if (tag.equals(SwitchType.On.toString())) {
                        if (MDMSettings.isAirplaneMode(this)) {
                            MDMSettings.flightModeSettings(this);
                        } else {
                            // MDMSettings.toggleAirplaneMode(0, true, this);
                            btn_areo_plane.setTag(SwitchType.Off.toString());
                            btn_areo_plane.setImageResource(R.drawable.ic_airplanemode_inactive_black_24px);
                            if (MDMSettings.isMobileDataEnable(this)) {
                                btn_mobile_data.setImageResource(R.drawable.import_export_white_24);
                                btn_mobile_data.setTag(SwitchType.On.toString());
                            }
                            String mobileDate1 = btn_mobile_data.getTag().toString();
                            appConfiguration.setMobileData(mobileDate1);
                            mAppPresenter.updateDBColumn(Contractor.AppConfigurationTable.COLUMN_NAME_MOBILE_DATA, mobileDate1, true);
                            String aeroPlane = btn_areo_plane.getTag().toString();
                            appConfiguration.setFlightMode(aeroPlane);
                            mAppPresenter.updateDBColumn(Contractor.AppConfigurationTable.COLUMN_NAME_Flight_MODE, aeroPlane, true);
                        }
                    } else {
                        if (!MDMSettings.isAirplaneMode(this)) {
                            MDMSettings.flightModeSettings(this);
                        } else {
                            //MDMSettings.toggleAirplaneMode(1, false, this);
                            btn_areo_plane.setTag(SwitchType.On.toString());
                            btn_areo_plane.setImageResource(R.drawable.ic_airplanemode_active_white_24px);
                            //MDMSettings.turnGPSOn(this);
                            btn_mobile_data.setImageResource(R.drawable.import_export_black_24);
                            btn_mobile_data.setTag(SwitchType.Off.toString());

                            String mobileDate1 = btn_mobile_data.getTag().toString();
                            appConfiguration.setMobileData(mobileDate1);
                            mAppPresenter.updateDBColumn(Contractor.AppConfigurationTable.COLUMN_NAME_MOBILE_DATA, mobileDate1, true);
                            String aeroPlane = btn_areo_plane.getTag().toString();
                            appConfiguration.setFlightMode(aeroPlane);
                            mAppPresenter.updateDBColumn(Contractor.AppConfigurationTable.COLUMN_NAME_Flight_MODE, aeroPlane, true);
                        }
                    }

                }
                break;
            case R.id.flash_light:
                boolean hasFlash = this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
                if (hasFlash) {
                    tag = btn_flash_light.getTag().toString();
                    if (tag.equals(SwitchType.On.toString())) {
                        MDMSettings.setFlashLight(false);
                        btn_flash_light.setTag(SwitchType.Off.toString());
                        btn_flash_light.setImageResource(R.drawable.ic_flash_inactive_black_24px);
                    } else {
                        MDMSettings.setFlashLight(true);
                        MDMSettings.toggleMobileDataConnection(true, this);
                        btn_flash_light.setTag(SwitchType.On.toString());
                        btn_flash_light.setImageResource(R.drawable.ic_flash_active_white24px);

                    }
                } else {
                    btn_flash_light.setTag(SwitchType.Off.toString());
                    btn_flash_light.setImageResource(R.drawable.ic_flash_inactive_black_24px);
                }

                break;
            case R.id.auto_rotate:
                tag = btn_auto_rotate.getTag().toString();
                if (tag.equals(SwitchType.On.toString())) {
                    MDMSettings.setAutoOrientation(this, false);
                    btn_auto_rotate.setTag(SwitchType.Off.toString());
                    btn_auto_rotate.setImageResource(R.drawable.ic_screen_lock_rotation_black_24px);
                } else {
                    MDMSettings.setAutoOrientation(this, true);
                    btn_auto_rotate.setTag(SwitchType.On.toString());
                    btn_auto_rotate.setImageResource(R.drawable.ic_screen_rotation_white_24px);
                }
                String screenOrientation = btn_auto_rotate.getTag().toString();
                appConfiguration.setScreenOrientation(screenOrientation);
                mAppPresenter.updateDBColumn(Contractor.AppConfigurationTable.COLUMN_NAME_Screen_Orientation, screenOrientation, true);
                break;
            case R.id.bluetooth:
                if (userControl != null && userControl.getBtUser() != null && userControl.getBtUser().equalsIgnoreCase(ConfigurationType.user.toString())) {
                    tag = btn_blue_tooth.getTag().toString();
                    if (tag.equals(SwitchType.On.toString())) {
                        if (MDMSettings.isBTEnabled()) {
                            MDMSettings.setBluetooth();
                        }
                        btn_blue_tooth.setTag(SwitchType.Off.toString());
                        btn_blue_tooth.setImageResource(R.drawable.ic_bluetooth_disabled_black_24px);
                    } else {
                        if (!MDMSettings.isBTEnabled()) {
                            MDMSettings.setBluetooth();
                        }
                        btn_blue_tooth.setTag(SwitchType.On.toString());
                        btn_blue_tooth.setImageResource(R.drawable.ic_bluetooth_white_24px);
                    }
                    String bluetooth = btn_blue_tooth.getTag().toString();
                    appConfiguration.setBluetooth(bluetooth);
                    mAppPresenter.updateDBColumn(Contractor.AppConfigurationTable.COLUMN_NAME_BLUETOOTH, bluetooth, true);
                }

                break;
            case R.id.wifi:
                if (userControl != null && userControl.getWifiUser() != null && userControl.getWifiUser().equalsIgnoreCase(ConfigurationType.user.toString())) {
                    // if (btn_areo_plane.getTag().equals(SwitchType.Off.toString())) {
                    tag = btn_wifi_network.getTag().toString();
                    if (tag.equals(SwitchType.On.toString())) {
                        if (MDMSettings.isWifiEnabled(this)) {
                            MDMSettings.setWifi(this);
                        }

                        btn_connect.setVisibility(View.GONE);
                        btn_wifi_network.setTag(SwitchType.Off.toString());
                        tv_network_label.setText("No Network");
                        btn_wifi_network.setImageResource(R.drawable.ic_signal_wifi_off_black_24px);
                        if (wifi_layout.getVisibility() == View.VISIBLE) {
                            wifi_layout.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        if (!MDMSettings.isWifiEnabled(this)) {
                            MDMSettings.setWifi(this);
                        }
                        btn_connect.setVisibility(View.VISIBLE);
                        btn_wifi_network.setTag(SwitchType.On.toString());
                        btn_wifi_network.setImageResource(R.drawable.ic_network_wifi_white_24px);
                        if (wifi_layout.getVisibility() == View.INVISIBLE) {
                            wifi_layout.setVisibility(View.VISIBLE);
                        }
                    }
                    String wifi = btn_wifi_network.getTag().toString();
                    appConfiguration.setWifi(wifi);
                    mAppPresenter.updateDBColumn(Contractor.AppConfigurationTable.COLUMN_NAME_WIFI, wifi, true);
                    // }
                }
                break;
            case R.id.gps_mode:
                break;
            case R.id.wifi_connect:
                String ssid = et_username.getText().toString().trim();
                if (ssid.length() > 0) {
                    String password = et_password.getText().toString().trim();
                    if (scanResultList != null) {
                        int securityType = mWifiPresenter.getSecurityType(scanResultList.get(scanPosition));
                        String networkSSID = scanResultList.get(scanPosition).SSID;
                        if (securityType != 0 && password.length() <= 0) {
                            et_password.requestFocus();
                            et_password.setHint("Enter Password");
                            et_password.setHighlightColor(getResources().getColor(R.color.red));
                        } else {
                            if (mWifiPresenter.connectToWifi(networkSSID, securityType, password)) {
                                // if (WifiBusiness.getInstance(Constants.getmContext()).getConnectedNetwork().equalsIgnoreCase(networkSSID)) {
                                Toast.makeText(this, ssid + "Connected", Toast.LENGTH_SHORT).show();
//                            } else {
//                                Toast.makeText(this, "Unable to connect " + ssid, Toast.LENGTH_SHORT).show();
//                            }

                            } else {
                                Toast.makeText(this, "Unable to connect " + ssid, Toast.LENGTH_SHORT).show();
                            }
                        }

                    }

                }
                break;
            case R.id.btn_recieve:
                MDMSettings.dataBeamFlag = false;
                tv_recieve_label.setTextColor(getResources().getColor(R.color.orange));
                tv_send_label.setTextColor(getResources().getColor(R.color.black));
                btn_send.setEnabled(false);
                btn_send.setClickable(false);
                mWifiPresenter.enableHostSpot();

                break;
            case R.id.btn_send:
                MDMSettings.dataBeamFlag = true;
                tv_recieve_label.setTextColor(getResources().getColor(R.color.black));
                tv_send_label.setTextColor(getResources().getColor(R.color.orange));
                btn_receive.setEnabled(false);
                btn_receive.setClickable(false);
                mWifiPresenter.scanHotSpots(false);
                break;
            case R.id.tv_app_data:
                view_app_data.setBackgroundResource(R.color.orange);
                view_app_files.setBackgroundResource(R.color.black);
                mWifiPresenter.filesList(".apk");
                break;
            case R.id.tv_files:
                view_app_data.setBackgroundResource(R.color.black);
                view_app_files.setBackgroundResource(R.color.orange);
                mWifiPresenter.filesList("");
                break;
            case R.id.btn_transfer_data:
                if (!mFilePath.equals("")) {
                    if (mRotateProgressBar != null) {
                        mRotateProgressBar.setVisibility(View.VISIBLE);
                    }
                    mWifiPresenter.sendFile(mFilePath);
                } else {
                    Toast.makeText(this, "Please Select File to Transfer", Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override  //overriden method for on seek bar changed of brightness ,ringer and media volume
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        seek_bar_progress = progress;
        switch (seekBar.getId()) {
            case R.id.seekbar_brightness:
                MDMSettings.setBrightness(seek_bar_progress, this, this);
                break;
            case R.id.seekbar_ringer:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    MDMSettings.offDNDMode(notificationManager);
                }
                MDMSettings.setRingerVolume(this, seek_bar_progress);
                break;
            case R.id.seekbar_media:
                MDMSettings.setMediaVolume(this, seek_bar_progress);
                break;
        }
    }

    @Override //overriden method for on seek bar changed of brightness ,ringer and media volume
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override //overriden method of activity
    protected void onStop() {
        startService(new Intent(this, AppStartStopService.class));
        super.onStop();
    }

    @Override //overriden method for on seek bar changed of brightness ,ringer and media volume
    public void onStopTrackingTouch(SeekBar seekBar) {
        switch (seekBar.getId()) {
            case R.id.seekbar_brightness:
                MDMSettings.setKeyValue(this, MDMSettings.KEY_BRIGHTNESS, seek_bar_progress);
                break;
            case R.id.seekbar_ringer:
                MDMSettings.setKeyValue(this, MDMSettings.KEY_RINGER, seek_bar_progress);
                break;
            case R.id.seekbar_media:
                MDMSettings.setKeyValue(this, MDMSettings.KEY_MEDIA, seek_bar_progress);
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override //overriden method activity
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // If the permission has been checked
        if (requestCode == MDMSettings.REQUEST_HOME_SETTINGS) {
            selectLauncher();
        } else if (requestCode == MDMSettings.REQUEST_FOR_OVERLAY) {
            if (Settings.canDrawOverlays(this)) {
                Log.d("permission", "granted");
                // goToReactActivity();
                // preventStatusBarExpansion(this);
                if (!Settings.System.canWrite(getApplicationContext())) {
                    MDMSettings.callCanWriteSettings(this);
                } else if (!MDMSettings.getAppUsageSettings()) {
                    MDMSettings.appUsageStats(this);
                } else if (!MDMSettings.isDoNotDisturbGranted(notificationManager)) {
                    MDMSettings.doNotDisturbSettings(this);
                } else {
                    checkPermission();
                }

            } else {

                MDMSettings.callDrawOverlaySettings(this);
            }
        } else if (requestCode == MDMSettings.REQUEST_FOR_CAN_WRITE) {
            if (!Settings.System.canWrite(this)) {
                MDMSettings.callCanWriteSettings(this);
            } else if (!MDMSettings.getAppUsageSettings()) {
                MDMSettings.appUsageStats(this);
            } else if (!MDMSettings.isDoNotDisturbGranted(notificationManager)) {
                MDMSettings.doNotDisturbSettings(this);
            } else {
                checkPermission();
            }
        } else if (requestCode == MDMSettings.REQUEST_USAGE_STATS) {
            if (!MDMSettings.getAppUsageSettings()) {
                MDMSettings.appUsageStats(this);
            } else if (!MDMSettings.isDoNotDisturbGranted(notificationManager)) {
                MDMSettings.doNotDisturbSettings(this);
            } else {
                checkPermission();
            }

        } else if (requestCode == MDMSettings.REQUEST_DO_NOT_DISTURB) {
            if (!MDMSettings.isDoNotDisturbGranted(notificationManager)) {
                MDMSettings.doNotDisturbSettings(this);
            } else {
                checkPermission();
            }

        } else if (requestCode == 4567) {

            if (Settings.System.canWrite(this)) {
                setUpSettingsViews();
            }
        } else if (requestCode == MDMSettings.REQUEST_FOR_MOBILE_DATA) {
            if (btn_mobile_data.getTag().equals(SwitchType.On.toString())) {
                if (!MDMSettings.isMobileDataEnable(this)) {
                    btn_mobile_data.setTag(SwitchType.Off.toString());
                    btn_mobile_data.setImageResource(R.drawable.import_export_black_24);
                    String mobileDate1 = btn_mobile_data.getTag().toString();
                    appConfiguration.setMobileData(mobileDate1);
                    mAppPresenter.updateDBColumn(Contractor.AppConfigurationTable.COLUMN_NAME_MOBILE_DATA, mobileDate1, true);
                } else {
                    Toast.makeText(MainActivity.this, "Please switch off the mobile data", Toast.LENGTH_LONG).show();
                    MDMSettings.mobileDataSettings(this);
                }
            } else {
                if (MDMSettings.isMobileDataEnable(this)) {
                    btn_mobile_data.setTag(SwitchType.On.toString());
                    btn_mobile_data.setImageResource(R.drawable.import_export_white_24);
                    String mobileDate1 = btn_mobile_data.getTag().toString();
                    appConfiguration.setMobileData(mobileDate1);
                    mAppPresenter.updateDBColumn(Contractor.AppConfigurationTable.COLUMN_NAME_MOBILE_DATA, mobileDate1, true);
                } else {
                    Toast.makeText(MainActivity.this, "Please switch on the mobile data", Toast.LENGTH_LONG).show();
                    MDMSettings.mobileDataSettings(this);
                }

            }
        } else if (requestCode == MDMSettings.REQUEST_FLIGHT_MODE) {

            if (btn_areo_plane.getTag().toString().equalsIgnoreCase(SwitchType.On.toString())) {
                if (!MDMSettings.isAirplaneMode(this)) {
                    btn_areo_plane.setTag(SwitchType.Off.toString());
                    btn_areo_plane.setImageResource(R.drawable.ic_airplanemode_inactive_black_24px);
                    if (MDMSettings.isMobileDataEnable(this)) {
                        btn_mobile_data.setImageResource(R.drawable.import_export_white_24);
                        btn_mobile_data.setTag(SwitchType.On.toString());
                    } else {
                        btn_mobile_data.setImageResource(R.drawable.import_export_black_24);
                        btn_mobile_data.setTag(SwitchType.Off.toString());
                    }

                    if (MDMSettings.isWifiEnabled(this)) {
                        btn_wifi_network.setTag(SwitchType.On.toString());
                        btn_wifi_network.setImageResource(R.drawable.ic_network_wifi_white_24px);
                        btn_connect.setVisibility(View.VISIBLE);
                        tv_network_label.setText(wifi_spinner.getText().toString());
                        if (wifi_layout.getVisibility() == View.INVISIBLE) {
                            wifi_layout.setVisibility(View.VISIBLE);
                        }

                    } else {
//                        if(appConfiguration!=null && appConfiguration.getWifi().equalsIgnoreCase(SwitchType.On.toString())){
//
//                            if (MDMSettings.isWifiEnabled(this)==false) {
//                                MDMSettings.setWifi(this);
//                            }
//                            tv_network_label.setText(wifi_spinner.getText().toString());
//                            btn_connect.setVisibility(View.VISIBLE);
//                            btn_wifi_network.setTag(SwitchType.On.toString());
//                            btn_wifi_network.setImageResource(R.drawable.ic_network_wifi_white_24px);
//                            if (wifi_layout.getVisibility() == View.INVISIBLE) {
//                                wifi_layout.setVisibility(View.VISIBLE);
//                            }
//                        }else {
                        btn_wifi_network.setTag(SwitchType.Off.toString());
                        btn_wifi_network.setImageResource(R.drawable.ic_signal_wifi_off_black_24px);
                        tv_network_label.setText("No Network");
                        if (wifi_layout.getVisibility() == View.VISIBLE) {
                            wifi_layout.setVisibility(View.INVISIBLE);
                        }
                        //  }

                    }


                    String wifiData = btn_wifi_network.getTag().toString();
                    appConfiguration.setWifi(wifiData);
                    mAppPresenter.updateDBColumn(Contractor.AppConfigurationTable.COLUMN_NAME_WIFI, wifiData, true);
                    String mobileDate1 = btn_mobile_data.getTag().toString();
                    appConfiguration.setMobileData(mobileDate1);
                    mAppPresenter.updateDBColumn(Contractor.AppConfigurationTable.COLUMN_NAME_MOBILE_DATA, mobileDate1, true);
                    String aeroPlane = btn_areo_plane.getTag().toString();
                    appConfiguration.setFlightMode(aeroPlane);
                    mAppPresenter.updateDBColumn(Contractor.AppConfigurationTable.COLUMN_NAME_Flight_MODE, aeroPlane, true);
                } else {
                    Toast.makeText(MainActivity.this, "Please switch off the Flight mode", Toast.LENGTH_LONG).show();
                    MDMSettings.flightModeSettings(this);
                }
            } else {
                if (MDMSettings.isAirplaneMode(this)) {
                    btn_areo_plane.setTag(SwitchType.On.toString());
                    btn_areo_plane.setImageResource(R.drawable.ic_airplanemode_active_white_24px);
                    //MDMSettings.turnGPSOn(this);
                    btn_mobile_data.setImageResource(R.drawable.import_export_black_24);
                    btn_mobile_data.setTag(SwitchType.Off.toString());

                    btn_wifi_network.setTag(SwitchType.Off.toString());
                    btn_wifi_network.setImageResource(R.drawable.ic_signal_wifi_off_black_24px);
                    tv_network_label.setText("No Network");
                    if (wifi_layout.getVisibility() == View.VISIBLE) {
                        wifi_layout.setVisibility(View.INVISIBLE);
                    }
                    String wifiData = btn_wifi_network.getTag().toString();
                    appConfiguration.setWifi(wifiData);
                    mAppPresenter.updateDBColumn(Contractor.AppConfigurationTable.COLUMN_NAME_WIFI, wifiData, true);

                    String mobileDate1 = btn_mobile_data.getTag().toString();
                    appConfiguration.setMobileData(mobileDate1);
                    mAppPresenter.updateDBColumn(Contractor.AppConfigurationTable.COLUMN_NAME_MOBILE_DATA, mobileDate1, true);
                    String aeroPlane = btn_areo_plane.getTag().toString();
                    appConfiguration.setFlightMode(aeroPlane);
                    mAppPresenter.updateDBColumn(Contractor.AppConfigurationTable.COLUMN_NAME_Flight_MODE, aeroPlane, true);
                } else {
                    //Toast.makeText(MainActivity.this, "Please switch on the Flight mode", Toast.LENGTH_LONG).show();
                    MDMSettings.flightModeSettings(this);
                }

            }

        } else if (requestCode == MDMSettings.REQUEST_GPS_CHECK_SETTINGS) {
            switch (resultCode) {

                case RESULT_OK:
                    // All required changes were successfully made

                    if (MDMSettings.checkGpsStatus(this)) {
                        if (initial_flag) {
                            initial_flag = false;
                            appConfiguration = responseData.getAppConfiguration();
                            enableIconBaseOnConfigurationData(appConfiguration);
                        }
                        if (BasicImplementation.isGooglePlayServicesAvailable(this)) {
                            mGpsPresenter.startGpsService(appConfiguration.getGpstimeinterval());
                            Toast.makeText(this, "Gps Enabled", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(this, "Google Play service not available", Toast.LENGTH_SHORT).show();
                        }
                    }

                    break;
                case RESULT_CANCELED:
                    // The user was asked to change settings, but chose not to
                    mGpsPresenter.startGpsService(appConfiguration.getGpstimeinterval());

                    break;
                default:
                    break;
            }
        }
    }

   /* @Override
    public void volumeEvent() {
        super.volumeEvent();
        if (seekbar_ringer_volume != null) {
            int volume = MDMSettings.getDefalutRingerVolume(this);
            seekbar_ringer_volume.setProgress(volume);
            MDMSettings.setKeyValue(this, MDMSettings.KEY_RINGER, volume);
        }
    }
*/

    //methdd to text the selected value of spinner
    public void notifySpinnerData(String str, int position) {
        dialog.dismiss();
        Log.d("Wifi network", "SSID:" + str);
        if (index == 1) {
            scanPosition = position;
            int security = mWifiPresenter.getSecurityType(scanResultList.get(position));
            et_password.setText("");
            switch (security) {
                case 0: //none
                    et_password.setEnabled(false);
                    et_password.setHint("");
                    break;
                case 1: //  SECURITY_WEP
                    et_password.setHint("Enter Password");
                    et_password.requestFocus();
                    break;
                case 2:  //SECURITY_EAP
                    et_password.setHint("Enter Password");
                    et_password.requestFocus();
                    break;
                case 3: //SECURITY_PSK
                    et_password.setHint("Enter Password");
                    et_password.requestFocus();
                    break;

            }
            et_username.setText(str);
            wifi_spinner.setText(str);
            tv_network_label.setText(str);
            if (btn_connect.getVisibility() == View.GONE)
                btn_connect.setVisibility(View.VISIBLE);
        } else if (index == 2) {
            changeGpsMode(str);

        }

    }


    //method to show spinner selection
    public void showSpinnerselectionDialog(String title, List<String> listObj) {

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        View view = LayoutInflater.from(this).inflate(R.layout.show_multi_selection_alert, null);
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        TextView tv_logout_header = (TextView) dialog.findViewById(R.id.tv_logout_header);
        tv_logout_header.setText(title);

        final ListView listView = (ListView) dialog.findViewById(R.id.listView);

        SpinnerAdapter spinnerAdapter1 = new SpinnerAdapter(this, listObj);
        listView.setAdapter(spinnerAdapter1);
        dialog.show();
    }


    //method to initalize databeam screen objects
    public void dataBeamScreenInitilization() {
        app_files_list = new ArrayList<>();
        app_files_data_array = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, app_files_list);
        discoveryList = new ArrayList<>();
        discoveryArray = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, discoveryList);

        btn_send = (ImageView) findViewById(R.id.btn_send);
        btn_send.setEnabled(true);
        btn_send.setOnClickListener(this);

        btn_receive = (ImageView) findViewById(R.id.btn_recieve);
        btn_receive.setEnabled(true);
        btn_receive.setOnClickListener(this);

        tv_send_label = (TextView) findViewById(R.id.tv_send_lable);
        tv_send_label.setTextColor(getResources().getColor(R.color.black));
        tv_recieve_label = (TextView) findViewById(R.id.tv_recive_label);
        tv_recieve_label.setTextColor(getResources().getColor(R.color.black));
        tv_app_data = (TextView) findViewById(R.id.tv_app_data);
        tv_app_data.setOnClickListener(this);
        tv_app_files = (TextView) findViewById(R.id.tv_files);
        tv_app_files.setOnClickListener(this);
        tv_password = (TextView) findViewById(R.id.tv_password);
        tv_password.setText("Password");
        tv_connection_status = (TextView) findViewById(R.id.tv_connection_status);
        tv_connection_status.setText("Status: Not Connected");
        view_app_data = (View) findViewById(R.id.view_app_data);
        view_app_data.setBackgroundResource(R.color.red);
        view_app_files = (View) findViewById(R.id.view_files);
        spinner_app_info = (Spinner) findViewById(R.id.spinner_files_data);
        spinner_app_info.setAdapter(app_files_data_array);
        view_app_files.setBackgroundResource(R.color.black);
        spinner_discovry_list = (Spinner) findViewById(R.id.spinner_discovery);
        spinner_discovry_list.setAdapter(discoveryArray);
        spinner_app_info.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position > 0) {
                    mFilePath = app_files_list.get(position - 1);
                } else {
                    mFilePath = "";
                }
                Log.d("File path", mFilePath);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_discovry_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position > 0) {
                    mWifiPresenter.displayPasswordDialog(discoveryList.get(position - 1));
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btn_transfer = (Button) findViewById(R.id.btn_transfer_data);
        btn_transfer.setOnClickListener(this);
        btn_transfer.setEnabled(false);
        btn_transfer.setBackgroundResource(R.drawable.bg_button_gray);
        mRotateProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        if (mRotateProgressBar != null) {
            mRotateProgressBar.setVisibility(View.INVISIBLE);
        }

        tv_hotspot_name = (TextView) findViewById(R.id.tv_hotspot_name);
        tv_hotspot_name.setText("");
    }

    //method to set the progress of databeam
    public void setProgressToCircle(int percentage) {
        if (percentage > 100)
            percentage = 100;
        tv_progress_update.setText("" + percentage + "%");
        pieProgressDrawable.setLevel(percentage);
        timeProgress.invalidate();

    }

    //method to initialze the progress of databeam
    public void dataBeamProgressBar() {
        pieProgressDrawable = new PieProgressDrawable();
        pieProgressDrawable.setColor(ContextCompat.getColor(this, R.color.blue));
        pieProgressDrawable.setCircleColor(ContextCompat.getColor(this, R.color.gray));
        timeProgress = (ImageView) findViewById(R.id.time_progress);
        timeProgress.setImageDrawable(pieProgressDrawable);
        tv_progress_update = (TextView) findViewById(R.id.tv_update);
        tv_progress_update.setText("0%");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setProgressToCircle(0);
            }
        });
    }


    @Override //overriden method of atcivity
    protected void onDestroy() {

        if (MDMSettings.isBTEnabled()) {
            MDMSettings.setBluetooth();
        }
        if (MDMSettings.checkGpsStatus(this)) {
            mGpsPresenter.stopGpsService();
        }

        unregisterServerUpdateReceiver();

        super.onDestroy();
    }

    ///method to save device usage in db
    public void saveDeviceUsage() {
        DeviceUsage deviceUsage = new DeviceUsage(new DeviceInfo().getUUID(this), Constants.Device_Switch_On_Date_Time, Constants.Device_Switch_Off_Date_Time);
        DatabaseMgr db = DatabaseMgr.getInstance(this);
        db.insertDeviceUsageRecords(deviceUsage);
    }

    //method to change the gps mode
    public void changeGpsMode(final String str) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(Constants.getmContext(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(Constants.getmContext());
        }
        builder.setTitle("Gps Mode")
                .setMessage("Are you sure you want to Change the Gps Mode ?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();


                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


    //method to diaply the notificaiton view
    public void notificationView() {

        if (rel_settings != null && (rel_settings.getVisibility() == View.VISIBLE)) {
            rel_settings.setVisibility(View.GONE);
        }
        if (rel_data_beam != null && (rel_data_beam.getVisibility() == View.VISIBLE)) {
            rel_data_beam.setVisibility(View.GONE);
        }
        if (rel_about_device != null && (rel_about_device.getVisibility() == View.VISIBLE)) {
            rel_about_device.setVisibility(View.GONE);
        }

        if (notification_recycler_view == null) {
            notification_recycler_view = (RecyclerView) findViewById(R.id.notification_recycler_view);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
            notification_recycler_view.setLayoutManager(linearLayoutManager);
        }
        if (rel_notification_data != null && (rel_notification_data.getVisibility() == View.GONE)) {
            rel_notification_data.setVisibility(View.VISIBLE);
        }

        mAppPresenter.getNotificationList();
        if (!notificationDataList.isEmpty()) {
            toolbar_textview.setText("Notifications");
            tool_bar_logo.setImageResource(R.drawable.ic_arrow_back_black_48px);
            screen_navigation = 3;
            back_press_flag = true;
            NotificationListAdapter notificationListAdapter = new NotificationListAdapter(this, notificationDataList, this);
            notification_recycler_view.setAdapter(notificationListAdapter);
        }

    }

    //method to open contacts
    public void openContacts() {

        back_press_flag = true;
        screen_navigation = 4;
        toolbar_textview.setText("Phone");
        tool_bar_logo.setImageResource(R.drawable.ic_arrow_back_black_48px);
        getContactView();
    }

    //method to open contact view
    public void getContactView() {
        if (rel_settings.getVisibility() == View.VISIBLE) {
            rel_settings.setVisibility(View.GONE);
        }
        if (rel_notification_data != null && (rel_notification_data.getVisibility() == View.VISIBLE)) {
            rel_notification_data.setVisibility(View.GONE);
        }

        if (rel_data_beam.getVisibility() == View.VISIBLE) {
            rel_data_beam.setVisibility(View.GONE);
        }
        if (rl_app_list.getVisibility() == View.VISIBLE) {
            rl_app_list.setVisibility(View.GONE);
        }
        if (ll_dots.getVisibility() == View.VISIBLE) {
            ll_dots.setVisibility(View.GONE);
        }

        if (rel_contact_view.getVisibility() == View.GONE) {
            rel_contact_view.setVisibility(View.VISIBLE);
        }
        if (rel_about_device.getVisibility() == View.VISIBLE) {
            rel_about_device.setVisibility(View.GONE);
        }

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);


        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager());

        adapter.addFrag(new OpenDialpad(), "Dial");
        adapter.addFrag(new NewContact(), "Add Contact");
        adapter.addFrag(new PhoneContacts(), "Contacts");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    //method to check mobile data with app configuation and displays scrolling message
    public void checkMobileData(LinearLayout scrollTextLinear, TextView tvScrollText) {
        if (!userControl.getMobileUser().equalsIgnoreCase(ConfigurationType.user.toString())) {

            if (appConfiguration.getMobileData().equalsIgnoreCase(SwitchType.On.toString())) {
                if (!MDMSettings.isMobileDataEnable(this)) {
                    if (scrollTextLinear.getVisibility() == View.GONE) {
                        scrollTextLinear.setVisibility(View.VISIBLE);
                    }
                    tvScrollText.setText(getResources().getString(R.string.deviationMobileOff));
                } else {
                    if (scrollTextLinear.getVisibility() == View.VISIBLE) {
                        scrollTextLinear.setVisibility(View.GONE);
                    }
                }
            } else {
                if (MDMSettings.isMobileDataEnable(this)) {
                    if (scrollTextLinear.getVisibility() == View.GONE) {
                        scrollTextLinear.setVisibility(View.VISIBLE);
                    }
                    tvScrollText.setText(getResources().getString(R.string.deviationMobileOn));
                } else {
                    if (scrollTextLinear.getVisibility() == View.VISIBLE) {
                        scrollTextLinear.setVisibility(View.GONE);
                    }
                }
            }
        } else {
            tvScrollText.setText("");
            if (scrollTextLinear.getVisibility() == View.VISIBLE) {
                scrollTextLinear.setVisibility(View.GONE);
            }
        }
    }

    //method to check airplane mode with app configuation and displays scrolling message
    public void displayMobileDataContent(AppConfiguration appConfiguration, UserControl userControl) {

        LinearLayout scrollTextLinear = (LinearLayout) findViewById(R.id.scroll_text);
        TextView tvScrollText = (TextView) findViewById(R.id.tv_scroll_content);
        tvScrollText.setText("");
        tvScrollText.setSelected(true);
        if (appConfiguration != null && userControl != null) {
            if (!userControl.getAeroUser().equalsIgnoreCase(ConfigurationType.user.toString())) {
                if (appConfiguration.getFlightMode().equalsIgnoreCase("On")) {
                    if (!MDMSettings.isAirplaneMode(this)) {
                        if (scrollTextLinear.getVisibility() == View.GONE) {
                            scrollTextLinear.setVisibility(View.VISIBLE);
                        }
                        tvScrollText.setText(getResources().getString(R.string.flightModeOff));
                    } else {
                        if (scrollTextLinear.getVisibility() == View.VISIBLE) {
                            scrollTextLinear.setVisibility(View.GONE);
                        }
                    }
                } else {
                    if (MDMSettings.isAirplaneMode(this)) {
                        if (scrollTextLinear.getVisibility() == View.GONE) {
                            scrollTextLinear.setVisibility(View.VISIBLE);
                        }
                        tvScrollText.setText(getResources().getString(R.string.flightModeOn));
                    } else {
                        /*if (scrollTextLinear.getVisibility() == View.VISIBLE) {
                            scrollTextLinear.setVisibility(View.GONE);
                        }*/
                        checkMobileData(scrollTextLinear, tvScrollText);

                    }
                }
            } else {
                checkMobileData(scrollTextLinear, tvScrollText);
            }
        } else {
            tvScrollText.setText("");
            if (scrollTextLinear.getVisibility() == View.VISIBLE) {
                scrollTextLinear.setVisibility(View.GONE);
            }
        }

    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void checkInitialPermission() {// method to check intial permission of the application
        registerServerUpdateReceiver();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            if (!Constants.isMyAppLauncherDefault(this)) {
                MDMSettings.callHomeSettings(this);
            } else if (!Settings.canDrawOverlays(this)) {

                // Open the permission page
                MDMSettings.callDrawOverlaySettings(this);
                return;
            } else if (!Settings.System.canWrite(this)) {
                MDMSettings.callCanWriteSettings(this);
            } else {
                checkPermission();
            }
        } else {
            preventStatusBarExpansion(this);
            appRegistration();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void checkPermission() { //method to call permission
        ActivityCompat.requestPermissions(this, Constants.PERMISSION, MDMSettings.REQUEST_FOR_PERMISSION);
    }

    //overriden method of requrest permission event will be trigged here
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean allgranted = true;
        if (requestCode == MDMSettings.REQUEST_FOR_PERMISSION) {
            //check if all permissions are granted
            if (grantResults.length > 0) {
                for (int i = 0; i < grantResults.length; i++) {
                    Log.d("granstresults...", String.valueOf(grantResults[i]));
                    Log.d("permission...", String.valueOf(permissions[i]));
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        allgranted = false;
                        break;
                    }
                }
            }
        }
        if (allgranted) {
            preventStatusBarExpansion(this);
            appRegistration();
        } else {
            requestPermissions(Constants.PERMISSION, MDMSettings.REQUEST_FOR_PERMISSION);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void selectLauncher() { //method to check all settings and open the launcher

        if (!Constants.isMyAppLauncherDefault(this)) {
            MDMSettings.callHomeSettings(this);
        } else if (!Settings.canDrawOverlays(this)) {

            // Open the permission page
            MDMSettings.callDrawOverlaySettings(this);
            return;
        } else if (!Settings.System.canWrite(this)) {
            MDMSettings.callCanWriteSettings(this);
        } else if (!MDMSettings.getAppUsageSettings()) {
            MDMSettings.appUsageStats(this);
        } else if (!MDMSettings.isDoNotDisturbGranted(notificationManager)) {
            MDMSettings.doNotDisturbSettings(this);
        } else {
            checkPermission();
        }


    }

    @Override //method to block all the keys of mobile
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (BasicImplementation.blockedKeys.contains(event.getKeyCode())) {
            Log.d("KeyEvent ", "" + event.getKeyCode());
            return true;
        } else {
            return super.dispatchKeyEvent(event);
        }
    }

    //method to receive broad cast updates form other server and perform action
    public class BroadcastUpdates extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("MyRefreshedToken", "broadcasr received");
            if (intent.getAction().equals(Constants.SERVER_UPDATE_ACTION)) {
                // call appRegistration for new app list
                int status = intent.getIntExtra("status", 0);
                String message = intent.getStringExtra("message");
                if (status == 1) { //success
                    appRegistration(); // this is to enabled when server sends updated list
                    Log.d("BaseActivity", "Server Updates");
                } else if (status == 5) {
                    openUpdateDialog(message);
                } else {
                    Log.d("BaseActivity", "Message " + message);
                }

            } else if (intent.getAction().equals(Constants.NOTIFICATION_DATA_UPDATE)) {
                int notificationCount = intent.getIntExtra("count", 0);
                Log.d("MyRefreshedToken", "Inside Broadcast Reciever");
                updateCount(notificationCount);
                //update count in UI
            } else if (intent.getAction().equals(Constants.PHONE_CALL_UPDATES)) {
                Constants.frgndFlag = false;
            } else if (intent.getAction().equals(Constants.SMS_UPDATES)) {
                int smsEvent = intent.getIntExtra("status", 0);
                switch (smsEvent) {
                    case 1: // current location
                        mGpsPresenter.getCurrentGpsLocation();
                        break;
                    case 2: // unlock
                        MDMUnlock();
                        break;
                    case 3: // wipe out
                        break;
                    case 4: // internet availability
                        String networkSignalStrength = "No";
                        if (Constants.isNetworkAvailable(MainActivity.this)) {
                            //   networkSignalStrength = "Yes Network Signal " + Constants.getSignalStrength(this);
                        }
                        break;
                    case 5: //  data usage
                        break;
                    default:
                        break;

                }
            }
        }
    }

    //method to regsiter server update broadcast reciever
    public void registerServerUpdateReceiver() {
        broadcastUpdates = new BroadcastUpdates();
        IntentFilter filter = new IntentFilter(Constants.SERVER_UPDATE_ACTION);
        filter.addAction(Constants.NOTIFICATION_DATA_UPDATE);
        filter.addAction(Constants.PHONE_CALL_UPDATES);
        filter.addAction(Constants.SMS_UPDATES);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastUpdates, filter);
    }

    //method to unregsiter server update broadcast reciever
    public void unregisterServerUpdateReceiver() {
        if (broadcastUpdates != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastUpdates);
        }
    }


    //method to download the updated APK
    public void updateApk(String apkLink) {
        if (Constants.isNetworkAvailable(this)) {
            Constants.frgndFlag = false;
            new ApkFileDownloader(apkLink, false, "Updated").execute();

        } else {
            Toast.makeText(this, "Please check your network ", Toast.LENGTH_LONG).show();
        }
    }

    //method to diaply the alert dialog for apk update
    public void openUpdateDialog(String appLink) {


        final MyAlertDialog builder = new MyAlertDialog(getApplicationContext());
        builder.setTag(appLink);
        builder.setTitle("Alert..!").setMessage("A new version is available so please update the new version.").setCancelable(false).setPositiveButton("Update", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                updateApk(builder.getTag());
                dialog.cancel();

            }
        });
        final android.app.AlertDialog alert = builder.create();
        alert.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        alert.show();

    }


    //method to show the notification conent on click of notification
    public void showNotificationContent(String title, String content, Context context) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
        if (content.contains("<html")) {
            WebView webView = new WebView(context);
            TextView t = new TextView(context);
            t.setText(Html.fromHtml(title));
            webView.loadData(content, "text/html", "utf-8");
            builder.setTitle(t.getText().toString())
                    .setView(webView)
                    .setNeutralButton("OK", null)
                    .show();
        } else {
            builder.setTitle(title);
            builder.setMessage(content)
                    .setNeutralButton("OK", null)
                    .show();
        }

    }

    //method to download the APK
    public class ApkFileDownloader extends AsyncTask<Void, Void, String> {
        String urlPath = "";
        boolean flag;
        ProgressDialog pd;


        public ApkFileDownloader(String url, boolean fileFlag, String appTitle) {
            urlPath = url;
            flag = fileFlag;
            Log.d("Url path:", urlPath);
            Log.d("flag", "" + flag);
            pd = new ProgressDialog(MainActivity.this);
            pd.setTitle("Please wait..!");
            pd.setMessage(appTitle + " Apk is downloading..");
            pd.setCancelable(false);
            pd.setCanceledOnTouchOutside(false);

        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }

        @Override
        protected void onPostExecute(String path) {
            pd.dismiss();
            if (path != null) {
                installApkFromPath(path);
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            return ApkDownloader.getFileOrApk(urlPath, flag);
        }
    }

    public void installApkFromPath(String path) {

        if (!path.equals("")) {

            ApkDownloader.installApk(this, path);
        }
    }

    //method to instialize the acivity variables
    public void viewInitialization() {
        hideSoftKeyboard();
        mAppPresenter = new AppPresenter(this, this);
        mGpsPresenter = new GpsPresenter(this, this);
        mGpsPresenter.stopGpsService();
        mWifiPresenter = new WifiPresenter(this, this);
        toolbar_textview = (TextView) findViewById(R.id.toolbar_textview);
        tv_device_ph_num = (TextView) findViewById(R.id.tv_device_ph_num);
        rel_about_device = (RelativeLayout) findViewById(R.id.rel_about_device);
        iv_delete_all = (ImageView) findViewById(R.id.iv_delete_all);
        iv_delete_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DeleteNotification(2, "");
            }
        });
        ll_dots = (LinearLayout) findViewById(R.id.ll_dots);
        view_pager = (ViewPager) findViewById(R.id.view_pager);
        tv_round_1 = (TextView) findViewById(R.id.tv_round_1);
        tv_round_2 = (TextView) findViewById(R.id.tv_round_2);

        tv_device_identifier = (TextView) findViewById(R.id.tv_device_identifier);
        tv_about_staff_name = (TextView) findViewById(R.id.tv_staff_name_about);
        tv_staff_details = (TextView) findViewById(R.id.tv_staff_details_about);
        tv_reg_date = (TextView) findViewById(R.id.tv_reg_date);
        tv_version = (TextView) findViewById(R.id.tv_version);
        tv_ph_no = (TextView) findViewById(R.id.tv_ph_no);
        tv_email_id = (TextView) findViewById(R.id.tv_email_id);
        tv_custm_label = (TextView) findViewById(R.id.tv_custm_label);
        tv_device_details_label = (TextView) findViewById(R.id.tv_device_details_label);

        rl_app_list = (RelativeLayout) findViewById(R.id.rl_app_list);
        rel_asset_info = (RelativeLayout) findViewById(R.id.rel_asset_info);

        main_fab_button = (FloatingActionButton) findViewById(R.id.main_fab_button);
        main_fab_button.setVisibility(View.GONE);
        main_fab_button.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view == main_fab_button) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        openContacts();
                    }
                }
                return true;
            }
        });

        tool_bar_logo = (ImageView) findViewById(R.id.toolbar_logo);
        tool_bar_logo.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view == tool_bar_logo) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        if (back_press_flag) {
                            back_press_flag = false;
                            if (screen_navigation == 1) {
                                if (rel_settings.getVisibility() == View.VISIBLE) {
                                    rel_settings.setVisibility(View.GONE);
                                }
                                if (rl_app_list.getVisibility() == View.GONE) {
                                    rl_app_list.setVisibility(View.VISIBLE);
                                }
                                if (ll_dots.getVisibility() == View.GONE) {
                                    ll_dots.setVisibility(View.VISIBLE);
                                }
                            } else if (screen_navigation == 2) {
                                if (rel_data_beam.getVisibility() == View.VISIBLE) {
                                    rel_data_beam.setVisibility(View.GONE);
                                }


                                if (MDMSettings.dataBeamFlag) {
                                    mWifiPresenter.removeNetwork();

                                } else {
                                    Log.d(TAG, "" + MDMSettings.dataBeamFlag);
                                    mWifiPresenter.disableHotspot();
                                }
                                if (rl_app_list.getVisibility() == View.GONE) {
                                    rl_app_list.setVisibility(View.VISIBLE);
                                }
                                if (ll_dots.getVisibility() == View.GONE) {
                                    ll_dots.setVisibility(View.VISIBLE);
                                }
                            } else if (screen_navigation == 3) {
                                if (rel_notification_data.getVisibility() == View.VISIBLE) {
                                    rel_notification_data.setVisibility(View.GONE);
                                }
                                if (rl_app_list.getVisibility() == View.GONE) {
                                    rl_app_list.setVisibility(View.VISIBLE);
                                }
                                if (ll_dots.getVisibility() == View.GONE) {
                                    ll_dots.setVisibility(View.VISIBLE);
                                }
                                layout_notification.setVisibility(View.VISIBLE);
                                iv_delete_all.setVisibility(View.GONE);
                            } else if (screen_navigation == 4) {
                                if (rel_contact_view.getVisibility() == View.VISIBLE) {
                                    rel_contact_view.setVisibility(View.GONE);
                                }
                                if (rl_app_list.getVisibility() == View.GONE) {
                                    rl_app_list.setVisibility(View.VISIBLE);
                                }
                                if (ll_dots.getVisibility() == View.GONE) {
                                    ll_dots.setVisibility(View.VISIBLE);
                                }
                            } else if (screen_navigation == 5) {
                                if (rel_about_device.getVisibility() == View.VISIBLE) {
                                    rel_about_device.setVisibility(View.GONE);
                                }
                                if (rl_app_list.getVisibility() == View.GONE) {
                                    rl_app_list.setVisibility(View.VISIBLE);
                                }
                                if (ll_dots.getVisibility() == View.GONE) {
                                    ll_dots.setVisibility(View.VISIBLE);
                                }
                            }

                        }
                        toolbar_textview.setText("");
                        tool_bar_logo.setImageResource(R.drawable.toolbar_logo);
                    }
                }
                return true;
            }
        });


        rel_settings = (RelativeLayout) findViewById(R.id.rel_settings);
        rel_data_beam = (RelativeLayout) findViewById(R.id.rel_data_beam);
        rel_notification_data = (RelativeLayout) findViewById(R.id.rel_notification_data);
        wifi_layout = (LinearLayout) findViewById(R.id.wifi_layout);
        layout_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = Integer.parseInt(tv_notification_count.getText().toString());
                Log.d("Click", "" + count);
                if (count > 0) {
                    layout_notification.setVisibility(View.GONE);
                    iv_delete_all.setVisibility(View.VISIBLE);
                    notificationView();
                }
            }
        });

        if (Constants.Device_Switch_On_Date_Time != null && !Constants.Device_Switch_On_Date_Time.equals("")) {
            DeviceUsage deviceUsage = new DeviceUsage();
            deviceUsage.setSwitchOnDateTime(Constants.Device_Switch_On_Date_Time);
            deviceUsage.setSwitchOffDateTime("");
            mAppPresenter.saveDeviceUsage(deviceUsage);
            MDMSettings.saveFirstDeviceTime(Long.parseLong(Constants.Device_Switch_On_Date_Time));
            Constants.Device_Switch_On_Date_Time = "";
        } else if (MDMSettings.getFirstDeviceTime() == 0) {
            long deviceOnTime = DateUtils.getCurrentDateGMT();
            DeviceUsage deviceUsage = new DeviceUsage();
            deviceUsage.setSwitchOnDateTime(Long.toString(deviceOnTime));
            deviceUsage.setSwitchOffDateTime("");
            mAppPresenter.saveDeviceUsage(deviceUsage);
            MDMSettings.saveFirstDeviceTime(deviceOnTime);
            Constants.Device_Switch_On_Date_Time = "";
        }

        activateOverflowMenu();
        setUpSettingsViews();
        et_asset_id = (EditText) findViewById(R.id.et_asset_id);
        et_asset_id.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (et_asset_id.getText().toString().trim().length() > 0)
                    et_asset_id.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        tv_imei_number = (TextView) findViewById(R.id.imei_number);

        tv_sim1_number = (TextView) findViewById(R.id.sim1_number);

        tv_sim2_number = (TextView) findViewById(R.id.sim2_number);

        tv_staff_name = (TextView) findViewById(R.id.tv_staff_name);
        tv_staff_other_info = (TextView) findViewById(R.id.tv_staff_other_info);

        layout_staff = (LinearLayout) findViewById(R.id.layout_staff);
        layout_staff_other = (LinearLayout) findViewById(R.id.layout_staff_other);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {

            if (MDMSettings.getAssetIdentifierFromPreference() == null || MDMSettings.getAssetIdentifierFromPreference().trim().length() == 0) {
                rel_asset_info.setVisibility(View.VISIBLE);
                rl_app_list.setVisibility(View.GONE);
                ll_dots.setVisibility(View.GONE);
                toolbar.setVisibility(View.GONE);
            } else {
                rel_asset_info.setVisibility(View.GONE);
//                rl_app_list.setVisibility(View.VISIBLE);
//                toolbar.setVisibility(View.VISIBLE);
                if (MDMSettings.getAssetIdentifierFromPreference() != null) {
                    Constants.asset_identifier = MDMSettings.getAssetIdentifierFromPreference();
                }
                mAppPresenter.getAppList();
            }

        } else {
            if (MDMSettings.getAssetIdentifierFromPreference() == null || MDMSettings.getAssetIdentifierFromPreference().trim().length() == 0) {
                rel_asset_info.setVisibility(View.VISIBLE);
                rl_app_list.setVisibility(View.GONE);
                ll_dots.setVisibility(View.GONE);
                toolbar.setVisibility(View.GONE);
            } else {
                rel_asset_info.setVisibility(View.GONE);
            }
        }


        btn_register = (Button) findViewById(R.id.btn_register);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Constants.isNetworkAvailable(MainActivity.this)) {
                    mAppPresenter.getAppList();
                } else {
                    Toast.makeText(MainActivity.this, "Please check the internet", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btn_exit = (Button) findViewById(R.id.btn_exit);
        btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                directMDMUnlock();
            }
        });
        cb_verify = (CheckBox) findViewById(R.id.cb_verifiy);
        et_asset_id.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (et_asset_id.getText().toString().length() == 0) {
                    cb_verify.setChecked(false);
                    tv_asset_verify.setText("Check");
                    tv_asset_verify.setTextColor(getResources().getColor(R.color.about_red));
                    layout_staff.setVisibility(View.GONE);
                    layout_staff_other.setVisibility(View.GONE);
                    cb_verify.setBackgroundResource(android.R.drawable.checkbox_off_background);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        cb_verify.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (rel_asset_info.getVisibility() == View.VISIBLE) {
                    if (isChecked) {
                        if (et_asset_id.getText().toString().trim().length() == 0) {
                            et_asset_id.setError("Please enter device identifier");
                            et_asset_id.requestFocus();
                            et_asset_id.requestFocusFromTouch();
                            cb_verify.setChecked(false);
                            cb_verify.setBackgroundResource(android.R.drawable.checkbox_off_background);
                        } else if (Constants.isNetworkAvailable(MainActivity.this)) {
                            cb_verify.setBackgroundResource(android.R.drawable.checkbox_on_background);
                            showAlertDialog();
                        } else {
                            cb_verify.setChecked(false);
                            layout_staff.setVisibility(View.GONE);
                            layout_staff_other.setVisibility(View.GONE);
                            Toast.makeText(MainActivity.this, "Please check your internet", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        tv_asset_verify.setText("Check");
                        tv_asset_verify.setTextColor(getResources().getColor(R.color.about_red));
                        layout_staff.setVisibility(View.GONE);
                        layout_staff_other.setVisibility(View.GONE);
                        cb_verify.setBackgroundResource(android.R.drawable.checkbox_off_background);
                    }
                }
            }
        });
        tv_asset_verify = (TextView) findViewById(R.id.tv_asset_verify);
        rel_contact_view = (RelativeLayout) findViewById(R.id.contact_view_layout);
    }

}
