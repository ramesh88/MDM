package com.thril.mdm.kiosk;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.view.WindowManager;

//Class to wake the device when screen is off
public class OnScreenOffReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(Intent.ACTION_SCREEN_OFF.equals(intent.getAction())){
          //  AppContext ctx = (AppContext) context.getApplicationContext();
            // is Kiosk Mode active?

            if(intent.getAction().equals(Intent.ACTION_SCREEN_OFF)){
              // Toast.makeText(context,"Screen on",Toast.LENGTH_SHORT).show();
                wakeUpDevice(context);

            }else if(intent.getAction().equals(Intent.ACTION_SCREEN_ON)){
             //   Toast.makeText(context,"Screen on",Toast.LENGTH_SHORT).show();
            }
            /*if(PrefUtils.isKioskModeActive(ctx)) {
                wakeUpDevice(ctx);
            }*/
        }
    }




//method to wakeup device when device is in sleep mode with parameter context
    private void wakeUpDevice(Context mContext) {
       /* KeyguardManager manager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock lock = manager.newKeyguardLock("wakeup");
        lock.disableKeyguard();*/
        KeyguardManager manager = (KeyguardManager) mContext.getSystemService(Context.KEYGUARD_SERVICE);

        if (android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.FROYO) {
            // only for gingerbread and newer versions
            ((Activity) mContext).getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
            ((Activity) mContext).getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
            ((Activity) mContext).getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);


            KeyguardManager.KeyguardLock lock = manager.newKeyguardLock("wakeup");
            lock.disableKeyguard();

        } else {

            KeyguardManager km = (KeyguardManager) mContext.getSystemService(Context.KEYGUARD_SERVICE);
            KeyguardManager.KeyguardLock keyguardLock = km.newKeyguardLock("wakeup");
            ((Activity) mContext).getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
            ((Activity) mContext).getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

            keyguardLock.disableKeyguard();
            PowerManager pm = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
            PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
                    | PowerManager.ACQUIRE_CAUSES_WAKEUP
                    | PowerManager.ON_AFTER_RELEASE
                    | PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "MyWakeLock");

            wakeLock.acquire();
            wakeLock.release();
        }
    }


}