package com.thril.mdm.databaselayer.databasemanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.thril.mdm.BuildConfig;
import com.thril.mdm.Enum.ConfigurationType;
import com.thril.mdm.Enum.SwitchType;
import com.thril.mdm.Global.ApkDownloader;
import com.thril.mdm.Global.DateUtils;
import com.thril.mdm.Global.MDMSettings;
import com.thril.mdm.Network.utils.Constants;
import com.thril.mdm.databaselayer.contractor.Contractor;
import com.thril.mdm.model.App;
import com.thril.mdm.model.AppConfiguration;
import com.thril.mdm.model.AppUsage;
import com.thril.mdm.model.BatteryUsage;
import com.thril.mdm.model.DataConsumption;
import com.thril.mdm.model.DeviceResponse;
import com.thril.mdm.model.DeviceUsage;
import com.thril.mdm.model.GeoFence;
import com.thril.mdm.model.GpsAssetTracker;
import com.thril.mdm.model.GpsTrackerViolation;
import com.thril.mdm.model.NotificationData;
import com.thril.mdm.model.PhotoResolution;
import com.thril.mdm.model.ResponseData;
import com.thril.mdm.model.SignalStrength;
import com.thril.mdm.model.StorageFreeSpace;
import com.thril.mdm.model.UserControl;
import com.thril.mdm.model.VideoResolution;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by Ram on 2/3/2018.
 */

//Class to make database operations
public class DatabaseMgr {

    public static DatabaseMgr mInstance;
    public static SQLiteDatabase mDB;
    public static Context mContext;
    private static final String TAG = "DatabaseMgr";

    //Constructor class
    private DatabaseMgr() {

    }

    //Static Method returns the instance and creates a instance for the class
    public static DatabaseMgr getInstance(Context context) {
        mContext = context;
        if (mInstance == null) {
            mInstance = new DatabaseMgr();
            Log.d(TAG, "DatabaseMgr");
            createTable(context);
        }
        return mInstance;
    }


    //method to  to intialize database object
    public static void createTable(Context context) {
        mDB = DBHelper.getInstance(context).getReadableDatabase();
    }

    //method to insert app to database with parameters list of apps
    public long insertAppList(List<App> apps) {

        long rowCount = 0;
        for (App app : apps) {
            ContentValues cv = new ContentValues();
            cv.put(Contractor.AppListTable.COLUMN_NAME_APP_TITLE, app.getTitle());
            cv.put(Contractor.AppListTable.COLUMN_NAME_APP_ID, app.getAppid());
            cv.put(Contractor.AppListTable.COLUMN_NAME_APP_TYPE, app.getType());
            cv.put(Contractor.AppListTable.COLUMN_NAME_APP_VERSION_NO, app.getVersionNo());
            cv.put(Contractor.AppListTable.COLUMN_NAME_APP_ICON_PATH, app.getIconPath());
            cv.put(Contractor.AppListTable.COLUMN_NAME_APP_APK_PATH, app.getApkPath());
            cv.put(Contractor.AppListTable.COLUMN_NAME_APP_PACKAGE_NAME, app.getPackageName());
            cv.put(Contractor.AppListTable.COLUMN_NAME_APK_ACTIVE_FROM, app.getActiveFrom());
            cv.put(Contractor.AppListTable.COLUMN_NAME_APK_ACTIVE_TO, app.getActiveTo());
            try {
                rowCount = mDB.insert(Contractor.AppListTable.TABLE_NAME, null, cv);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return rowCount;
    }

    //method to insert app to database with parameters app
    public long insertAppList(App app) {

        long rowCount = 0;
        ContentValues cv = new ContentValues();
        cv.put(Contractor.AppListTable.COLUMN_NAME_APP_TITLE, app.getTitle());
        cv.put(Contractor.AppListTable.COLUMN_NAME_APP_TYPE, app.getType());
        cv.put(Contractor.AppListTable.COLUMN_NAME_APP_VERSION_NO, app.getVersionNo());
        cv.put(Contractor.AppListTable.COLUMN_NAME_APP_ICON_PATH, loadFileFromServer(app).getIconPath());
        cv.put(Contractor.AppListTable.COLUMN_NAME_APP_APK_PATH, app.getApkPath());
        cv.put(Contractor.AppListTable.COLUMN_NAME_APP_PACKAGE_NAME, app.getPackageName());
        cv.put(Contractor.AppListTable.COLUMN_NAME_APK_ACTIVE_FROM, app.getActiveFrom());
        cv.put(Contractor.AppListTable.COLUMN_NAME_APK_ACTIVE_TO, app.getActiveTo());
        try {
            rowCount = mDB.insert(Contractor.AppListTable.TABLE_NAME, null, cv);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rowCount;
    }

    //method to insert app to database with parameters app
    public long updateAppDetailsList(App app) {

        long rowCount = 0;
        ContentValues cv = new ContentValues();
        cv.put(Contractor.AppListTable.COLUMN_NAME_APP_TITLE, app.getTitle());
        cv.put(Contractor.AppListTable.COLUMN_NAME_APP_TYPE, app.getType());
        cv.put(Contractor.AppListTable.COLUMN_NAME_APP_VERSION_NO, app.getVersionNo());
        cv.put(Contractor.AppListTable.COLUMN_NAME_APP_ICON_PATH, loadFileFromServer(app).getIconPath());
        cv.put(Contractor.AppListTable.COLUMN_NAME_APP_APK_PATH, app.getApkPath());
        cv.put(Contractor.AppListTable.COLUMN_NAME_APP_PACKAGE_NAME, app.getPackageName());
        cv.put(Contractor.AppListTable.COLUMN_NAME_APK_ACTIVE_FROM, app.getActiveFrom());
        cv.put(Contractor.AppListTable.COLUMN_NAME_APK_ACTIVE_TO, app.getActiveTo());
        try {
            rowCount = mDB.update(Contractor.AppListTable.TABLE_NAME, cv, Contractor.AppListTable.COLUMN_NAME_APP_ID + " = '" + app.getAppid() + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rowCount;
    }

    //method to update app to database with parameters app
    public long updateAppList(App app) {
        long rowCount = 0;
        ContentValues cv = new ContentValues();
        cv.put(Contractor.AppListTable.COLUMN_NAME_APP_TITLE, app.getTitle());
        cv.put(Contractor.AppListTable.COLUMN_NAME_APP_TYPE, app.getType());
        cv.put(Contractor.AppListTable.COLUMN_NAME_APP_VERSION_NO, app.getVersionNo());
        cv.put(Contractor.AppListTable.COLUMN_NAME_APP_ICON_PATH, loadFileFromServer(app).getIconPath());
        cv.put(Contractor.AppListTable.COLUMN_NAME_APP_APK_PATH, app.getApkPath());
        cv.put(Contractor.AppListTable.COLUMN_NAME_APP_PACKAGE_NAME, app.getPackageName());
        cv.put(Contractor.AppListTable.COLUMN_NAME_APK_ACTIVE_FROM, app.getActiveFrom());
        cv.put(Contractor.AppListTable.COLUMN_NAME_APK_ACTIVE_TO, app.getActiveTo());
        try {
            rowCount = mDB.update(Contractor.AppListTable.TABLE_NAME, cv, Contractor.AppListTable.COLUMN_NAME_APP_ID + " = '" + app.getAppid() + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rowCount;
    }

    //method to get app count from database
    public long getAppListRecordCount() {
        long appListCount = 0;
        Cursor cursor = mDB.query(Contractor.AppListTable.TABLE_NAME, new String[]{"*"}, null, null, null, null, null);
        if (cursor != null) {
            appListCount = cursor.getCount();
            cursor.close();
        }
        return appListCount;
    }

    //method to retrive app from database
    public List<App> retrieveAppList() {
        List<App> apps = new ArrayList<>();
        Cursor cursor = mDB.query(Contractor.AppListTable.TABLE_NAME, new String[]{"*"}, null, null, null, null, Contractor.AppListTable.COLUMN_NAME_APP_TITLE + " ASC");
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                App app = new App();
                app.setTitle(cursor.getString(cursor.getColumnIndex(Contractor.AppListTable.COLUMN_NAME_APP_TITLE)));
                app.setAppid(cursor.getString(cursor.getColumnIndex(Contractor.AppListTable.COLUMN_NAME_APP_ID)));
                app.setVersionNo(cursor.getString(cursor.getColumnIndex(Contractor.AppListTable.COLUMN_NAME_APP_VERSION_NO)));
                app.setType(cursor.getString(cursor.getColumnIndex(Contractor.AppListTable.COLUMN_NAME_APP_TYPE)));
                app.setIconPath(cursor.getString(cursor.getColumnIndex(Contractor.AppListTable.COLUMN_NAME_APP_ICON_PATH)));
                app.setApkPath(cursor.getString(cursor.getColumnIndex(Contractor.AppListTable.COLUMN_NAME_APP_APK_PATH)));
                app.setPackageName(cursor.getString(cursor.getColumnIndex(Contractor.AppListTable.COLUMN_NAME_APP_PACKAGE_NAME)));
                app.setActiveFrom(cursor.getLong(cursor.getColumnIndex(Contractor.AppListTable.COLUMN_NAME_APK_ACTIVE_FROM)));
                app.setActiveTo(cursor.getLong(cursor.getColumnIndex(Contractor.AppListTable.COLUMN_NAME_APK_ACTIVE_TO)));
                if (DateUtils.compareAppDates(app.getActiveFrom(), app.getActiveTo())) {
                    apps.add(app);
                }


            }
        }
        if (cursor != null) {
            cursor.close();
        }
        return apps;
    }

    //method to retrive app packages
    public List<String> retrieveAppPackages() {
        List<String> apps = new ArrayList<>();
        Cursor cursor = mDB.query(Contractor.AppListTable.TABLE_NAME, new String[]{"*"}, null, null, null, null, Contractor.AppListTable.COLUMN_NAME_APP_TITLE + " ASC");
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                apps.add(cursor.getString(cursor.getColumnIndex(Contractor.AppListTable.COLUMN_NAME_APP_PACKAGE_NAME)));
            }
        }
        apps.add("com.thril.mdm");
        apps.add("com.android.systemui");
        apps.add("com.google.android.packageinstaller");
        apps.add("com.android.vending");
        if (cursor != null) {
            cursor.close();
        }
        return apps;
    }

    //method to update app configuration in database with parameter appconfiguration
    public boolean updateAppConfiguration(AppConfiguration appConfiguration) {
        try {
            ContentValues cv = new ContentValues();
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_DEVICE_KEY, appConfiguration.getDevicekey());
            String gps = appConfiguration.getGps();
            String gpsUser = ConfigurationType.NA.toString();
            if (gps.equalsIgnoreCase(ConfigurationType.user.toString())) {
                gpsUser = ConfigurationType.user.toString();
                gps = SwitchType.Off.toString();
            }

            updateUserControl(Contractor.UserControl.COLUMN_LOCATION_USER, gpsUser);
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_GPS, gps);
            String mobile = appConfiguration.getMobileData();
            String mobileUser = ConfigurationType.NA.toString();
            if (mobile.equalsIgnoreCase(ConfigurationType.user.toString())) {
                mobile = SwitchType.Off.toString();
                mobileUser = ConfigurationType.user.toString();
            }
            updateUserControl(Contractor.UserControl.COLUMN_MOBILE_USER, mobileUser);
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_MOBILE_DATA, mobile);
            String bluetooth = appConfiguration.getBluetooth();
            String btUser = ConfigurationType.NA.toString();
            if (bluetooth.equalsIgnoreCase(ConfigurationType.user.toString())) {
                btUser = ConfigurationType.user.toString();
                bluetooth = SwitchType.Off.toString();
            }
            updateUserControl(Contractor.UserControl.COLUMN_BT_USER, btUser);
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_BLUETOOTH, bluetooth);
            String flight = appConfiguration.getFlightMode();
            String flightUser = ConfigurationType.NA.toString();
            if (flight.equalsIgnoreCase(ConfigurationType.user.toString())) {
                flightUser = ConfigurationType.user.toString();
                flight = SwitchType.Off.toString();
            }
            updateUserControl(Contractor.UserControl.COLUMN_AEROPLANE_USER, flightUser);
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_Flight_MODE, flight);
            String wifi = appConfiguration.getWifi();
            String wifiUser = ConfigurationType.NA.toString();
            if (wifi.equalsIgnoreCase(ConfigurationType.user.toString())) {
                wifiUser = ConfigurationType.user.toString();
                wifi = SwitchType.Off.toString();
            }
            updateUserControl(Contractor.UserControl.COLUMN_WIFI_USER, wifiUser);
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_WIFI, wifi);
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_USB, appConfiguration.getUsb());
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_OTG, appConfiguration.getOtg());
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_GPS_MODE, appConfiguration.getGpsMode());
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_COMMAND_CENTER_PHONE_NO, appConfiguration.getCommandCenterPhNo());
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_COMMAND_CENTER_EMAIL, appConfiguration.getCommandcenteremail());
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_SYNC_TIME_INTERVAL, appConfiguration.getSyncTimeInterval());
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_Assigned_Mobile_No, appConfiguration.getAssignedMobileNo());
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_IS_Dual_Sim_Allowed, appConfiguration.getIsDualSimAllowed());
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_Restrict_Device_MNC, appConfiguration.getRestrictDeviceMNC());
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_Screen_Orientation, appConfiguration.getScreenOrientation());
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_GPS_TIME_INTERVAL, appConfiguration.getGpstimeinterval());
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_DEVICE_PH_NUM, appConfiguration.getDevicePhoneNumber());
            mDB.update(Contractor.AppConfigurationTable.TABLE_NAME, cv, Contractor.AppConfigurationTable.COLUMN_NAME_DEVICE_KEY + " = '" + appConfiguration.getDevicekey() + "'", null);
            PhotoResolution photoResolution = appConfiguration.getPhotoResolution();
            if (photoResolution != null) {
                updatePhotoResolution(appConfiguration);
            }
            VideoResolution videoResolution = appConfiguration.getVideoResolution();
            if (videoResolution != null) {
                updateVideoResolution(appConfiguration);
            }
            GeoFence geoFence = appConfiguration.getGeoFence();
            if (geoFence != null) {
                updateVideoResolution(appConfiguration);
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    //method to insert app configuration with parameter app configuration
    public boolean insertAppConfiguration(AppConfiguration appConfiguration) {
        try {
            ContentValues cv = new ContentValues();
            UserControl userControl = new UserControl();
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_DEVICE_KEY, appConfiguration.getDevicekey());
            String gps = appConfiguration.getGps();
            userControl.setLocationUser(ConfigurationType.NA.toString());
            if (gps.equalsIgnoreCase(ConfigurationType.user.toString())) {
                userControl.setLocationUser(ConfigurationType.user.toString());
                gps = SwitchType.Off.toString();
            }
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_GPS, gps);
            String mobile = appConfiguration.getMobileData();
            userControl.setMobileUser(ConfigurationType.NA.toString());
            if (mobile.equalsIgnoreCase(ConfigurationType.user.toString())) {
                userControl.setMobileUser(ConfigurationType.user.toString());
                mobile = SwitchType.Off.toString();
            }
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_MOBILE_DATA, mobile);

            String bluetooth = appConfiguration.getBluetooth();
            userControl.setBtUser(ConfigurationType.NA.toString());
            if (bluetooth.equalsIgnoreCase(ConfigurationType.user.toString())) {
                userControl.setMobileUser(ConfigurationType.user.toString());
                bluetooth = SwitchType.Off.toString();
            }
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_BLUETOOTH, bluetooth);
            String flight = appConfiguration.getFlightMode();
            userControl.setAeroUser(ConfigurationType.NA.toString());
            if (flight.equalsIgnoreCase(ConfigurationType.user.toString())) {
                userControl.setAeroUser(ConfigurationType.user.toString());
                flight = SwitchType.Off.toString();
            }

            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_Flight_MODE, flight);
            String wifi = appConfiguration.getWifi();
            userControl.setWifiUser(ConfigurationType.NA.toString());
            if (wifi.equalsIgnoreCase(ConfigurationType.user.toString())) {
                userControl.setWifiUser(ConfigurationType.user.toString());
                wifi = SwitchType.Off.toString();
            }
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_WIFI, wifi);
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_USB, appConfiguration.getUsb());
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_OTG, appConfiguration.getOtg());
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_GPS_MODE, appConfiguration.getGpsMode());
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_COMMAND_CENTER_PHONE_NO, appConfiguration.getCommandCenterPhNo());
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_COMMAND_CENTER_EMAIL, appConfiguration.getCommandcenteremail());
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_SYNC_TIME_INTERVAL, appConfiguration.getSyncTimeInterval());
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_Assigned_Mobile_No, appConfiguration.getAssignedMobileNo());
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_IS_Dual_Sim_Allowed, appConfiguration.getIsDualSimAllowed());
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_Restrict_Device_MNC, appConfiguration.getRestrictDeviceMNC());
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_Screen_Orientation, appConfiguration.getScreenOrientation());
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_GPS_TIME_INTERVAL, appConfiguration.getGpstimeinterval());
            cv.put(Contractor.AppConfigurationTable.COLUMN_NAME_DEVICE_PH_NUM, appConfiguration.getDevicePhoneNumber());
            mDB.insert(Contractor.AppConfigurationTable.TABLE_NAME, null, cv);
            insertUserControl(userControl);

            if (!insertPhotoResolution(appConfiguration)) {
                return false;
            }
            if (!insertVideoResolution(appConfiguration)) {
                return false;
            }
            if (insertGeoFence(appConfiguration)) {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    //method to insert photo resultion in database
    public boolean insertPhotoResolution(AppConfiguration appConfiguration) {

        ContentValues prCv = new ContentValues();
        prCv.put(Contractor.AppConfigurationTable.COLUMN_NAME_DEVICE_KEY, appConfiguration.getDevicekey());
        prCv.put(Contractor.PhotoResolutionTable.COLUMN_NAME_WIDTH, appConfiguration.getPhotoResolution().getWidth());
        prCv.put(Contractor.PhotoResolutionTable.COLUMN_NAME_HEIGHT, appConfiguration.getPhotoResolution().getHeight());
        try {
            mDB.insert(Contractor.PhotoResolutionTable.TABLE_NAME, null, prCv);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    //method to update  photo resultion in database
    public boolean updatePhotoResolution(AppConfiguration appConfiguration) {

        ContentValues prCv = new ContentValues();
        prCv.put(Contractor.PhotoResolutionTable.COLUMN_NAME_WIDTH, appConfiguration.getPhotoResolution().getWidth());
        prCv.put(Contractor.PhotoResolutionTable.COLUMN_NAME_HEIGHT, appConfiguration.getPhotoResolution().getHeight());
        try {
            mDB.update(Contractor.PhotoResolutionTable.TABLE_NAME, prCv, Contractor.AppConfigurationTable.COLUMN_NAME_DEVICE_KEY + " = '" + appConfiguration.getDevicekey() + "'", null);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    //method to insert video resultion in database
    public boolean insertVideoResolution(AppConfiguration appConfiguration) {
        ContentValues vrCv = new ContentValues();
        vrCv.put(Contractor.AppConfigurationTable.COLUMN_NAME_DEVICE_KEY, appConfiguration.getDevicekey());
        vrCv.put(Contractor.VideoResolutionTable.COLUMN_NAME_WIDTH, appConfiguration.getVideoResolution().getWidth());
        vrCv.put(Contractor.VideoResolutionTable.COLUMN_NAME_HEIGHT, appConfiguration.getVideoResolution().getHeight());
        try {
            mDB.insert(Contractor.PhotoResolutionTable.TABLE_NAME, null, vrCv);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    //method to update video resoultion in database
    public boolean updateVideoResolution(AppConfiguration appConfiguration) {
        ContentValues vrCv = new ContentValues();
        vrCv.put(Contractor.VideoResolutionTable.COLUMN_NAME_WIDTH, appConfiguration.getVideoResolution().getWidth());
        vrCv.put(Contractor.VideoResolutionTable.COLUMN_NAME_HEIGHT, appConfiguration.getVideoResolution().getHeight());
        try {
            mDB.update(Contractor.PhotoResolutionTable.TABLE_NAME, vrCv, Contractor.AppConfigurationTable.COLUMN_NAME_DEVICE_KEY + " = '" + appConfiguration.getDevicekey() + "'", null);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    //method to insert geo fence in database
    public boolean insertGeoFence(AppConfiguration appConfiguration) {
        ContentValues gfCv = new ContentValues();
        gfCv.put(Contractor.AppConfigurationTable.COLUMN_NAME_DEVICE_KEY, appConfiguration.getDevicekey());
        gfCv.put(Contractor.GeoFenceTable.COLUMN_NAME_GEO_FENCE_LATITUDE, appConfiguration.getGeoFence().getLatitude());
        gfCv.put(Contractor.GeoFenceTable.COLUMN_NAME_GEO_FENCE_LONGITUDE, appConfiguration.getGeoFence().getLongitude());
        gfCv.put(Contractor.GeoFenceTable.COLUMN_NAME_GEO_FENCE_RANGE, appConfiguration.getGeoFence().getRange());
        try {
            mDB.insert(Contractor.GeoFenceTable.TABLE_NAME, null, gfCv);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    //method to update geo fence in database
    public boolean updateGeoFence(AppConfiguration appConfiguration) {
        ContentValues gfCv = new ContentValues();
        gfCv.put(Contractor.GeoFenceTable.COLUMN_NAME_GEO_FENCE_LATITUDE, appConfiguration.getGeoFence().getLatitude());
        gfCv.put(Contractor.GeoFenceTable.COLUMN_NAME_GEO_FENCE_LONGITUDE, appConfiguration.getGeoFence().getLongitude());
        gfCv.put(Contractor.GeoFenceTable.COLUMN_NAME_GEO_FENCE_RANGE, appConfiguration.getGeoFence().getRange());
        try {
            mDB.update(Contractor.GeoFenceTable.TABLE_NAME, gfCv, Contractor.AppConfigurationTable.COLUMN_NAME_DEVICE_KEY + " = '" + appConfiguration.getDevicekey() + "'", null);
        } catch (Exception e) {
            return false;
        }
        return true;
    }


    //method to get device key from database
    public String getDeviceKey() {
        String deviceKey = "";
        Cursor cursor = mDB.query(Contractor.AppConfigurationTable.TABLE_NAME, new String[]{"*"}, null, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                deviceKey = cursor.getString(cursor.getColumnIndex(Contractor.AppConfigurationTable.COLUMN_NAME_DEVICE_KEY));
            }
            cursor.close();
        }


        return deviceKey;
    }

    //method to retrive app configuration from database
    public AppConfiguration retrieveAppConfiguration() {
        AppConfiguration appConfiguration = null;
        Cursor cursor = mDB.query(Contractor.AppConfigurationTable.TABLE_NAME, new String[]{"*"}, null, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                appConfiguration = new AppConfiguration();
                appConfiguration.setDevicekey(cursor.getString(cursor.getColumnIndex(Contractor.AppConfigurationTable.COLUMN_NAME_DEVICE_KEY)));
                appConfiguration.setGps(cursor.getString(cursor.getColumnIndex(Contractor.AppConfigurationTable.COLUMN_NAME_GPS)));
                appConfiguration.setMobileData(cursor.getString(cursor.getColumnIndex(Contractor.AppConfigurationTable.COLUMN_NAME_MOBILE_DATA)));
                appConfiguration.setBluetooth(cursor.getString(cursor.getColumnIndex(Contractor.AppConfigurationTable.COLUMN_NAME_BLUETOOTH)));
                appConfiguration.setFlightMode(cursor.getString(cursor.getColumnIndex(Contractor.AppConfigurationTable.COLUMN_NAME_Flight_MODE)));
                appConfiguration.setWifi(cursor.getString(cursor.getColumnIndex(Contractor.AppConfigurationTable.COLUMN_NAME_WIFI)));
                appConfiguration.setUsb(cursor.getString(cursor.getColumnIndex(Contractor.AppConfigurationTable.COLUMN_NAME_USB)));
                appConfiguration.setOtg(cursor.getString(cursor.getColumnIndex(Contractor.AppConfigurationTable.COLUMN_NAME_OTG)));
                appConfiguration.setGpsMode(cursor.getInt(cursor.getColumnIndex(Contractor.AppConfigurationTable.COLUMN_NAME_GPS_MODE)));

                appConfiguration.setCommandCenterPhNo(cursor.getString(cursor.getColumnIndex(Contractor.AppConfigurationTable.COLUMN_NAME_COMMAND_CENTER_PHONE_NO)));
                appConfiguration.setCommandcenteremail(cursor.getString(cursor.getColumnIndex(Contractor.AppConfigurationTable.COLUMN_NAME_COMMAND_CENTER_EMAIL)));
                appConfiguration.setSyncTimeInterval(cursor.getInt(cursor.getColumnIndex(Contractor.AppConfigurationTable.COLUMN_NAME_SYNC_TIME_INTERVAL)));
                appConfiguration.setAssignedMobileNo(cursor.getString(cursor.getColumnIndex(Contractor.AppConfigurationTable.COLUMN_NAME_Assigned_Mobile_No)));
                appConfiguration.setIsDualSimAllowed(cursor.getString(cursor.getColumnIndex(Contractor.AppConfigurationTable.COLUMN_NAME_IS_Dual_Sim_Allowed)));

                appConfiguration.setRestrictDeviceMNC(cursor.getString(cursor.getColumnIndex(Contractor.AppConfigurationTable.COLUMN_NAME_Restrict_Device_MNC)));
                appConfiguration.setScreenOrientation(cursor.getString(cursor.getColumnIndex(Contractor.AppConfigurationTable.COLUMN_NAME_Screen_Orientation)));
                appConfiguration.setGpstimeinterval(cursor.getInt(cursor.getColumnIndex(Contractor.AppConfigurationTable.COLUMN_NAME_GPS_TIME_INTERVAL)));
                appConfiguration.setDevicePhoneNumber(cursor.getString(cursor.getColumnIndex(Contractor.AppConfigurationTable.COLUMN_NAME_DEVICE_PH_NUM)));

            }
            cursor.close();

            if (appConfiguration != null) {
                PhotoResolution photoResolution = retrievePhotoResolution();
                VideoResolution videoResolution = retrieveVideoResolution();
                GeoFence geoFence = retrieveGeoFence();
                if (photoResolution != null) {
                    appConfiguration.setPhotoResolution(photoResolution);
                }
                if (videoResolution != null) {
                    appConfiguration.setVideoResolution(videoResolution);
                }
                if (geoFence != null) {
                    appConfiguration.setGeoFence(geoFence);
                }
            }

        }

        return appConfiguration;
    }

    //method to retrive photo resolution from database
    public PhotoResolution retrievePhotoResolution() {
        PhotoResolution photoResolution = null;
        Cursor cursor = mDB.query(Contractor.PhotoResolutionTable.TABLE_NAME, new String[]{"*"}, null, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                photoResolution = new PhotoResolution();
                photoResolution.setWidth(cursor.getInt(cursor.getColumnIndex(Contractor.PhotoResolutionTable.COLUMN_NAME_WIDTH)));
                photoResolution.setWidth(cursor.getInt(cursor.getColumnIndex(Contractor.PhotoResolutionTable.COLUMN_NAME_HEIGHT)));

            }
            cursor.close();
        }
        return photoResolution;
    }

    //method to retrive video resolution from database
    public VideoResolution retrieveVideoResolution() {
        VideoResolution videoResolution = null;
        Cursor cursor = mDB.query(Contractor.VideoResolutionTable.TABLE_NAME, new String[]{"*"}, null, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                videoResolution = new VideoResolution();
                videoResolution.setWidth(cursor.getInt(cursor.getColumnIndex(Contractor.VideoResolutionTable.COLUMN_NAME_WIDTH)));
                videoResolution.setWidth(cursor.getInt(cursor.getColumnIndex(Contractor.VideoResolutionTable.COLUMN_NAME_HEIGHT)));

            }
            cursor.close();
        }
        return videoResolution;
    }

    //method to retrive geo fence from database
    public GeoFence retrieveGeoFence() {
        GeoFence geoFence = null;
        Cursor cursor = mDB.query(Contractor.GeoFenceTable.TABLE_NAME, new String[]{"*"}, null, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                geoFence = new GeoFence();
                geoFence.setLatitude(cursor.getString(cursor.getColumnIndex(Contractor.GeoFenceTable.COLUMN_NAME_GEO_FENCE_LATITUDE)));
                geoFence.setLongitude(cursor.getString(cursor.getColumnIndex(Contractor.GeoFenceTable.COLUMN_NAME_GEO_FENCE_LONGITUDE)));
                geoFence.setRange(cursor.getInt(cursor.getColumnIndex(Contractor.GeoFenceTable.COLUMN_NAME_GEO_FENCE_RANGE)));

            }
            cursor.close();
        }
        return geoFence;
    }

    //method to update coloumn in database with parameters coloumn name , columnvalue and type
    public long updateColumn(String columnName, String columnValue, boolean type) {

        ContentValues cv = new ContentValues();
        if (type) {
            cv.put(columnName, columnValue);
        } else {
            cv.put(columnName, Integer.parseInt(columnValue));
        }
        return mDB.update(Contractor.AppConfigurationTable.TABLE_NAME, cv, Contractor.COLUMN_COMMON_ID + " = " + 1, null);
    }

    //method to insert gps tracker viloation in database with parameter gpstracker violation
    public void insertGpsTrackerViolationRecords(GpsTrackerViolation gpsTrackerViolation) {

        List<GpsTrackerViolation> gpsAssetTrackers = getGpsTrackerViolationRecords();
        AppConfiguration appConfiguration = retrieveAppConfiguration();
        boolean flag = false;
        if (gpsAssetTrackers.size() > 0) {
            GpsTrackerViolation assetTracker = gpsAssetTrackers.get(gpsAssetTrackers.size() - 1);
            Long londate = Long.parseLong(assetTracker.getDeviatedEndDateTime());
            if ((System.currentTimeMillis() - londate) >= appConfiguration.getGpstimeinterval()) {
                flag = true;
            }
        } else {
            flag = true;
        }
        if (flag) {
            ContentValues cv = new ContentValues();
            // Log.d("Latitude",gpsTrackerViolation.getLatitude());
            cv.put(Contractor.GpsTrackerViolation.COLUMN_NAME_LATITUDE, gpsTrackerViolation.getLatitude());
            cv.put(Contractor.GpsTrackerViolation.COLUMN_NAME_LONGITUDE, gpsTrackerViolation.getLongitude());
            cv.put(Contractor.GpsTrackerViolation.COLUMN_NAME_DATE_TIME_GMT, gpsTrackerViolation.getDeviatedEndDateTime());
            mDB.insert(Contractor.GpsTrackerViolation.TABLE_NAME, null, cv);
        }
    }

    //method to retrive gps tracker viloations in database
    public List<GpsTrackerViolation> getGpsTrackerViolationRecords() {
        List<GpsTrackerViolation> gpsTrackerViolations = new ArrayList<>();
        List<String> date = new ArrayList<>();
        Cursor cursor = mDB.query(Contractor.GpsTrackerViolation.TABLE_NAME, new String[]{"*"}, null, null, null, null, Contractor.GpsTrackerViolation.COLUMN_NAME_DATE_TIME_GMT + " ASC");
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                GpsTrackerViolation gpsTrackerViolation = new GpsTrackerViolation();
                gpsTrackerViolation.setLatitude(cursor.getString(cursor.getColumnIndex(Contractor.GpsTrackerViolation.COLUMN_NAME_LATITUDE)));
                Log.d("latitude in gps", gpsTrackerViolation.getLatitude());
                gpsTrackerViolation.setLongitude(cursor.getString(cursor.getColumnIndex(Contractor.GpsTrackerViolation.COLUMN_NAME_LONGITUDE)));
                gpsTrackerViolation.setDeviatedEndDateTime(cursor.getString(cursor.getColumnIndex(Contractor.GpsTrackerViolation.COLUMN_NAME_DATE_TIME_GMT)));
                Long londate = Long.parseLong(cursor.getString(cursor.getColumnIndex(Contractor.GpsTrackerViolation.COLUMN_NAME_DATE_TIME_GMT)));
                String sdate = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss").format(new Date(londate));
                if (!date.contains(sdate)) {
                    date.add(sdate);
                    gpsTrackerViolations.add(gpsTrackerViolation);
                }

            }
            cursor.close();
        }
        return gpsTrackerViolations;
    }

    //method to insert battery status records to database with parameter batter usage
    public void insertBatteryStatusRecords(BatteryUsage batteryUsage) {
        ContentValues cv = new ContentValues();
        cv.put(Contractor.BatteryUsage.COLUMN_NAME_BATTERY_STATUS, batteryUsage.getBatteryStatus());
        cv.put(Contractor.BatteryUsage.COLUMN_NAME_BATTERY_PERCENTAGE, batteryUsage.getBatteryPercentage());
        cv.put(Contractor.BatteryUsage.COLUMN_NAME_DATE_TIME_GMT, batteryUsage.getDateTime());
        mDB.insert(Contractor.BatteryUsage.TABLE_NAME, null, cv);
    }

    //method to retrivebattery usage records in database
    public List<BatteryUsage> getBatteryStatusRecords() {
        List<BatteryUsage> batteryUsages = new ArrayList<>();
        Cursor cursor = mDB.query(Contractor.BatteryUsage.TABLE_NAME, new String[]{"*"}, null, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                BatteryUsage batteryUsage = new BatteryUsage();
                batteryUsage.setBatteryStatus(cursor.getString(cursor.getColumnIndex(Contractor.BatteryUsage.COLUMN_NAME_BATTERY_STATUS)));
                batteryUsage.setBatteryPercentage(cursor.getString(cursor.getColumnIndex(Contractor.BatteryUsage.COLUMN_NAME_BATTERY_PERCENTAGE)));
                batteryUsage.setDateTime(cursor.getString(cursor.getColumnIndex(Contractor.GpsTrackerViolation.COLUMN_NAME_DATE_TIME_GMT)));

                batteryUsages.add(batteryUsage);
            }
            cursor.close();
        }
        return batteryUsages;
    }

    //method to insert signal strength records to database with parameter signal strength
    public void insertSignalStrengthRecords(SignalStrength signalStrength) {
        ContentValues cv = new ContentValues();
        cv.put(Contractor.SignalStrength.COLUMN_NAME_GSM_PROVIDER, signalStrength.getGmsProvider());
        cv.put(Contractor.SignalStrength.COLUMN_NAME_SIGNAL_STRENGTH, signalStrength.getSignalStrength());
        cv.put(Contractor.SignalStrength.COLUMN_NAME_DATE_TIME_GMT, signalStrength.getDateTime());
        mDB.insert(Contractor.SignalStrength.TABLE_NAME, null, cv);
    }

    //method to SignalStrength records in database
    public List<SignalStrength> getSignalStrengthRecords() {
        List<SignalStrength> signalStrengths = new ArrayList<>();
        Cursor cursor = mDB.query(Contractor.SignalStrength.TABLE_NAME, new String[]{"*"}, null, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                SignalStrength signalStrength = new SignalStrength();
                signalStrength.setGmsProvider(cursor.getString(cursor.getColumnIndex(Contractor.SignalStrength.COLUMN_NAME_GSM_PROVIDER)));
                signalStrength.setSignalStrength(cursor.getString(cursor.getColumnIndex(Contractor.SignalStrength.COLUMN_NAME_SIGNAL_STRENGTH)));
                signalStrength.setDateTime(cursor.getString(cursor.getColumnIndex(Contractor.SignalStrength.COLUMN_NAME_DATE_TIME_GMT)));

                signalStrengths.add(signalStrength);
            }
            cursor.close();
        }
        return signalStrengths;
    }

    //method to insert app usage records to database with parameter appUsage
    public void insertAppUsageRecords(AppUsage appUsage) {
        ContentValues cv = new ContentValues();
        cv.put(Contractor.AppUsage.COLUMN_NAME_APP_ID, appUsage.getAppID());
        cv.put(Contractor.AppUsage.COLUMN_NAME_APP_START_DATE_TIME, appUsage.getAppStartDateTime());
        cv.put(Contractor.AppUsage.COLUMN_NAME_APP_STOP_DATE_TIME, appUsage.getAppStopDateTime());
        mDB.insert(Contractor.AppUsage.TABLE_NAME, null, cv);
    }

    //method to AppUsage records in database
    public List<AppUsage> getAppUsageRecords() {
        List<AppUsage> appUsages = new ArrayList<>();
        Cursor cursor = mDB.query(Contractor.AppUsage.TABLE_NAME, new String[]{"*"}, null, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                AppUsage appUsage = new AppUsage();
                appUsage.setAppID(cursor.getString(cursor.getColumnIndex(Contractor.AppUsage.COLUMN_NAME_APP_ID)));
                appUsage.setAppStartDateTime(cursor.getString(cursor.getColumnIndex(Contractor.AppUsage.COLUMN_NAME_APP_START_DATE_TIME)));
                appUsage.setAppStopDateTime(cursor.getString(cursor.getColumnIndex(Contractor.AppUsage.COLUMN_NAME_APP_STOP_DATE_TIME)));

                appUsages.add(appUsage);
            }
            cursor.close();
        }
        return appUsages;
    }

    //method to insert data conusmption usage records to database with parameter dataConsumption
    public void insertDataConsumptionRecords(DataConsumption dataConsumption) {
        ContentValues cv = new ContentValues();
        cv.put(Contractor.DataConsumption.COLUMN_NAME_DATA_TYPE, dataConsumption.getDataType());
        cv.put(Contractor.DataConsumption.COLUMN_NAME_DATA_SOURCE, dataConsumption.getDataSource());
        cv.put(Contractor.DataConsumption.COLUMN_NAME_DATA_UPLOAD, dataConsumption.getDataUpload());
        cv.put(Contractor.DataConsumption.COLUMN_NAME_DATA_DOWNLOAD, dataConsumption.getDataDownload());
        cv.put(Contractor.DataConsumption.COLUMN_NAME_DATE_TIME_GMT, dataConsumption.getDateTime());
        mDB.insert(Contractor.DataConsumption.TABLE_NAME, null, cv);
    }

    //method to DataConsumption records in database
    public List<DataConsumption> getDataConsumptionRecords() {
        List<DataConsumption> dataConsumptions = new ArrayList<>();
        Cursor cursor = mDB.query(Contractor.DataConsumption.TABLE_NAME, new String[]{"*"}, null, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                DataConsumption dataConsumption = new DataConsumption();
                dataConsumption.setDataType(cursor.getString(cursor.getColumnIndex(Contractor.DataConsumption.COLUMN_NAME_DATA_TYPE)));
                dataConsumption.setDataSource(cursor.getString(cursor.getColumnIndex(Contractor.DataConsumption.COLUMN_NAME_DATA_SOURCE)));
                dataConsumption.setDataUpload(cursor.getString(cursor.getColumnIndex(Contractor.DataConsumption.COLUMN_NAME_DATA_UPLOAD)));
                dataConsumption.setDataDownload(cursor.getString(cursor.getColumnIndex(Contractor.DataConsumption.COLUMN_NAME_DATA_DOWNLOAD)));
                dataConsumption.setDateTime(cursor.getString(cursor.getColumnIndex(Contractor.DataConsumption.COLUMN_NAME_DATE_TIME_GMT)));

                dataConsumptions.add(dataConsumption);
            }
            cursor.close();
        }
        return dataConsumptions;
    }

    //method to insert device usage records to database with parameter deviceUsage
    public void insertDeviceUsageRecords(DeviceUsage deviceUsage) {
        ContentValues cv = new ContentValues();
        cv.put(Contractor.DeviceUsage.COLUMN_NAME_SWITCH_ON_DATE_TIME_GMT, deviceUsage.getSwitchOnDateTime());
        cv.put(Contractor.DeviceUsage.COLUMN_NAME_SWITCH_OFF_DATE_TIME_GMT, deviceUsage.getSwitchOffDateTime());
        mDB.insert(Contractor.DeviceUsage.TABLE_NAME, null, cv);
    }

    //method to DeviceUsage records in database
    public List<DeviceUsage> getDeviceUsageRecords() {
        List<DeviceUsage> deviceUsages = new ArrayList<>();
        Cursor cursor = mDB.query(Contractor.DeviceUsage.TABLE_NAME, new String[]{"*"}, null, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                DeviceUsage deviceUsage = new DeviceUsage();
                String deviceOnTime = cursor.getString(cursor.getColumnIndex(Contractor.DeviceUsage.COLUMN_NAME_SWITCH_ON_DATE_TIME_GMT));
                String deviceOffTime = cursor.getString(cursor.getColumnIndex(Contractor.DeviceUsage.COLUMN_NAME_SWITCH_OFF_DATE_TIME_GMT));
                if (deviceOnTime != null) {
                    deviceUsage.setSwitchOnDateTime(deviceOnTime);
                }
                if (deviceOffTime != null) {
                    deviceUsage.setSwitchOffDateTime(deviceOffTime);
                }
                deviceUsages.add(deviceUsage);
            }
            cursor.close();
        }
        return deviceUsages;
    }

    //method to delete notication from  database with parameter not_id
    public void deleteNotification(String not_id) {
        mDB.delete(Contractor.NotificationData.TABLE_NAME, Contractor.NotificationData.COLUMN_NAME_NOTIFICATION_ID + " = '" + not_id + "'", null);
    }

    //method to delete all notications from  database
    public void deleteAllNotification() {
        mDB.delete(Contractor.NotificationData.TABLE_NAME, null, null);
    }

    //method to delete device usage from  database
    public void deleteDeviceUsageRecords() {
        mDB.delete(Contractor.DeviceUsage.TABLE_NAME, null, null);
    }

    //method to delete app usage from  database
    public void deleteAppUsageRecords() {
        mDB.delete(Contractor.AppUsage.TABLE_NAME, null, null);
    }

    //method to delete signal strengths from  database
    public void deleteSignalStrengthRecords() {
        mDB.delete(Contractor.SignalStrength.TABLE_NAME, null, null);
    }

    //method to delete battery usage from  database
    public void deleteBatteryStatusRecords() {
        mDB.delete(Contractor.BatteryUsage.TABLE_NAME, null, null);
    }

    //method to delete gps tracker violation from  database
    public void deleteGpsTrackerViolationRecords() {
        mDB.delete(Contractor.GpsTrackerViolation.TABLE_NAME, null, null);
    }

    //method to delete gps asset from  database
    public void deleteGpsAsestTracker() {
        mDB.delete(Contractor.GpsAssetTracker.TABLE_NAME, null, null);
    }

    //method to delete data consumption usage from  database
    public void deleteDataConsumptionRecords() {
        mDB.delete(Contractor.DataConsumption.TABLE_NAME, null, null);
    }

    //method to insert notification to database with parameter notificationData
    public void insertNotificationData(NotificationData notificationData) {
        ContentValues cv = new ContentValues();
        cv.put(Contractor.NotificationData.COLUMN_NAME_NOTIFICATION_ID, notificationData.getId());
        cv.put(Contractor.NotificationData.COLUMN_NAME_NOTIFICATION_TITLE, notificationData.getTitle());
        cv.put(Contractor.NotificationData.COLUMN_NAME_NOTIFICATION_CONTENT, notificationData.getContent());
        cv.put(Contractor.NotificationData.COLUMN_NAME_NOTIFICATION_DATE, notificationData.getNotificationDate());
        cv.put(Contractor.NotificationData.COLUMN_NAME_NOTIFICATION_TIME, notificationData.getNotificationTime());
        mDB.insert(Contractor.NotificationData.TABLE_NAME, null, cv);
    }

    //method to get notifcation count from database
    public int getNotificationDataCount() {
        int count = 0;
        Cursor cursor = mDB.query(Contractor.NotificationData.TABLE_NAME, new String[]{"*"}, null, null, null, null, null);
        if (cursor != null) {
            count = cursor.getCount();
            cursor.close();
        }
        return count;
    }

    //method to get notifications to database
    public List<NotificationData> getNotificationData() {
        List<NotificationData> notificationDataList = new ArrayList<>();
        Cursor cursor = mDB.query(Contractor.NotificationData.TABLE_NAME, new String[]{"*"}, null, null, null, null, Contractor.NotificationData.COLUMN_NAME_NOTIFICATION_DATE + " DESC");
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                NotificationData notificationData = new NotificationData();
                notificationData.setId(cursor.getString(cursor.getColumnIndex(Contractor.NotificationData.COLUMN_NAME_NOTIFICATION_ID)));
                notificationData.setTitle(cursor.getString(cursor.getColumnIndex(Contractor.NotificationData.COLUMN_NAME_NOTIFICATION_TITLE)));
                notificationData.setContent(cursor.getString(cursor.getColumnIndex(Contractor.NotificationData.COLUMN_NAME_NOTIFICATION_CONTENT)));
                String date = cursor.getString(cursor.getColumnIndex(Contractor.NotificationData.COLUMN_NAME_NOTIFICATION_DATE)).trim();
                if (date.indexOf(" ") != -1) {
                    date = date.substring(0, date.indexOf(" "));
                }
                notificationData.setNotificationDate(date);
                notificationData.setNotificationTime(cursor.getString(cursor.getColumnIndex(Contractor.NotificationData.COLUMN_NAME_NOTIFICATION_TIME)));
                notificationDataList.add(notificationData);
                Log.e("date", cursor.getString(cursor.getColumnIndex(Contractor.NotificationData.COLUMN_NAME_NOTIFICATION_DATE)));
            }
            cursor.close();
        }
        return notificationDataList;


    }

    //method to insert user control to database with parameter usercontrol
    public void insertUserControl(UserControl userControl) {
        ContentValues cv = new ContentValues();
        cv.put(Contractor.UserControl.COLUMN_LOCATION_USER, userControl.getLocationUser());
        cv.put(Contractor.UserControl.COLUMN_MOBILE_USER, userControl.getMobileUser());
        cv.put(Contractor.UserControl.COLUMN_AEROPLANE_USER, userControl.getAeroUser());
        cv.put(Contractor.UserControl.COLUMN_BT_USER, userControl.getBtUser());
        cv.put(Contractor.UserControl.COLUMN_WIFI_USER, userControl.getWifiUser());
        mDB.insert(Contractor.UserControl.TABLE_NAME, null, cv);
    }

    //method to get user control from database
    public UserControl getUserControl() {
        Cursor cursor = mDB.query(Contractor.UserControl.TABLE_NAME, new String[]{"*"}, null, null, null, null, null);
        UserControl userControl = null;
        if (cursor != null && cursor.getCount() > 0) {
            userControl = new UserControl();
            while (cursor.moveToNext()) {
                userControl.setLocationUser(cursor.getString(cursor.getColumnIndex(Contractor.UserControl.COLUMN_LOCATION_USER)));
                userControl.setMobileUser(cursor.getString(cursor.getColumnIndex(Contractor.UserControl.COLUMN_MOBILE_USER)));
                userControl.setAeroUser(cursor.getString(cursor.getColumnIndex(Contractor.UserControl.COLUMN_AEROPLANE_USER)));
                userControl.setBtUser(cursor.getString(cursor.getColumnIndex(Contractor.UserControl.COLUMN_BT_USER)));
                userControl.setWifiUser(cursor.getString(cursor.getColumnIndex(Contractor.UserControl.COLUMN_WIFI_USER)));

            }
            cursor.close();
        }

        return userControl;
    }


    //method to update user control to database with parameters suercolmn  and user value
    public void updateUserControl(String userColumn, String userValue) {
        ContentValues cv = new ContentValues();
        cv.put(userColumn, userValue);
        mDB.update(Contractor.UserControl.TABLE_NAME, cv, null, null);
    }

    //method to get user control count from db
    public long getUserControlCount() {
        long count = 0;
        Cursor cursor = mDB.query(Contractor.UserControl.TABLE_NAME, new String[]{"*"}, null, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            count = 1;
        }
        cursor.close();

        return count;

    }

    //method to insert gps asset postion in database with parameter gpsassettracker
    public void insertGpsAssetTrackerPosition(GpsAssetTracker gpsAssetTracker) {
        List<GpsAssetTracker> gpsAssetTrackers = getGpsAssetTrackerPosition();
        AppConfiguration appConfiguration = retrieveAppConfiguration();
        boolean flag = false;
        if (gpsAssetTrackers.size() > 0) {
            GpsAssetTracker assetTracker = gpsAssetTrackers.get(gpsAssetTrackers.size() - 1);
            Long londate = Long.parseLong(assetTracker.getDateTime());
            if ((System.currentTimeMillis() - londate) >= appConfiguration.getGpstimeinterval()) {
                flag = true;
            }
        } else {
            flag = true;
        }
        if (flag) {
            ContentValues cv = new ContentValues();
            cv.put(Contractor.GpsAssetTracker.COLUMN_NAME_LATITUDE, gpsAssetTracker.getLatitude());
            cv.put(Contractor.GpsAssetTracker.COLUMN_NAME_LONGITUDE, gpsAssetTracker.getLongitude());
            cv.put(Contractor.GpsAssetTracker.COLUMN_NAME_DATE_TIME_LONG_GMT, gpsAssetTracker.getDateTime());
            mDB.insert(Contractor.GpsAssetTracker.TABLE_NAME, null, cv);
        }
    }

    //method to get gps asset postions from database
    public List<GpsAssetTracker> getGpsAssetTrackerPosition() {
        List<GpsAssetTracker> gpsAssetTrackers = new ArrayList<>();
        List<String> date = new ArrayList<>();
        Cursor cursor = mDB.query(Contractor.GpsAssetTracker.TABLE_NAME, new String[]{"*"}, null, null, null, null, Contractor.GpsAssetTracker.COLUMN_NAME_DATE_TIME_LONG_GMT + " ASC");
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                GpsAssetTracker gps = new GpsAssetTracker();
                gps.setLatitude(cursor.getString(cursor.getColumnIndex(Contractor.GpsAssetTracker.COLUMN_NAME_LATITUDE)));
                gps.setLongitude(cursor.getString(cursor.getColumnIndex(Contractor.GpsAssetTracker.COLUMN_NAME_LONGITUDE)));
                gps.setDateTime(cursor.getString(cursor.getColumnIndex(Contractor.GpsAssetTracker.COLUMN_NAME_DATE_TIME_LONG_GMT)));
                Long londate = Long.parseLong(cursor.getString(cursor.getColumnIndex(Contractor.GpsAssetTracker.COLUMN_NAME_DATE_TIME_LONG_GMT)));
                String sdate = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss").format(new Date(londate));
                if (!date.contains(sdate)) {
                    date.add(sdate);
                    gpsAssetTrackers.add(gps);
                }

            }
            cursor.close();
        }
        return gpsAssetTrackers;
    }


    //method to update response data in database with parameter responsedata
    public void updateResponseToDB(ResponseData responseData) {
        DatabaseMgr databaseMgr = DatabaseMgr.getInstance(Constants.getmContext());
        if (responseData != null) {
            List<App> appList = responseData.getApp();
            if (!appList.isEmpty()) {
                for (App app : appList) {
                    if (app.getType().equalsIgnoreCase("New")) {
                        databaseMgr.insertAppList(app);
                    } else if (app.getType().equalsIgnoreCase("appupdate")) {
                        databaseMgr.updateAppList(app);
                    } else {
                        databaseMgr.updateAppDetailsList(app);
                    }
                }
            }
            AppConfiguration appConfiguration = responseData.getAppConfiguration();
            if (appConfiguration != null) {
                databaseMgr.updateAppConfiguration(appConfiguration);
            }
        }

    }

    //method to load icon path from server to a app with parameter app
    public static App loadFileFromServer(App app) {
        try {
            app.setIconPath(new FileDownloader(app.getIconPath(), true).execute().get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return app;
    }

    //method to download icons from databsse
    public static class FileDownloader extends AsyncTask<Void, Void, String> {
        String urlPath = "";
        boolean flag;


        public FileDownloader(String url, boolean fileFlag) {
            urlPath = url;
            flag = fileFlag;
            Log.d("Url path:", urlPath);
            Log.d("flag", "" + flag);
        }

        @Override
        protected String doInBackground(Void... params) {
            return ApkDownloader.getFileOrApk(urlPath, flag);
        }
    }


    //method to get device response form database
    public DeviceResponse getDeviceResponse() {
        DeviceResponse deviceResponse = new DeviceResponse();
        deviceResponse.setDeviceKey(getDeviceKey());
        List<DeviceUsage> deviceUsage = getDeviceUsageRecords();
        List<AppUsage> appUsages = getAppUsageRecords();
        List<GpsTrackerViolation> gpsTrackerViolations = getGpsTrackerViolationRecords();
        List<GpsAssetTracker> gpsAssetTrackers = getGpsAssetTrackerPosition();
        List<BatteryUsage> batteryUsages = getBatteryStatusRecords();
        List<SignalStrength> signalStrengths = getSignalStrengthRecords();
        List<DataConsumption> dataConsumptions = getDataConsumptionRecords();
        List<StorageFreeSpace> storageFreeSpaces = new ArrayList<>();
        storageFreeSpaces.add(Constants.getStorageFreeSpace());
        if (!batteryUsages.isEmpty()) {
            deviceResponse.setBatteryUsage(batteryUsages);
        }
        if (!signalStrengths.isEmpty()) {
            deviceResponse.setSignalStrength(signalStrengths);
        }
        if (!dataConsumptions.isEmpty()) {
            deviceResponse.setDataConsumption(dataConsumptions);
        }
        if (!deviceUsage.isEmpty()) {
            deviceResponse.setDeviceUsage(deviceUsage);
        }
        if (!appUsages.isEmpty()) {
            deviceResponse.setAppUsage(appUsages);
        }
        if (!gpsAssetTrackers.isEmpty()) {
            deviceResponse.setGpsAssetTracker(gpsAssetTrackers);
        }
        if (!gpsTrackerViolations.isEmpty()) {
            deviceResponse.setGpsTrackerViolation(gpsTrackerViolations);
        }
        if (!storageFreeSpaces.isEmpty()) {
            deviceResponse.setStorageFreeSpace(storageFreeSpaces);
        }
        String fcm_token = MDMSettings.getFireBaseToken();
        if (fcm_token != null) {
            deviceResponse.setFcm_token(fcm_token);
        }
        String mdm_appversion = BuildConfig.VERSION_NAME;
        if (mdm_appversion != null) {
            deviceResponse.setMDMAppVersion(mdm_appversion);
        }
        Log.d("Device Response", new Gson().toJson(deviceResponse));
        return deviceResponse;
    }

    //method to delete device responses tables
    public void deleteDeviceResponseTable() {
        deleteBatteryStatusRecords();
        deleteDataConsumptionRecords();
        deleteSignalStrengthRecords();
        deleteDeviceUsageRecords();
        deleteAppUsageRecords();
        deleteGpsAsestTracker();
        deleteGpsTrackerViolationRecords();

    }


}
