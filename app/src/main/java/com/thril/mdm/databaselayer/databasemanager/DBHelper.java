package com.thril.mdm.databaselayer.databasemanager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.thril.mdm.databaselayer.contractor.Contractor;

/**
 * Created by Ram on 2/3/2018.
 */

//class to create db and perform db operations
public class DBHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "MDM";
    public static final int DB_VERSION = 5;
    private static DBHelper instance;
    private static final String TAG = "DBHelper";


    //constructor class with parameter context
    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        Log.d(TAG, "Constructor");
    }

    //Static Method returns the instance and creates a instance for the class
    public static DBHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DBHelper(context);
            Log.d(TAG, "DBHelper");
        }
        return instance;

    }


    @Override //overriden method of sqlite helper to create tables
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Contractor.AppListTable.APP_LIST_QUERY);
        db.execSQL(Contractor.AppConfigurationTable.APP_CONFIGURATION_QUERY);
        db.execSQL(Contractor.GeoFenceTable.GEO_FENCE_QUERY);
        db.execSQL(Contractor.PhotoResolutionTable.PHOTO_RESOLUTION_QUERY);
        db.execSQL(Contractor.VideoResolutionTable.VIDEO_RESOLUTION_QUERY);
        db.execSQL(Contractor.GpsTrackerViolation.GPS_TRACKER_VIOLATION_QUERY);
        db.execSQL(Contractor.BatteryUsage.BATTERY_USAGE_QUERY);
        db.execSQL(Contractor.SignalStrength.SIGNAL_STRENGTH_QUERY);
        db.execSQL(Contractor.DataConsumption.DATA_CONSUMPTION_QUERY);
        db.execSQL(Contractor.AppUsage.APP_USAGE_QUERY);
        db.execSQL(Contractor.DeviceUsage.DEVICE_USAGE_QUERY);
        db.execSQL(Contractor.GpsAssetTracker.GPS_ASSET_TRACKER_QUERY);
        db.execSQL(Contractor.NotificationData.NOTIFICATION_DATA_QUERY);
        db.execSQL(Contractor.UserControl.USER_CONTROL_QUERY);
        Log.d(TAG, "Table Created");

    }

    @Override //overriden method of sqlite helper to upgrade tables
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Contractor.AppListTable.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Contractor.AppConfigurationTable.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Contractor.GeoFenceTable.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Contractor.PhotoResolutionTable.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Contractor.VideoResolutionTable.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Contractor.GpsTrackerViolation.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Contractor.BatteryUsage.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Contractor.SignalStrength.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Contractor.DataConsumption.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Contractor.AppUsage.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Contractor.DeviceUsage.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Contractor.GpsAssetTracker.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Contractor.NotificationData.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Contractor.UserControl.TABLE_NAME);
        onCreate(db);
    }
}
