package com.thril.mdm.databaselayer.contractor;

/**
 * Created by Ram on 2/3/2018.
 */

//Class for declaration database tables and columns
public class Contractor {

    public static final String COLUMN_TYPE_TEXT = " TEXT";
    public static final String COLUMN_TYPE_INTEGER = " INTEGER";
    public static final String COMA_SEP = " ,";

    public static final String COLUMN_COMMON_ID = "_ID";


    //Table for Apps
    public static class AppListTable {
        public static final String TABLE_NAME = "AppList";
        public static final String COLUMN_NAME_APP_TITLE = "AppTitle";
        public static final String COLUMN_NAME_APP_ID="AppId";
        public static final String COLUMN_NAME_APP_VERSION_NO = "AppVersionNo";
        public static final String COLUMN_NAME_APP_TYPE = "AppType";
        public static final String COLUMN_NAME_APP_ICON_PATH = "AppIconPath";
        public static final String COLUMN_NAME_APP_PACKAGE_NAME = "AppPckgName";
        public static final String COLUMN_NAME_APP_APK_PATH = "ApkPath";
        public static final String COLUMN_NAME_APK_ACTIVE_FROM = "ApkActiveFrom";
        public static final String COLUMN_NAME_APK_ACTIVE_TO = "ApkActiveTo";

        //Query to create app list table
        public static final String APP_LIST_QUERY = "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_COMMON_ID + COLUMN_TYPE_INTEGER + " PRIMARY KEY AUTOINCREMENT" + COMA_SEP +
                COLUMN_NAME_APP_TITLE + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_APP_ID + COLUMN_TYPE_TEXT +COMA_SEP +
                COLUMN_NAME_APP_VERSION_NO + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_APP_TYPE + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_APP_ICON_PATH + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_APP_APK_PATH + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_APP_PACKAGE_NAME + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_APK_ACTIVE_FROM + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_APK_ACTIVE_TO + COLUMN_TYPE_TEXT + " )";
    }

    //Table for App Configuration
    public static class AppConfigurationTable {

        public static final String TABLE_NAME = "AppConfiguration";
        public static final String COLUMN_NAME_DEVICE_KEY="DeviceKey";
        public static final String COLUMN_NAME_GPS = "gps";
        public static final String COLUMN_NAME_MOBILE_DATA = "MobileData";
        public static final String COLUMN_NAME_BLUETOOTH = "BlueTooth";
        public static final String COLUMN_NAME_Flight_MODE = "FlightMode";
        public static final String COLUMN_NAME_WIFI = "WiFi";
        public static final String COLUMN_NAME_USB = "Usb";
        public static final String COLUMN_NAME_OTG = "Otg";
        public static final String COLUMN_NAME_GPS_MODE = "GpsMode";
        public static final String COLUMN_NAME_COMMAND_CENTER_PHONE_NO = "CommandCenterPhNo";
        public static final String COLUMN_NAME_COMMAND_CENTER_EMAIL = "CommandCenterEmail";
        public static final String COLUMN_NAME_SYNC_TIME_INTERVAL = "SynchTimeInterval";
        public static final String COLUMN_NAME_Assigned_Mobile_No = "AssignedMobileNo";
        public static final String COLUMN_NAME_IS_Dual_Sim_Allowed = "IsDualSimAllowed";
        public static final String COLUMN_NAME_Restrict_Device_MNC = "RestrictDeviceMNC";
        public static final String COLUMN_NAME_Screen_Orientation = "ScreenOrientation";
        public static final String COLUMN_NAME_GPS_TIME_INTERVAL = "GPSTimeInterval";

        public static final String COLUMN_NAME_DEVICE_PH_NUM = "DevicePhoneNumber";

        //Query to create app configuration table
        public static final String APP_CONFIGURATION_QUERY = "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_COMMON_ID + COLUMN_TYPE_INTEGER + " PRIMARY KEY AUTOINCREMENT" + COMA_SEP +
                COLUMN_NAME_DEVICE_KEY +COLUMN_TYPE_TEXT +COMA_SEP +
                COLUMN_NAME_GPS + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_MOBILE_DATA + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_BLUETOOTH + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_Flight_MODE + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_WIFI + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_USB + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_OTG + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_GPS_MODE + COLUMN_TYPE_INTEGER + COMA_SEP +
                COLUMN_NAME_COMMAND_CENTER_PHONE_NO + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_SYNC_TIME_INTERVAL + COLUMN_TYPE_INTEGER + COMA_SEP +
                COLUMN_NAME_Assigned_Mobile_No + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_IS_Dual_Sim_Allowed + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_Restrict_Device_MNC + COLUMN_TYPE_INTEGER + COMA_SEP +
                COLUMN_NAME_COMMAND_CENTER_EMAIL + COLUMN_TYPE_INTEGER + COMA_SEP +
                COLUMN_NAME_GPS_TIME_INTERVAL + COLUMN_TYPE_INTEGER + COMA_SEP +
                COLUMN_NAME_DEVICE_PH_NUM + COLUMN_TYPE_INTEGER + COMA_SEP +
                COLUMN_NAME_Screen_Orientation + COLUMN_TYPE_TEXT + " )";

    }

    //Table for Geofence
       public static class GeoFenceTable {
        public static final String TABLE_NAME = "AppGeoFence";
        public static final String COLUMN_NAME_GEO_FENCE_LATITUDE = "Latitude";
        public static final String COLUMN_NAME_GEO_FENCE_LONGITUDE = "Longitude";
        public static final String COLUMN_NAME_GEO_FENCE_RANGE = "Range";

        //Query to create geofence table
        public static final String GEO_FENCE_QUERY = "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_COMMON_ID + COLUMN_TYPE_INTEGER + " PRIMARY KEY AUTOINCREMENT" + COMA_SEP +
                AppConfigurationTable.COLUMN_NAME_DEVICE_KEY + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_GEO_FENCE_LATITUDE + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_GEO_FENCE_LONGITUDE + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_GEO_FENCE_RANGE + COLUMN_TYPE_INTEGER + " )";
    }

    //Table for Photoresultion
    public static class PhotoResolutionTable {
        public static final String TABLE_NAME = "AppPhotoResolution";

        public static final String COLUMN_NAME_WIDTH = "Width";
        public static final String COLUMN_NAME_HEIGHT = "Height";

        //Query to create photo resolution table
        public static final String PHOTO_RESOLUTION_QUERY = "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_COMMON_ID + COLUMN_TYPE_INTEGER + " PRIMARY KEY AUTOINCREMENT" + COMA_SEP +
                AppConfigurationTable.COLUMN_NAME_DEVICE_KEY + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_WIDTH + COLUMN_TYPE_INTEGER + COMA_SEP +
                COLUMN_NAME_HEIGHT + COLUMN_TYPE_INTEGER + " )";
    }
    //Table for Video Resoultion
    public static class VideoResolutionTable {
        public static final String TABLE_NAME = "AppVideoResolution";
        public static final String COLUMN_NAME_WIDTH = "Width";
        public static final String COLUMN_NAME_HEIGHT = "Height";

        //Query to create video resolution table
        public static final String VIDEO_RESOLUTION_QUERY = "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_COMMON_ID + COLUMN_TYPE_INTEGER + " PRIMARY KEY AUTOINCREMENT" + COMA_SEP +
                AppConfigurationTable.COLUMN_NAME_DEVICE_KEY + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_WIDTH + COLUMN_TYPE_INTEGER + COMA_SEP +
                COLUMN_NAME_HEIGHT + COLUMN_TYPE_INTEGER + " )";
    }

    //Table for GPS Asset Tracker
    public static class GpsAssetTracker{
        public static final String TABLE_NAME = "GpsAssetTracker";
        public static final String COLUMN_NAME_LATITUDE = "latitude";
        public static final String COLUMN_NAME_LONGITUDE = "longitude";
        public static final String COLUMN_NAME_DATE_TIME_LONG_GMT="dateTimeGMT";

        //Query to create GPS Asset Tarcker table
        public static final String GPS_ASSET_TRACKER_QUERY = "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_COMMON_ID + COLUMN_TYPE_INTEGER + " PRIMARY KEY AUTOINCREMENT" + COMA_SEP +
                COLUMN_NAME_LATITUDE + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_LONGITUDE + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_DATE_TIME_LONG_GMT +COLUMN_TYPE_TEXT + " )";
    }

    //Table for GPS Tracker Violation
    public static class GpsTrackerViolation {
        public static final String TABLE_NAME = "GpsTrackerViolation";
        public static final String COLUMN_NAME_LATITUDE = "latitude";
        public static final String COLUMN_NAME_LONGITUDE = "longitude";
        public static final String COLUMN_NAME_VIOLATION = "violation";
        public static final String COLUMN_NAME_DATE_TIME_GMT="dateTime";

        //Query to create GPS Tarcker Viloation table
        public static final String GPS_TRACKER_VIOLATION_QUERY = "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_COMMON_ID + COLUMN_TYPE_INTEGER + " PRIMARY KEY AUTOINCREMENT" + COMA_SEP +
                COLUMN_NAME_LATITUDE + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_LONGITUDE + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_VIOLATION + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_DATE_TIME_GMT+COLUMN_TYPE_TEXT +" )";
    }

    //Table for Battery Usage
    public static class BatteryUsage{
        public static final String TABLE_NAME="BatteryUsage";
        public static final String COLUMN_NAME_DEVICE_ID="deviceId";
        public static final String COLUMN_NAME_BATTERY_PERCENTAGE="batteryPer";
        public static final String COLUMN_NAME_BATTERY_STATUS="batteryStatus";
        public static final String COLUMN_NAME_DATE_TIME_GMT="dateTime";

        //Query to create battery usage table
        public static final String BATTERY_USAGE_QUERY="CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_COMMON_ID + COLUMN_TYPE_INTEGER + " PRIMARY KEY AUTOINCREMENT" + COMA_SEP +
                COLUMN_NAME_DEVICE_ID + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_BATTERY_STATUS + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_BATTERY_PERCENTAGE + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_DATE_TIME_GMT +COLUMN_TYPE_TEXT + " )";

    }

    //Table for Signal Strength
    public static class SignalStrength{
        public static final String TABLE_NAME="SignalStrength";
        public static final String COLUMN_NAME_DEVICE_ID="deviceId";
        public static final String COLUMN_NAME_GSM_PROVIDER="gmsProvider";
        public static final String COLUMN_NAME_SIGNAL_STRENGTH="signalStrength";
        public static final String COLUMN_NAME_DATE_TIME_GMT="dateTime";

        //Query to create Signal Strength table
        public static final String SIGNAL_STRENGTH_QUERY="CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_COMMON_ID + COLUMN_TYPE_INTEGER + " PRIMARY KEY AUTOINCREMENT" + COMA_SEP +
                COLUMN_NAME_DEVICE_ID + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_GSM_PROVIDER + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_SIGNAL_STRENGTH + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_DATE_TIME_GMT +COLUMN_TYPE_TEXT  + " )";
    }

    //Table for Data Consumption
       public static class DataConsumption{
        public static final String TABLE_NAME="DataConsumption";
        public static final String COLUMN_NAME_DEVICE_ID="deviceId";
        public static final String COLUMN_NAME_DATA_TYPE="dataType";
        public static final String COLUMN_NAME_DATA_SOURCE="dataSource";
        public static final String COLUMN_NAME_DATA_UPLOAD="dataUpload";
        public static final String COLUMN_NAME_DATA_DOWNLOAD="dataDownload";
        public static final String COLUMN_NAME_DATE_TIME_GMT="dateTime";

        //Query to Table for Data Consumption
        public static final String DATA_CONSUMPTION_QUERY="CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_COMMON_ID + COLUMN_TYPE_INTEGER + " PRIMARY KEY AUTOINCREMENT" + COMA_SEP +
                COLUMN_NAME_DEVICE_ID + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_DATA_TYPE + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_DATA_SOURCE + COLUMN_TYPE_TEXT + COMA_SEP+
                COLUMN_NAME_DATA_UPLOAD + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_DATA_DOWNLOAD + COLUMN_TYPE_TEXT + COMA_SEP+
                COLUMN_NAME_DATE_TIME_GMT +COLUMN_TYPE_TEXT  + " )";
    }

    //Table for App Usage
    public static class AppUsage{
        public static final String TABLE_NAME="AppUsage";
        public static final String COLUMN_NAME_DEVICE_ID="deviceId";
        public static final String COLUMN_NAME_APP_ID="appName";
        public static final String COLUMN_NAME_APP_START_DATE_TIME="appStartDateTime";
        public static final String COLUMN_NAME_APP_STOP_DATE_TIME="appStoptDateTime";

        //Query to create Table for App Usage
        public static final String APP_USAGE_QUERY="CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_COMMON_ID + COLUMN_TYPE_INTEGER + " PRIMARY KEY AUTOINCREMENT" + COMA_SEP +
                COLUMN_NAME_DEVICE_ID + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_APP_ID + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_APP_START_DATE_TIME + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_APP_STOP_DATE_TIME + COLUMN_TYPE_TEXT   + " )";
    }

    //Table for Device Usage
    public static class DeviceUsage{
        public static final String TABLE_NAME="DeviceUsage";
        public static final String COLUMN_NAME_DEVICE_ID="deviceId";
        public static final String COLUMN_NAME_SWITCH_ON_DATE_TIME_GMT="switchOnDateTime";
        public static final String COLUMN_NAME_SWITCH_OFF_DATE_TIME_GMT="switchOffDateTime";

        //Query to create Table for Device Usage
        public static final String DEVICE_USAGE_QUERY="CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_COMMON_ID + COLUMN_TYPE_INTEGER + " PRIMARY KEY AUTOINCREMENT" + COMA_SEP +
                COLUMN_NAME_DEVICE_ID + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_SWITCH_ON_DATE_TIME_GMT + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_SWITCH_OFF_DATE_TIME_GMT + COLUMN_TYPE_TEXT  + " )";
    }

    //Table for NOtification Data
    public static class NotificationData{
        public static final String TABLE_NAME="NotificationData";
        public static final String COLUMN_NAME_NOTIFICATION_ID="notificationId";
        public static final String COLUMN_NAME_NOTIFICATION_TITLE="notificationTitle";
        public static final String COLUMN_NAME_NOTIFICATION_CONTENT="notificationContent";
        public static final String COLUMN_NAME_NOTIFICATION_DATE="notificationDate";
        public static final String COLUMN_NAME_NOTIFICATION_TIME="notifiactionTime";

        //Query to create Table for NOtification Data
        public static final String NOTIFICATION_DATA_QUERY="CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_COMMON_ID + COLUMN_TYPE_INTEGER + " PRIMARY KEY AUTOINCREMENT" + COMA_SEP +
                COLUMN_NAME_NOTIFICATION_ID + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_NOTIFICATION_TITLE + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_NOTIFICATION_DATE +COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_NOTIFICATION_TIME +COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_NAME_NOTIFICATION_CONTENT + COLUMN_TYPE_TEXT  + " )";
    }

    //Table for User Control
    public static class UserControl{
        public static final String TABLE_NAME="UserControl";
        public static final String COLUMN_BT_USER="bt";
        public static final String COLUMN_WIFI_USER="wifi";
        public static final String COLUMN_MOBILE_USER="mobile";
        public static final String COLUMN_AEROPLANE_USER="aeroplane";
        public static final String COLUMN_LOCATION_USER="location";

        //Query to create Table for User control
        public static final String USER_CONTROL_QUERY="CREATE TABLE "+TABLE_NAME + " (" +
                COLUMN_COMMON_ID + COLUMN_TYPE_INTEGER + " PRIMARY KEY AUTOINCREMENT" + COMA_SEP +
                COLUMN_LOCATION_USER + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_MOBILE_USER + COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_AEROPLANE_USER +COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_BT_USER +COLUMN_TYPE_TEXT + COMA_SEP +
                COLUMN_WIFI_USER + COLUMN_TYPE_TEXT  + " )";
    }
}
