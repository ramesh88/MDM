package com.thril.mdm.Global;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Ram on 2/7/2018.
 */

//Helper class for date operations
public class DateUtils {

    //Method to check two dates with parameter activeFrom and activeTo
    public static boolean compareAppDates(long activeFrom, long activeTo) {
        Date activeFromDate = new Date(activeFrom); // GMT format
        Date activeToDate = new Date(activeTo); // GMT Format
        Date systemDate = new Date(DateUtils.getCurrentDateGMT()); // current date should be in GMT Format
        if (systemDate.compareTo(activeFromDate) < 0) {
            return false;
        } else if (systemDate.compareTo(activeToDate) > 0) {
            return false;
        } else if (systemDate.compareTo(activeFromDate) == 0 || systemDate.compareTo(activeToDate) == 0) {
            return true;
        } else if (systemDate.compareTo(activeFromDate) > 0 && systemDate.compareTo(activeToDate) < 0) {
            return true;
        }
        return false;
    }

    //Method to get current date with parameter format
    public static String getCurrentDate(String format) {
        String date = "";
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat(format);
        date = df.format(c.getTime());
        return date;
    }

    //Method to get current time
    public static String getCurrentTime() {
        String time = "";
        Calendar c=Calendar.getInstance();
        c.setTime(new Date());
        String hour=""+c.get(Calendar.HOUR);
        String minute=""+c.get(Calendar.MINUTE);
        String second=""+c.get(Calendar.SECOND);

        if(hour.length() < 2){
            hour="0"+hour;
        }
        if(minute.length()<2){
            minute="0"+minute;
        }if(second.length()<2){
            second="0"+minute;
        }
        time=hour+":"+minute+":"+second;
        return time;

    }

    //Method to get notification time
    public static String getNotificationTime() {
        String time = "";
        Calendar c = Calendar.getInstance();
        SimpleDateFormat formatTime = new SimpleDateFormat("hh:mm a");
        time=formatTime.format(new Date());
        return time;

    }

    //method to get current date in GMT format
    public static long getCurrentDateGMT(){
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        cal.setTime(new Date());
        Date currentLocalTime = cal.getTime();
        return currentLocalTime.getTime();
    }
}
