package com.thril.mdm.Global;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.usage.UsageStatsManager;
import android.bluetooth.BluetoothAdapter;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.view.Window;
import android.view.WindowManager;

import com.thril.mdm.Network.utils.Constants;
import com.thril.mdm.kiosk.MainActivity;
import com.thril.mdm.service.StopSettingsService;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by Ram on 2/18/2018.
 */

//Class for MDM app settings
public class MDMSettings {
    //Content resolver used as a handle to the system's settings
    private static ContentResolver cResolver;
    //Window object, that will store a reference to the current window
    private static Window window;
    private static Camera cam = null;
    public static final int REQUEST_GPS_ON = 101;
    public static final int REQUEST_GPS_OFF = 102;
    public static SharedPreferences sharedpreferences;
    public static final String KEY_BRIGHTNESS = "Brightness";
    public static final String KEY_RINGER = "Ringer";
    public static final String KEY_MEDIA = "Media";
    public static final String KEY_FLASH_LIGHT = "Flash";
    private static final String MY_PREFERENCE = "Configuration";


    public static final boolean GPS_SERVICE = false;

    public static final int REQUEST_GPS_CHECK_SETTINGS = 103;
    public static final int REQUEST_HOME_SETTINGS = 110;
    public static final int REQUEST_FOR_OVERLAY = 1234;
    public static final int REQUEST_FOR_CAN_WRITE = 105;
    public static final int REQUEST_FOR_PERMISSION = 101;
    public static final int REQUEST_FOR_MOBILE_DATA = 106;
    public static final int REQUEST_USAGE_STATS = 107;
    public static final int REQUEST_FLIGHT_MODE = 108;
    public static final int REQUEST_LAUNCHER = 109;
    public static final int REQUEST_DO_NOT_DISTURB=111;
    public static boolean dataBeamFlag = false;
    public static final String[] smsArray = new String[]{
            "GetCurrentLocation",
            "UnLock",
            "WipeOut",
            "GetInternetAvailability",
            "GetCurrentDataUsage"
    };
    public static String appPackage = "";
    public static boolean appPackageFlag = false;
    public static String appId = "";


    //method to set brightness with paramaters brightness,context and activity
    @RequiresApi(api = Build.VERSION_CODES.M)
    public static void setBrightness(int brightness, Context context, MainActivity activity) {
        //Get the content resolver
        cResolver = context.getContentResolver();
        //Get the current window
        window = activity.getWindow();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            android.provider.Settings.System.putInt(cResolver, android.provider.Settings.System.SCREEN_BRIGHTNESS, brightness);
        } else {
            if (Settings.System.canWrite(context)) {
                android.provider.Settings.System.putInt(cResolver, android.provider.Settings.System.SCREEN_BRIGHTNESS, brightness);
                //  }
                android.provider.Settings.System.putInt(cResolver,
                        android.provider.Settings.System.SCREEN_BRIGHTNESS_MODE,
                        android.provider.Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
                WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
                lp.screenBrightness = brightness / (float) 255;
                activity.getWindow().setAttributes(lp);
                //Apply attribute changes to this window
                window.setAttributes(lp);
            }
        }

    }

    //method to get brightness with paramaters context
    public static int getDefaultBrightness(Context context) {
        int brightness = 0;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            brightness = android.provider.Settings.System.getInt(
                    context.getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS, 0);

        } else {
            brightness = android.provider.Settings.System.getInt(
                    context.getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS, 0);
        }
        return brightness;
    }

    //method to get deafult ringer volume  with paramaters context
    public static int getDefalutRingerVolume(Context context) {
        AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        return audio.getStreamVolume(AudioManager.STREAM_RING);
    }

    //method to get media volume with paramaters context
      public static int getDefalutMediaVolume(Context context) {
        AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        return audio.getStreamVolume(AudioManager.STREAM_MUSIC);
    }

    //method to set oerientation od device with paramaters context and flag
    public static void setAutoOrientation(Context context, boolean enabled) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            android.provider.Settings.System.putInt(context.getContentResolver(), android.provider.Settings.System.ACCELEROMETER_ROTATION, enabled ? 1 : 0);
        } else {
            android.provider.Settings.System.putInt(context.getContentResolver(), android.provider.Settings.System.ACCELEROMETER_ROTATION, enabled ? 1 : 0);
        }
    }

    //method to get orientation with paramaters context
    public static boolean getAutoOrientation(Context context) {
        return android.provider.Settings.System.getInt(context.getContentResolver(),
                android.provider.Settings.System.ACCELEROMETER_ROTATION, 0) != 0;
    }


    @SuppressLint("NewApi") //method to know airplane mode is on or not with paramaters context
    public static boolean isAirplaneMode(Context context) {
        // check the version
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {

            return android.provider.Settings.System.getInt(context.getContentResolver(),
                    android.provider.Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.Global.getInt(context.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 0) != 0;

        }
    }

    //method to toggle airplane mode  with paramaters value ,state and context
    public static void toggleAirplaneMode(int value, boolean state, Context context) {
        // toggle airplane mode
        // check the version
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) { // if
            // less
            // then
            // version
            // 4.2

            Settings.System.putInt(context.getContentResolver(),
                    Settings.System.AIRPLANE_MODE_ON, value);
        } else {
            Settings.Global.putInt(context.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, value);
        }
        // broadcast an intent to inform
//        Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
//        intent.putExtra("state", !state);
//        context.sendBroadcast(intent);
    }

    //method to check mobile data is on or not with paramaters context
    public static boolean isMobileDataEnable(Context context) {
        boolean mobileDataEnabled = false; // Assume disabled
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            Class cmClass = Class.forName(cm.getClass().getName());
            Method method = cmClass.getDeclaredMethod("getMobileDataEnabled");
            method.setAccessible(true); // Make the method callable
            // get the setting for "mobile data"
            mobileDataEnabled = (Boolean) method.invoke(cm);
        } catch (Exception e) {
            // Some problem accessible private API and do whatever error handling you want here
        }
        return mobileDataEnabled;
    }

    //method to toggle mobile data with paramaters flag and context
    public static boolean toggleMobileDataConnection(boolean ON, Context context) {
        try {
            Constants.settings_index = 2;
            //create instance of connectivity manager and get system connectivity service
            final ConnectivityManager conman = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            //create instance of class and get name of connectivity manager system service class
            final Class conmanClass = Class.forName(conman.getClass().getName());
            //create instance of field and get mService Declared field
            final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
            //Attempt to set the value of the accessible flag to true
            iConnectivityManagerField.setAccessible(true);
            //create instance of object and get the value of field conman
            final Object iConnectivityManager = iConnectivityManagerField.get(conman);
            //create instance of class and get the name of iConnectivityManager field
            final Class iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
            //create instance of method and get declared method and type
            final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
            //Attempt to set the value of the accessible flag to true
            setMobileDataEnabledMethod.setAccessible(true);
            //dynamically invoke the iConnectivityManager object according to your need (true/false)
            setMobileDataEnabledMethod.invoke(iConnectivityManager, ON);
        } catch (Exception e) {
        }
        return true;
    }

    //method to set flash light  with paramaters flag
    public static void setFlashLight(boolean flag) {
        cam = Camera.open();
        if (flag) {
            Camera.Parameters p = cam.getParameters();
            p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            cam.setParameters(p);
            cam.startPreview();
        } else {
            Camera.Parameters params = cam.getParameters();
            params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            cam.stopPreview();
            cam.release();
            cam = null;
        }

    }
    //method to enable bluetooth
    public static boolean setBluetooth() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter != null) {
            if (mBluetoothAdapter.isEnabled()) {
                mBluetoothAdapter.disable();
                return false;
            } else {
                mBluetoothAdapter.enable();
                return true;
            }
        } else {
            return false;
        }


    }

    // method to trun on gps with paramter activity
    public static void turnGPSOn(MainActivity activity) {
        Intent intent1 = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        activity.startActivityForResult(intent1, REQUEST_GPS_ON);

    }

     // method to trun off gps with paramter activity
    public static void turnGPSOff(MainActivity activity) {
        Intent intent1 = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        activity.startActivityForResult(intent1, REQUEST_GPS_OFF);


    }

    // method to check gps status on gps with paramter context
    public static boolean checkGpsStatus(Context context) {

        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        boolean gpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        return gpsStatus;
    }

    // method to set wifi with paramter context
    public static boolean setWifi(Context context) {

        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(false);
            return false;
        } else {
            wifiManager.setWifiEnabled(true);
            return true;
        }
    }

    //method to get max ringer volume of device with parameter context
    public static int getMaxRingerVolume(Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        return audioManager.getStreamMaxVolume(AudioManager.STREAM_RING);
    }

    //method to set max ringer volume of device with parameter context,volume
    public static void setRingerVolume(Context context, int volume) {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_RING, volume, 0);
    }

    //method to get max media volume of device with parameter context
    public static int getMaxMediaVolume(Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        return audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
    }

    //method to get set media volume of device with parameter context,volume
    public static void setMediaVolume(Context context, int volume) {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
    }

    //method to get screen brightness of device with parameter context
    public static int getDefaultScrenBrightness(Context context) {
        int brightness = 0;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            brightness = android.provider.Settings.System.getInt(context.getContentResolver(), android.provider.Settings.System.SCREEN_BRIGHTNESS, 0);
        } else {
            brightness = android.provider.Settings.System.getInt(context.getContentResolver(), android.provider.Settings.System.SCREEN_BRIGHTNESS, 0);
        }
        return brightness;
    }

    //method to get key value with parameter context, key
    public static int getKeyValue(Context context, String key) {
        sharedpreferences = context.getSharedPreferences(MY_PREFERENCE,
                Context.MODE_PRIVATE);
        return sharedpreferences.getInt(key, 0);
    }

    //method to set key value with parameter context, key,value
    public static void setKeyValue(Context context, String key, int value) {
        sharedpreferences = context.getSharedPreferences(MY_PREFERENCE,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt(key, value);
        editor.commit();

    }


    //method to set flash value with parameter context, key,value
    public static void setFlashValue(Context context, String key, String value) {
        sharedpreferences = context.getSharedPreferences(MY_PREFERENCE,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(key, value);
        editor.commit();

    }

    //method to get key value with parameter context, key
    public static String getFlashValue(Context context, String key) {
        sharedpreferences = context.getSharedPreferences(MY_PREFERENCE,
                Context.MODE_PRIVATE);
        return sharedpreferences.getString(key, "");
    }


    //method to check wifi enabled or not with parameter context
    public static boolean isWifiEnabled(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (wifiManager.isWifiEnabled())
            return true;

        return false;
    }

    //method to check bluetooth enabled or not
    public static boolean isBTEnabled() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter != null)
            if (mBluetoothAdapter.isEnabled()) {
                return true;
            }
        return false;
    }

    //method to call home settings  with parameter activity
    public static void callHomeSettings(Activity activity) {

        activity.startActivityForResult(new Intent(Settings.ACTION_HOME_SETTINGS), MDMSettings.REQUEST_HOME_SETTINGS);
    }

    //method to call write settings with parameter activity
    public static void callCanWriteSettings(Activity activity) {
        Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS);
        intent.setData(Uri.parse("package:" + activity.getPackageName()));
        activity.startActivityForResult(intent, MDMSettings.REQUEST_FOR_CAN_WRITE);
    }

    //method to call overlay  settings  with parameter activity
    public static void callDrawOverlaySettings(Activity activity) {
        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:" + activity.getPackageName()));
        activity.startActivityForResult(intent, MDMSettings.REQUEST_FOR_OVERLAY);
    }

    //method to save asset identifier  with parameter asset identifier
    public static void saveAssetIdentifier(String assetIdentifier) {
        SharedPreferences sharedPreferences = Constants.getmContext().getSharedPreferences("AssetIdentifier", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("assetID", assetIdentifier);
        editor.commit();
    }

    //method to save staff details  with parameter staffname ,staffotherinfo
    public static void saveStaffDetails(String StaffNam, String Staffotherinfo) {
        SharedPreferences sharedPreferences = Constants.getmContext().getSharedPreferences("StaffDetails", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("staffName", StaffNam);
        editor.putString("staffOther", Staffotherinfo);
        editor.commit();
    }

    //method to save reg date with parameter asset regDate
    public static void saveRegistrationDate(String regDate) {
        SharedPreferences sharedPreferences = Constants.getmContext().getSharedPreferences("RegistrationDate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("regDate", regDate);
        editor.commit();
    }

    //method to get staff name
    public static String getStaffName() {
        String staffName = null;
        SharedPreferences sharedPreferences = Constants.getmContext().getSharedPreferences("StaffDetails", Context.MODE_PRIVATE);
        if (sharedPreferences.contains("staffName")) {
            staffName = sharedPreferences.getString("staffName", null);
        }
        return staffName;
    }

    //method to get staff other info
    public static String getStaffOtherInfo() {
        String staffOther = null;
        SharedPreferences sharedPreferences = Constants.getmContext().getSharedPreferences("StaffDetails", Context.MODE_PRIVATE);
        if (sharedPreferences.contains("staffOther")) {
            staffOther = sharedPreferences.getString("staffOther", null);
        }
        return staffOther;
    }

    //method to get reg date
    public static String getRegDate() {
        String regDate = null;
        SharedPreferences sharedPreferences = Constants.getmContext().getSharedPreferences("RegistrationDate", Context.MODE_PRIVATE);
        if (sharedPreferences.contains("regDate")) {
            regDate = sharedPreferences.getString("regDate", null);
        }
        return regDate;
    }


    //method to get asset identifier from prefernces
    public static String getAssetIdentifierFromPreference() {
        String assetID = null;
        SharedPreferences sharedPreferences = Constants.getmContext().getSharedPreferences("AssetIdentifier", Context.MODE_PRIVATE);
        if (sharedPreferences.contains("assetID")) {
            assetID = sharedPreferences.getString("assetID", null);
        }
        return assetID;
    }

    //method to save firebase token with paarmeter token
    public static void saveFireBaseToken(String token) {
        SharedPreferences sharedPreferences = Constants.getmContext().getSharedPreferences("FireBaseToken", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("tokenId", token);
        editor.commit();
    }
    //method to get firebase token
    public static String getFireBaseToken() {
        String tokenId = null;
        SharedPreferences sharedPreferences = Constants.getmContext().getSharedPreferences("FireBaseToken", Context.MODE_PRIVATE);
        if (sharedPreferences.contains("tokenId")) {
            tokenId = sharedPreferences.getString("tokenId", null);
        }
        return tokenId;
    }

    //method to save first device time with paarmeter time
    public static void saveFirstDeviceTime(long time) {
        SharedPreferences sharedPreferences = Constants.getmContext().getSharedPreferences("DeviceOnTime", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong("deviceOnTime", time);
        editor.commit();
    }

    //method to get first device time
    public static long getFirstDeviceTime() {
        long deviceOnTime = 0;
        SharedPreferences sharedPreferences = Constants.getmContext().getSharedPreferences("DeviceOnTime", Context.MODE_PRIVATE);
        if (sharedPreferences.contains("deviceOnTime")) {
            deviceOnTime = sharedPreferences.getLong("deviceOnTime", 0);
        }
        return deviceOnTime;
    }

    //method to open mobile data settings with paramter activity
    public static void mobileDataSettings(MainActivity activity) {
        Constants.settings_index = 2;
        Intent intent = new Intent();
        intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings$DataUsageSummaryActivity"));
        activity.startActivityForResult(intent, REQUEST_FOR_MOBILE_DATA);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static boolean getAppUsageSettings() { //method to get app usage settings
        UsageStatsManager mUsageStatsManager = (UsageStatsManager) Constants.getmContext().getSystemService(Context.USAGE_STATS_SERVICE);
        long time = System.currentTimeMillis();
        List stats = mUsageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 1000 * 10, time);
        if (stats == null || stats.isEmpty()) {
            return false;
        }
        return true;
    }

    //method to open appusage stats with paramter activity
    public static void appUsageStats(Activity activity) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_USAGE_ACCESS_SETTINGS);
        activity.startActivityForResult(intent, REQUEST_USAGE_STATS);
    }


    //method to open flight mode settings with paramter activity
    public static void flightModeSettings(Activity activity) {
        try {
            Constants.settings_index = 2;

            Intent intent = new Intent(android.provider.Settings.ACTION_AIRPLANE_MODE_SETTINGS);
            //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivityForResult(intent, REQUEST_FLIGHT_MODE);
        } catch (ActivityNotFoundException e) {
            try {
                Intent intent = new Intent("android.settings.WIRELESS_SETTINGS");
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivityForResult(intent, REQUEST_FLIGHT_MODE);
            } catch (ActivityNotFoundException ex) {
                // Toast.makeText(buttonView.getContext(), R.string.not_able_set_airplane, Toast.LENGTH_SHORT).show();
            }
        }
    }

    //method to check phone number  with paramter phonemnuber
    public static boolean getcheckPhoneno(String str) {
        boolean flag = false;
        str = str.trim();
        // System.out.println("i m in phone no check");
        if (str.length() < 10) {
            flag = true;
        } else if (!str.startsWith("7") && !str.startsWith("8")
                && !str.startsWith("9")) {
            flag = true;
            // System.out.println("phone no::::" + str);
        } else {
            int cnt = 1;
            char[] chArr = new char[str.length()];
            str.getChars(0, str.length(), chArr, 0);
            for (int i = 1; i < chArr.length; i++) {
                // System.out.println(chArr[0] + "" + chArr[i]);
                if (chArr[0] == chArr[i]) {
                    cnt++;
                }
            }
            // System.out.println("cnt value:" + cnt);
            if (cnt == str.length()) {
                flag = true;
            }
        }
        return flag;
    }


    //method to check service is running or not  with paramter activity and class name
    public static boolean isMyServiceRunning(Activity mA, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) mA.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {

            if (serviceClass.getName().equals(service.service.getClassName())) {
                System.out.println("serviceClass.getName()........"
                        + serviceClass.getName());
                return true;
            }
        }
        return false;
    }


    //method to open do not disturb settings with parameter activity
    public static void doNotDisturbSettings(Activity activity){

        Intent intent = new Intent(Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);
        activity.startActivityForResult(intent,REQUEST_DO_NOT_DISTURB);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)     //method to check do not disturb on or nor with parameter notificationManager
    public static boolean isDoNotDisturbGranted(NotificationManager notificationManager){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            if(notificationManager.isNotificationPolicyAccessGranted()){
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)//method to get current do not disturb status  with parameter notificationManager
    public static boolean getCurrentDNDStatus(NotificationManager notificationManager){

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                int currentDNDMode=notificationManager.getCurrentInterruptionFilter();
                switch (currentDNDMode){
                    case NotificationManager.INTERRUPTION_FILTER_NONE:
                        return true;
                    case NotificationManager.INTERRUPTION_FILTER_ALL:
                        return false;
                    case NotificationManager.INTERRUPTION_FILTER_PRIORITY:
                        return true;
                    case NotificationManager.INTERRUPTION_FILTER_ALARMS:
                        return true;
                    default:
                        return false;
                }
            }
            return false;
        }

    @RequiresApi(api = Build.VERSION_CODES.M)//method to off  do not disturb with parameter notificationManager
    public static void offDNDMode(NotificationManager notificationManager){

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (notificationManager != null && getCurrentDNDStatus(notificationManager)) {
                notificationManager.setInterruptionFilter(NotificationManager.INTERRUPTION_FILTER_ALL);
            }
        }

    }

}
