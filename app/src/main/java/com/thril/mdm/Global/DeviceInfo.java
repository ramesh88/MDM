package com.thril.mdm.Global;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.thril.mdm.Network.utils.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ram on 2/3/2018.
 */

//Class to get device information
public class DeviceInfo {

    //method to get uuid of device with parameter context
    public String getUUID(Context context) {
        String androidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        return androidId;
    }

    //method to get IMEI of device with parameter context
    public String getDeviceIMEI(Context ctx) {
        TelephonyManager telephonyManager = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    //method to get sim serial of device with parameter context
    public List<String> getSim1SerialNumber(Context ctx) {
        List<String> mobilenum = new ArrayList<>();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP_MR1) {
            SubscriptionManager subscriptionManager = SubscriptionManager.from(ctx);
            List<SubscriptionInfo> subsInfoList = subscriptionManager.getActiveSubscriptionInfoList();
            // Log.d("subscription size", "" + subsInfoList.size());
            if (subsInfoList != null)
                for (SubscriptionInfo subscriptionInfo : subsInfoList) {
                    // Log.d("Number",  subscriptionInfo.getNumber());
                    String number = subscriptionInfo.getIccId();
                    mobilenum.add(number);

                }
        } else {
            TelephonyManager telephonyManager = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
            mobilenum.add(telephonyManager.getSimSerialNumber());
        }

        return mobilenum;
    }


    //method to get device model
    public String getModel() {
        return Build.MODEL;
    }

    //method to get manufaturer of device
    public String getManufacturer() {
        return Build.MANUFACTURER;
    }

    //method to get platform of device
    public String getPlatformName() {
        int sdkVersion = Build.VERSION.SDK_INT;
        switch (sdkVersion) {
            case Build.VERSION_CODES.CUPCAKE:
                return "CUPCAKE";
            case Build.VERSION_CODES.DONUT:
                return "DONUT";
            case Build.VERSION_CODES.ECLAIR:
                return "ECLAIR";
            case Build.VERSION_CODES.ECLAIR_0_1:
                return "ECLAIR";
            case Build.VERSION_CODES.ECLAIR_MR1:
                return "ECLAIR";
            case Build.VERSION_CODES.GINGERBREAD:
                return "GINGERBREAD";
            case Build.VERSION_CODES.GINGERBREAD_MR1:
                return "GINGERBREAD";
            case Build.VERSION_CODES.FROYO:
                return "FROYO";
            case Build.VERSION_CODES.HONEYCOMB:
                return "HONEYCOMB";
            case Build.VERSION_CODES.HONEYCOMB_MR1:
                return "HONEYCOMB";
            case Build.VERSION_CODES.HONEYCOMB_MR2:
                return "HONEYCOMB";
            case Build.VERSION_CODES.ICE_CREAM_SANDWICH:
                return "ICECREAM SANDWICH";
            case Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1:
                return "ICECREAM SANDWICH";
            case Build.VERSION_CODES.JELLY_BEAN:
                return "JELLY BEAN";
            case Build.VERSION_CODES.JELLY_BEAN_MR1:
                return "JELLY BEAN";
            case Build.VERSION_CODES.JELLY_BEAN_MR2:
                return "JELLY BEAN";
            case Build.VERSION_CODES.KITKAT:
                return "KITKAT";
            case Build.VERSION_CODES.KITKAT_WATCH:
                return "KITKAT WATCH";
            case Build.VERSION_CODES.LOLLIPOP:
                return "LOLLIPOP";
            case Build.VERSION_CODES.LOLLIPOP_MR1:
                return "LOLLIPOP";
            case Build.VERSION_CODES.M:
                return "MARSHMALLOW";
            case Build.VERSION_CODES.N:
                return "NOUGAT";
            case Build.VERSION_CODES.N_MR1:
                return "NOUGAT";
            case Build.VERSION_CODES.O:
                return "OREO";
        }
        return "";
    }

    //method to get version of device
    public String getVersion() {
        return Build.VERSION.RELEASE;
    }

    //method to get serial of device
    public String getSerial() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return Build.getSerial();
        }
        return Build.SERIAL;
    }

    //method to get  device active or not
    public String getIsActive() {
        return "YES";
    }

    //method to get device virtual or not
    public String getIsVirtual() {
        return "NO";
    }

    //method to get name of device
    public String getName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }


    //method to capitalize the string with parameter string
    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    //method to get asset identifier
    public String getAssetIdentifier() {
        return Constants.asset_identifier;
    }

}
