package com.thril.mdm.Global;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.thril.mdm.BuildConfig;
import com.thril.mdm.Network.utils.Constants;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Ram on 2/7/2018.
 */

//Class for downloading apk and icons
public class ApkDownloader {

    private static final String TAG = "ApkDownloader";
    public static final String ICONPATH = "/.MDM/ICON/";
    public static final String APKPATH = "/.MDM/APK/";
    public static final String SCREENSHOTPATH = "/SCREENSHOTS/";


    // method to donwload and install file from ftp with parameters contex, sever, port number, user,password, filename and local file
    public static boolean ftpDownloadAndInstallFile(Context context, String server, int portNumber,
                                                    String user, String password, String filename, File localFile)
            throws IOException {
        FTPClient ftp = null;

        try {
            ftp = new FTPClient();
            ftp.connect(server, portNumber);
            Log.d(TAG, "Connected. Reply: " + ftp.getReplyString());

            ftp.login(user, password);
            Log.d(TAG, "Logged in");
            ftp.setFileType(FTP.BINARY_FILE_TYPE);
            Log.d(TAG, "Downloading");
            ftp.enterLocalPassiveMode();

            OutputStream outputStream = null;
            boolean success = false;
            try {
                outputStream = new BufferedOutputStream(new FileOutputStream(
                        localFile));
                success = ftp.retrieveFile(filename, outputStream);
                if (success) {
                    installApk(context, localFile.getAbsolutePath());
                }

            } finally {
                if (outputStream != null) {
                    outputStream.close();
                }
            }

            return success;
        } finally {
            if (ftp != null) {
                ftp.logout();
                ftp.disconnect();
            }
        }
    }
    // method to donwload and install file from server with parameters contex, severurl, apkname, downloaddesc,titldesc
    public static void httpDownloadAndInstallFile(final Context context, String serverUrl, String apkName, String downloadDescription, String titleDescription) {

        String destination = ApkDownloader.APKPATH;
        String fileName = apkName;

        //Delete update file if exists
        File file = new File(destination);
        if (!file.exists())
            //file.delete() - test this, I think sometimes it doesnt work
            file.mkdirs();


        destination += fileName;
        final Uri uri = Uri.parse("file://" + destination);

        //get url of app on server

        //set downloadmanager
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(serverUrl));
        request.setDescription(downloadDescription);
        request.setTitle(titleDescription);

        //set destination
        request.setDestinationUri(uri);

        // get download service and enqueue file
        final DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        final long downloadId = manager.enqueue(request);

        //set BroadcastReceiver to install app when .apk is downloaded
        BroadcastReceiver onComplete = new BroadcastReceiver() {
            public void onReceive(Context ctxt, Intent intent) {
                installApk(ctxt, uri.getPath());

                ctxt.unregisterReceiver(this);


            }
        };
        //register receiver for when .apk download is compete
        context.registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }


    //method to install apk with parameters context and apk path
    public static void installApk(Context context, String apkPath) {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            /*Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
            File file = new File(apkPath);
            Uri uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".fileprovider", file);
            intent.setData(uri);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            context.startActivity(intent);*/

            Uri contentUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".fileprovider", new File(apkPath));
            Intent openFileIntent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
            openFileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            openFileIntent.setData(contentUri);
            context.startActivity(openFileIntent);
        } else {
            Uri apkUri = Uri.fromFile(new File(apkPath));
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }

    }

    //method to open playstore with parameters context and apkpath
    public static void openPlaystore(Context context, String apkPath) {


//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//             Intent openFileIntent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
//            openFileIntent.setData(Uri.parse("market://details?id=" + apkPath));
//            openFileIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            context.startActivity(openFileIntent);
//        } else {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("market://details?id=" + apkPath));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        //   }

    }

    //method to check app installed or not with parameters context and packagename
    public static boolean appInstalledOrNot(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        try {
              pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    //method to get installed app version with parameters context and packagename
    public static String appInstalledAppVersion(Context context, String packageName) {
        String appVersion="";
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo pinfo =pm.getPackageInfo(packageName,0);
            appVersion=pinfo.versionName;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return appVersion;
    }

    //method to launch apk with parameters context and packagename
    public static void launchApk(Context context, String packageName) {
        Intent LaunchIntent = context.getPackageManager()
                .getLaunchIntentForPackage(packageName);
        context.startActivity(LaunchIntent);
    }


    public static String copyAssets(Context context, String fileName) {
        AssetManager assetManager = context.getAssets();
        String apkPath = "";

        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open(fileName);
            File outFile = new File(Environment.getExternalStorageDirectory().getPath() + File.separator +
                    Constants.APP_FOLDER + File.separator + fileName.substring(0, fileName.indexOf("/")));
            if (!outFile.exists())
                outFile.mkdirs();
            apkPath = outFile.getAbsolutePath() + File.separator + fileName.substring(fileName.indexOf("/") + 1);
            Log.e("apkPath", apkPath);
            out = new FileOutputStream(apkPath);
            copyFile(in, out);
        } catch (IOException e) {
            Log.e("tag", "Failed to copy asset file: " + fileName, e);
            apkPath = "";
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    // NOOP
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    // NOOP
                }
            }
        }
        return apkPath;
    }

    //method to copy file with parameters inputstream and output stream
    private static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    //method to get apk file path with parameters url and flag
    public static String getFileOrApk(String url, boolean fileFlag) {
        String apkPath = url.substring(url.lastIndexOf("/") + 1).trim();
        String dir = "";
        if (fileFlag) {
            dir = Environment.getExternalStorageDirectory() + ICONPATH;
        } else {
            dir = Environment.getExternalStorageDirectory() + APKPATH;
        }
        File directory = new File(dir);
        if (!directory.exists()) {
            directory.mkdirs();

        }
        File filePath = new File(directory.getAbsolutePath() + File.separator + apkPath);

        //from web
        try {
            URL imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is = conn.getInputStream();
            OutputStream os = new FileOutputStream(filePath);
            CopyStream(is, os);
            os.close();
            return filePath.getAbsolutePath();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    //method to copy stream with parameters inputstream and output stream
    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }


    //method to capture screen shot with parameter activity
    public static void captureScreen(Activity ctx) {
        View v = ctx.getWindow().getDecorView().getRootView();
        v.setDrawingCacheEnabled(true);
        Bitmap bmp = Bitmap.createBitmap(v.getDrawingCache());
        v.setDrawingCacheEnabled(false);
        String dir = "";

        dir = Environment.getExternalStorageDirectory() + SCREENSHOTPATH;
        File directory = new File(dir);
        if (!directory.exists()) {
            directory.mkdirs();

        }
        try {
            FileOutputStream fos = new FileOutputStream(new File(directory.getAbsolutePath(), "SCREEN"
                    + System.currentTimeMillis() + ".png"));
            bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
            Toast.makeText(ctx, "Screenshot captured...", Toast.LENGTH_SHORT).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
