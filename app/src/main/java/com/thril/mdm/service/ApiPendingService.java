package com.thril.mdm.service;

import android.app.IntentService;
import android.content.Intent;

import com.thril.mdm.AppWSRepository.AppRegistration;
import com.thril.mdm.Network.utils.Constants;
import com.thril.mdm.databaselayer.databasemanager.DatabaseMgr;
import com.thril.mdm.model.DeviceResponse;

/**
 * Created by Ram on 5/20/2018.
 */

//Intent service which invokes the rest api calling
public class ApiPendingService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     *
     */
    public ApiPendingService() {
        super("ApiPendingService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if(Constants.isNetworkAvailable(Constants.getmContext())){
            DeviceResponse deviceResponse= DatabaseMgr.getInstance(Constants.getmContext()).getDeviceResponse();
            AppRegistration.getmInstance().deviceResponse(deviceResponse);
        }
    }
}
