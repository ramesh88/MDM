package com.thril.mdm.service;

import android.app.ActivityManager;
import android.app.IntentService;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Intent;
import android.util.Log;

import com.thril.mdm.Global.DateUtils;
import com.thril.mdm.Global.MDMSettings;
import com.thril.mdm.Network.utils.Constants;

import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by Ram on 6/13/2018.
 */

//INtent srevice class to get app usage records
public class AppStartStopService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public AppStartStopService() {
        super("AppStartStopService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
       while (true){
           String currentApp=getAppList();
           if(!currentApp.equals("") && currentApp.equalsIgnoreCase(MDMSettings.appPackage)){
               MDMSettings.appPackageFlag=true;
               Constants.app_start_date_time= Long.toString(DateUtils.getCurrentDateGMT());
               break;
           }
       }
    }

    //method used to get the package name of forground app
    public String getAppList() {
        String currentApp="";

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {

            UsageStatsManager usm = (UsageStatsManager) getSystemService("usagestats");
            long time = System.currentTimeMillis();
            List<UsageStats> appList = usm.queryUsageStats(UsageStatsManager.INTERVAL_DAILY,
                    time - 1000 * 1000, time);
            if (appList != null && appList.size() > 0) {
                SortedMap<Long, UsageStats> mySortedMap = new TreeMap<Long, UsageStats>();
                for (UsageStats usageStats : appList) {
                    mySortedMap.put(usageStats.getLastTimeUsed(),
                            usageStats);
                }
                if (mySortedMap != null && !mySortedMap.isEmpty()) {
                    currentApp = mySortedMap.get(
                            mySortedMap.lastKey()).getPackageName();
                }
            }
        } else {
            ActivityManager am = (ActivityManager) getBaseContext().getSystemService(ACTIVITY_SERVICE);
            currentApp = am.getRunningTasks(1).get(0).topActivity.getPackageName();

        }
      //  Log.d("PackageName",currentApp);
        return currentApp;
    }
}
