package com.thril.mdm.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.thril.mdm.Global.MDMSettings;

import java.io.IOException;

/**
 * Created by Ram on 6/13/2018.
 */

//Intent service class to delete the fcm notification service
public class DeleteTokenService extends IntentService {
    public static final String TAG = DeleteTokenService.class.getSimpleName();

    public DeleteTokenService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        // Check for current token
        String originalToken = MDMSettings.getFireBaseToken();
        Log.d(TAG, "Token before deletion: " + originalToken);

        // Resets Instance ID and revokes all tokens.
        try {
            FirebaseInstanceId.getInstance().deleteInstanceId();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //now we will have the token
        String token = FirebaseInstanceId.getInstance().getToken();
        if (token != null) {
            MDMSettings.saveFireBaseToken(token);
        }

        //for now we are displaying the token in the log
        //copy it as this method is called only when the new token is generated
        //and usually new token is only generated when the app is reinstalled or the data is cleared
        if(token !=null) {
            Log.d("MyRefreshedToken", token);
        }
    }

}
