package com.thril.mdm.service;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.thril.mdm.Network.utils.Constants;
import com.thril.mdm.business.GpsBusinessListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.thril.mdm.databaselayer.databasemanager.DatabaseMgr;
import com.thril.mdm.model.GeoFence;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Ram on 4/15/2018.
 */
//Intent service class to get gps
public class GoogleClientService implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static GpsBusinessListener gpsBusinessListener;
    private static Context mContext;
    private static GoogleClientService instance;
    private static final String TAG = "GoogleClient";
    public static GoogleApiClient mLocationClient;
    LocationRequest mLocationRequest;
    private static long UPDATE_INTERVAL = 0;
    private static long FASTEST_INTERVAL = UPDATE_INTERVAL;
    public boolean client_connect = false;
    private List<Geofence> mGeofenceList = new ArrayList<>();
    PendingIntent mGeofencePendingIntent;
    private boolean geoFence = false;


    private GoogleClientService() {

    }

    //method to create location request object
    public LocationRequest createLocationRequest(final long time) {
        mLocationRequest = LocationRequest.create();
        if (Constants.gps_mode == 1 || Constants.gps_mode == 2) { // gps mode
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            UPDATE_INTERVAL = time;
            FASTEST_INTERVAL = UPDATE_INTERVAL;
        } else if (Constants.gps_mode == 3) { // network mode
            mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
            UPDATE_INTERVAL = time;
            FASTEST_INTERVAL = UPDATE_INTERVAL;
        }
        Log.e("UPDATE_INTERVAL", "  " + UPDATE_INTERVAL);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        return mLocationRequest;
    }

    //method to create a instance of service class
    public static GoogleClientService getInstance(Context context) {
        mContext = context;
        if (instance == null) {
            instance = new GoogleClientService();
        }
        return instance;

    }

    //method to remove updates of google client
    public void removeUpdates() {
        if (client_connect && mLocationClient != null && mLocationClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mLocationClient, this);
            client_connect = false;
        }
        Log.d(TAG, "Location update stopped .......................");
    }


    //method to build the google client
    public void buildGoogleClient() {
        mLocationClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mLocationClient.connect();


    }

    //method to set the gps business listner to class
    public void setGPSBusinessListener(GpsBusinessListener gpsBusinessListener) {
        this.gpsBusinessListener = gpsBusinessListener;
        Log.d(TAG, "Listener set");


    }

    //method to remove the goefence
    public void removeGeoFence() {
        if (mLocationClient != null && mGeofencePendingIntent != null) {
            if (geoFence) {
                LocationServices.GeofencingApi.removeGeofences(mLocationClient, mGeofencePendingIntent).setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        if (status.isSuccess()) {
                            geoFence = false;
                            Log.i(TAG, "remove Geofence");

                        } else {
                            geoFence = false;
                            Log.e(TAG, "Remove Geofence failed: " + status.getStatusMessage() +
                                    " : " + status.getStatusCode());
                        }
                    }
                });
            }
        }
    }

    //method to create geo fence
    public void createGeoFence(String latitude, String longitude, int range) {
        if (client_connect = true) {

            if (ActivityCompat.checkSelfPermission(Constants.getmContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            if (latitude != null && longitude != null) {
                createGeoFences(Double.parseDouble(latitude), Double.parseDouble(longitude), range);
                LocationServices.GeofencingApi.addGeofences(
                        mLocationClient,
                        getGeoFencingRequest(),
                        getGeoFencePendingIntent()
                ).setResultCallback(new ResultCallback<Status>() {


                    @Override
                    public void onResult(Status status) {
                        if (status.isSuccess()) {
                            geoFence = true;
                            Log.i(TAG, "Registered Geofence");

                        } else {
                            geoFence = false;
                            Log.e(TAG, "Registering geofence failed: " + status.getStatusMessage() +
                                    " : " + status.getStatusCode());
                        }
                    }
                });
            }

        }
    }

    //method to create location updates
    public void requestForLocationUpdates(final long time) {
        if (client_connect = true) {
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            if (mLocationClient != null && mLocationClient.isConnected()) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient,
                        createLocationRequest(time), this);
            }
            Log.d(TAG, "Location request initiated");
        }
    }

    //method to get current gps location
    public void getCurrentGpsLocation() {
        if (client_connect) {
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(mLocationClient);
            if (lastLocation != null) {
                gpsBusinessListener.currentGpsLocation(lastLocation);

            }


        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "Google Client connected");
        client_connect = true;

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        client_connect = false;
        Log.d(TAG, "Google Client failed");
    }

    @Override
    public void onLocationChanged(Location location) {

        if (location != null) {
            Log.d(TAG, "Location Update");
            if (checkInOutRange(location)) {
                gpsBusinessListener.outOfRange(location);
            }
            gpsBusinessListener.gpsLocation(location);

        }

    }

    //method to check the geo fence range
    public boolean checkInOutRange(Location location) {
        GeoFence geoFence = DatabaseMgr.getInstance(mContext).retrieveGeoFence();
        if (geoFence != null) {
            double latitude = 0, longitude = 0;
            int range = 0;
            if (geoFence.getLatitude() != null && geoFence.getLongitude() != null) {
                latitude = Double.parseDouble(geoFence.getLatitude());
                longitude = Double.parseDouble(geoFence.getLongitude());
                range = geoFence.getRange();
                if (range == 0) {
                    range = 100;
                }
            }

            if (latitude == 0 || longitude == 0) {
                return false;
            } else {
                Location centerPoint = new Location("");
                centerPoint.setLatitude(latitude);
                centerPoint.setLongitude(longitude);
                float[] dist = new float[2];

                Location.distanceBetween(location.getLatitude(), location.getLongitude(), centerPoint.getLatitude(), centerPoint.getLongitude(), dist);

                if (dist[0] > range) {
                    //here your code or alert box for outside 1Km radius area
                    Log.d(TAG, "Device outside the premises " + dist[0]);
                    return true;
                } else {
                    Log.d(TAG, "Device inside the premises " + dist[0]);
                    return false;
                }
            }


        }

        return false;
    }

    /**
     * Create a Geofence list
     */
    public void createGeoFences(double latitude, double longitude, int range) {
        if (range == 0) {
            range = 100;
        }
        String id = UUID.randomUUID().toString();
        Geofence fence = new Geofence.Builder()
                .setRequestId(id)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .setCircularRegion(latitude, longitude, range)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .build();
        if (fence != null)
            mGeofenceList.add(fence);
    }

    //method to create geofence request object
    private GeofencingRequest getGeoFencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(mGeofenceList);
        return builder.build();
    }

    private PendingIntent getGeoFencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(Constants.getmContext(), GeoFenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        return PendingIntent.getService(Constants.getmContext(), 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
    }


}
