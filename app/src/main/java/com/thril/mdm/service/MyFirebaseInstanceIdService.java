package com.thril.mdm.service;


import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.thril.mdm.Global.MDMSettings;

/**
 * Created by Ram on 12/8/2017.
 */

//service to create fcm token
public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {


    //this method will be called
    //when the token is generated
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        //now we will have the token
        String token = FirebaseInstanceId.getInstance().getToken();
        if(token!=null){
            MDMSettings.saveFireBaseToken(token);
        }

        //for now we are displaying the token in the log
        //copy it as this method is called only when the new token is generated
        //and usually new token is only generated when the app is reinstalled or the data is cleared
        if(token !=null) {
            Log.d("MyRefreshedToken", token);
        }
    }
}