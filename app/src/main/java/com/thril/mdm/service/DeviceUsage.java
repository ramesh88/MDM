package com.thril.mdm.service;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.TrafficStats;
import android.os.BatteryManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.google.gson.Gson;

import com.thril.mdm.Global.DateUtils;
import com.thril.mdm.Global.DeviceInfo;
import com.thril.mdm.Network.utils.Constants;
import com.thril.mdm.databaselayer.databasemanager.DatabaseMgr;
import com.thril.mdm.model.BatteryUsage;
import com.thril.mdm.model.DataConsumption;
import com.thril.mdm.model.SignalStrength;

/**
 * Created by Ram on 4/15/2018.
 */
//Intent service class to get DeviceUsage
public class DeviceUsage extends IntentService {

    BatteryBroadcastReceiver mReceiver =null;

    public DeviceUsage(){
        super("Device Usage");
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("Device Usage","Service Started");

        if(mReceiver == null) {
            mReceiver = new BatteryBroadcastReceiver();
            registerReceiver(mReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        }

        deviceSignalStrength();
        dataConsumption();
        /*if(mReceiver != null) {
            unregisterReceiver(mReceiver);
        }*/

    }

//method get the battery status of device
    private class BatteryBroadcastReceiver extends BroadcastReceiver {
        private final static String BATTERY_LEVEL = "level";

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
        @Override
        public void onReceive(Context context, Intent intent) {
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
            float batteryPct = (level / (float) scale) * 100;
            int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);

            // Determine the battery charging status
            boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING
                    ||
                    status == BatteryManager.BATTERY_STATUS_FULL;
            String chargingStatus = "On Battery";
            if (isCharging) {
                chargingStatus = "On Charging";
            }

            DatabaseMgr dbMgr = DatabaseMgr.getInstance(Constants.getmContext());
            BatteryUsage batteryUsage = new BatteryUsage(new DeviceInfo().getUUID(Constants.getmContext()), chargingStatus, "" + batteryPct, Long.toString(DateUtils.getCurrentDateGMT()));
            Log.d("json","batteryUsage \n"+new Gson().toJson(batteryUsage));
            dbMgr.insertBatteryStatusRecords(batteryUsage);
            if(mReceiver != null) {
                unregisterReceiver(mReceiver);
                mReceiver=null;
            }
            if(Constants.isNetworkAvailable(context)) {
                startService(new Intent(context, DeleteTokenService.class));
            }
            //dbMgr.getDeviceResponse();
            stopSelf();
        }
    }

    //method to get device signal strength of device
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void deviceSignalStrength() {
        DatabaseMgr dbMgr = DatabaseMgr.getInstance(Constants.getmContext());
        SignalStrength signalStrength = new SignalStrength(new DeviceInfo().getUUID(Constants.getmContext()), Constants.getNetworkCarrierName(Constants.getmContext()), "" + Constants.getSignalQuality(Constants.getSignalStrength(Constants.getmContext())), Long.toString(DateUtils.getCurrentDateGMT()));
        Log.d("json","signalStrength \n"+new Gson().toJson(signalStrength));
        dbMgr.insertSignalStrengthRecords(signalStrength);
    }

    //method insert data consumption of device to database
    public void dataConsumption() {
        DatabaseMgr dbMgr = DatabaseMgr.getInstance(Constants.getmContext());
        DataConsumption dataConsumption = new DataConsumption();
        String networkType = Constants.checkNetworkStatus(Constants.getmContext());
        dataConsumption.setDataType(networkType);
        String upload="", download="";
        String dataSource = "";
        if (networkType.equals("Wi-Fi")) {
            dataSource = Constants.getWifiName(Constants.getmContext());
            upload = Constants.bytesHumanReadable((TrafficStats.getTotalTxBytes() - TrafficStats.getMobileTxBytes()), true);
            download = Constants.bytesHumanReadable((TrafficStats.getTotalRxBytes() - TrafficStats.getMobileRxBytes()), true);
        } else if (networkType.equals("Mobile")) {
            dataSource = Constants.getNetworkCarrierName(Constants.getmContext());
            upload = Constants.bytesHumanReadable(TrafficStats.getMobileTxBytes(), true);
            download = Constants.bytesHumanReadable(TrafficStats.getMobileRxBytes(), true);
        }

        dataConsumption.setDataSource(dataSource);
        dataConsumption.setDataUpload(upload);
        dataConsumption.setDataDownload(download);
        dataConsumption.setDateTime(Long.toString(DateUtils.getCurrentDateGMT()));
        Log.d("json","dataConsumption \n"+new Gson().toJson(dataConsumption));
        dbMgr.insertDataConsumptionRecords(dataConsumption);

    }

}
