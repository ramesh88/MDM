package com.thril.mdm.service;

import android.app.ActivityManager;
import android.app.IntentService;
import android.app.Service;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.thril.mdm.Network.utils.Constants;
import com.thril.mdm.databaselayer.databasemanager.DatabaseMgr;
import com.thril.mdm.kiosk.MainActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by Ram on 6/13/2018.
 */

//Intent service to close other apps apart from mdm apps
public class StopSettingsService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */


    public StopSettingsService() {
        super("StopSettingsService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        while (true) {
            String currentApp = getAppList();
            if (Constants.settings_index == 1) {
                //getInstalledApps();
                DatabaseMgr databaseMgr = DatabaseMgr.getInstance(Constants.getmContext());
                List<String> appPackages = databaseMgr.retrieveAppPackages();
                if (currentApp.length() > 0) {
                    if (!appPackages.contains(currentApp.trim())) {
                        //killAppBypackage(currentApp);
                        Constants.frgndFlag = true;
                        ActivityManager activityManager = (ActivityManager) Constants.getmContext()
                                .getSystemService(Context.ACTIVITY_SERVICE);
                        activityManager.moveTaskToFront(((MainActivity) Constants.getmContext()).getTaskId(), 0);

//                        Intent startMain = new Intent(Constants.getmContext(), MainActivity.class);
//                        startMain.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                        startMain.addCategory(Intent.ACTION_MAIN);
//                        startMain.addCategory(Intent.CATEGORY_HOME);
//                        startMain.addCategory(Intent.CATEGORY_LAUNCHER);
//                        Constants.getmContext().startActivity(startMain);
//                        break;
                    }
                }
            }


//            if (!currentApp.equals("") && currentApp.trim().toLowerCase().indexOf("setting") != -1) {
//                //Log.e("PackageName Inside", currentApp);
//                if (Constants.settings_index == 1) {
//                    Constants.frgndFlag = true;
//                    ActivityManager activityManager = (ActivityManager) Constants.getmContext()
//                            .getSystemService(Context.ACTIVITY_SERVICE);
//                    activityManager.moveTaskToFront(((MainActivity) Constants.getmContext()).getTaskId(), 0);
//                    //   stopSelf();
////                    //sendBroadCast(1);
//                    //   break;
//                }
//            } else if (!currentApp.equals("") && currentApp.trim().toLowerCase().indexOf("chrome") != -1) {
//                Log.e("PackageName Inside", currentApp);
//                if (Constants.settings_index == 1) {
//                    Constants.frgndFlag = true;
//                    ActivityManager activityManager = (ActivityManager) Constants.getmContext()
//                            .getSystemService(Context.ACTIVITY_SERVICE);
//                    activityManager.moveTaskToFront(((MainActivity) Constants.getmContext()).getTaskId(), 0);
//                    //   stopSelf();
////                    //sendBroadCast(1);
//                    //   break;
//                }
//            } else if (!currentApp.equals("") && currentApp.trim().toLowerCase().indexOf("browser") != -1) {
//                if (Constants.settings_index == 1) {
//                    Constants.frgndFlag = true;
//                    ActivityManager activityManager = (ActivityManager) Constants.getmContext()
//                            .getSystemService(Context.ACTIVITY_SERVICE);
//                    activityManager.moveTaskToFront(((MainActivity) Constants.getmContext()).getTaskId(), 0);
//                    //   stopSelf();
////                    //sendBroadCast(1);
//                    //   break;
//                }
//            } else if (!currentApp.equals("") && currentApp.trim().toLowerCase().indexOf("googlequicksearchbox") != -1) {
//                if (Constants.settings_index == 1) {
//                    Constants.frgndFlag = true;
//                    ActivityManager activityManager = (ActivityManager) Constants.getmContext()
//                            .getSystemService(Context.ACTIVITY_SERVICE);
//                    activityManager.moveTaskToFront(((MainActivity) Constants.getmContext()).getTaskId(), 0);
//                    //   stopSelf();
////                    //sendBroadCast(1);
//                    //    break;
//                }
//            }
//            else if (!currentApp.equals("") && currentApp.trim().toLowerCase().indexOf("facebook") != -1) {
//                if (Constants.settings_index == 1) {
//                    Constants.frgndFlag = true;
//                    ActivityManager activityManager = (ActivityManager) getApplicationContext()
//                            .getSystemService(Context.ACTIVITY_SERVICE);
//                    activityManager.moveTaskToFront(((MainActivity) Constants.getmContext()).getTaskId(), 0);
//                    stopSelf();
//                    //sendBroadCast(1);
//                    break;
//                }
//            } else if (!currentApp.equals("") && currentApp.trim().toLowerCase().indexOf("twitter") != -1) {
//                if (Constants.settings_index == 1) {
//                    Constants.frgndFlag = true;
//                    ActivityManager activityManager = (ActivityManager) getApplicationContext()
//                            .getSystemService(Context.ACTIVITY_SERVICE);
//                    activityManager.moveTaskToFront(((MainActivity) Constants.getmContext()).getTaskId(), 0);
//                    stopSelf();
//                    //sendBroadCast(1);
//                    break;
//                }
//            } else if (!currentApp.equals("") && currentApp.trim().toLowerCase().indexOf("linkedin") != -1) {
//                if (Constants.settings_index == 1) {
//                    Constants.frgndFlag = true;
//                    ActivityManager activityManager = (ActivityManager) getApplicationContext()
//                            .getSystemService(Context.ACTIVITY_SERVICE);
//                    activityManager.moveTaskToFront(((MainActivity) Constants.getmContext()).getTaskId(), 0);
//                    stopSelf();
//                    //sendBroadCast(1);
//                    break;
//                }
//            } else if (!currentApp.equals("") && currentApp.trim().toLowerCase().indexOf("plus") != -1) {
//                if (Constants.settings_index == 1) {
//                    Constants.frgndFlag = true;
//                    ActivityManager activityManager = (ActivityManager) getApplicationContext()
//                            .getSystemService(Context.ACTIVITY_SERVICE);
//                    activityManager.moveTaskToFront(((MainActivity) Constants.getmContext()).getTaskId(), 0);
//                    stopSelf();
//                    //sendBroadCast(1);
//                    break;
//                }
//            }
//            else if (!currentApp.equals("") && currentApp.trim().toLowerCase().indexOf("play") != -1) {
//                if (Constants.settings_index == 1) {
//                    Constants.frgndFlag = true;
//                    ActivityManager activityManager = (ActivityManager) getApplicationContext()
//                            .getSystemService(Context.ACTIVITY_SERVICE);
//                    activityManager.moveTaskToFront(((MainActivity) Constants.getmContext()).getTaskId(), 0);
//                    // stopSelf();
//                    //sendBroadCast(1);
//                    // break;
//                }
//            }
        }

    }

    //method used to get the package name of forground app
    public String getAppList() {
        String currentApp = "";

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {

            UsageStatsManager usm = (UsageStatsManager) getSystemService("usagestats");
            long time = System.currentTimeMillis();
            List<UsageStats> appList = usm.queryUsageStats(UsageStatsManager.INTERVAL_DAILY,
                    time - 1000 * 1000, time);
            if (appList != null && appList.size() > 0) {
                SortedMap<Long, UsageStats> mySortedMap = new TreeMap<Long, UsageStats>();
                for (UsageStats usageStats : appList) {
                    mySortedMap.put(usageStats.getLastTimeUsed(),
                            usageStats);
                }
                if (mySortedMap != null && !mySortedMap.isEmpty()) {
                    currentApp = mySortedMap.get(
                            mySortedMap.lastKey()).getPackageName();
                }
            }
        } else {
            ActivityManager am = (ActivityManager) getBaseContext().getSystemService(ACTIVITY_SERVICE);
            currentApp = am.getRunningTasks(1).get(0).topActivity.getPackageName();

        }
    //    Log.e("PackageName", currentApp);
        return currentApp;
    }


}
