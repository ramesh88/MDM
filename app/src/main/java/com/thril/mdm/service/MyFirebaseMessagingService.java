package com.thril.mdm.service;

/**
 * Created by Ram on 6/9/2018.
 */

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.thril.mdm.Global.DateUtils;
import com.thril.mdm.Network.utils.Constants;
import com.thril.mdm.databaselayer.databasemanager.DatabaseMgr;
import com.thril.mdm.model.NotificationData;

import java.util.List;
import java.util.Map;


//Service to recieve notifcations
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e("MyRefreshedToken", "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e("MyRefreshedToken", "Notification Body: " + remoteMessage.getNotification().getBody());


            //handleNotification(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e("MyRefreshedToken", "Data Payload: " + remoteMessage.getData());
            Map<String,String> map= remoteMessage.getData();
            /*for (Map.Entry<String, String> entry : map.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                Log.d("Map Value","Key "+key+" Value "+value);
            }*/
             String data=new Gson().toJson(map);
             Log.d("data",""+data);
             NotificationData notificationData = new Gson().fromJson(data, NotificationData.class);
             notificationData.setNotificationDate(DateUtils.getCurrentDate("dd-MM-yyyy HH:mm:ss"));
             notificationData.setNotificationTime(DateUtils.getNotificationTime());
             DatabaseMgr.getInstance(Constants.getmContext()).insertNotificationData(notificationData);
            if(isTopMainActivity()) {
                Log.d("MyRefreshedToken", "Inside TOpMainActivity");
                sendBroadCast(DatabaseMgr.getInstance(Constants.getmContext()).getNotificationDataCount());
            }
        }
        super.onMessageReceived(remoteMessage);
    }

    public boolean isTopMainActivity(){
        ActivityManager am = (ActivityManager)Constants.getmContext().getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> alltasks = am
                .getRunningTasks(1);
        for (ActivityManager.RunningTaskInfo aTask : alltasks) {
            if(aTask.topActivity.getClassName().equals("com.thril.mdm.kiosk.MainActivity") || aTask.topActivity.getClassName().equals("com.thril.mdm.BaseActivity")){
               return true;
            }
        }
        return false;
    }
//method send broad cast messages to mainatcivity
    public void sendBroadCast(int count){
        Intent intent = new Intent(Constants.NOTIFICATION_DATA_UPDATE);
        intent.putExtra("count",count);
        LocalBroadcastManager.getInstance(Constants.getmContext()).sendBroadcast(intent);
    }

}