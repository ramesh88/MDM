package com.thril.mdm.service;

import android.app.IntentService;
import android.content.Intent;

/**
 * Created by Admin on 5/26/2018.
 */

import android.location.Location;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;
import com.thril.mdm.Global.DateUtils;
import com.thril.mdm.Network.utils.Constants;
import com.thril.mdm.databaselayer.databasemanager.DatabaseMgr;
import com.thril.mdm.model.GpsTrackerViolation;
/**
 * Created by Ram on 4/15/2018.
 */
//Intent service class to get geo fencing
public class GeoFenceTransitionsIntentService extends IntentService {

    private static final String TAG = "GeofenceTransitions";

    public GeoFenceTransitionsIntentService() {
        super("GeofenceTransitionsIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            Log.e(TAG, "Goefencing Error " + geofencingEvent.getErrorCode());
            return;
        }

        // Get the transition type.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();
        Location location;
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
            Log.d("GeofenceTransition","Entered");
            location=geofencingEvent.getTriggeringLocation();
            //showNotification("Entered", "Entered the Location");
        } else if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
            Log.d("GeofenceTransition","Exit");
            // showNotification("Exited", "Exited the Location");
            location=geofencingEvent.getTriggeringLocation();
            GpsTrackerViolation gpsTrackerViolation=new GpsTrackerViolation(Double.toString(location.getLatitude()),Double.toString(location.getLongitude()),Long.toString(DateUtils.getCurrentDateGMT()));
            DatabaseMgr.getInstance(Constants.getmContext()).insertGpsTrackerViolationRecords(gpsTrackerViolation);
        }else{
            Log.d("GeofenceTransition","error");
        }

 /*   public void showNotification(String text, String bigText) {

        // 1. Create a NotificationManager
        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        // 2. Create a PendingIntent for AllGeofencesActivity
        Intent intent = new Intent(this, Constants.getmContext());
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingNotificationIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // 3. Create and send a notification
        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(text)
                .setContentText(text)
                .setContentIntent(pendingNotificationIntent)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(bigText))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .build();
        notificationManager.notify(0, notification);
    }*/
    }
}