package com.thril.mdm.Presenter;

import android.content.Context;
import android.net.wifi.ScanResult;

import com.thril.mdm.business.WifiBusiness;
import com.thril.mdm.view.AppView;

import java.util.List;

/**
 * Created by Ram on 1/20/2018.
 */

//Wifi presenter class for wifi operations
public class WifiPresenter implements WifiPresenterListener{
    Context mContext;
    private static AppView mWifiView;

    //constructor for the class
    public WifiPresenter(Context context, AppView wifiView){
        mContext=context;
        mWifiView=wifiView;
        WifiBusiness.getInstance(context).setWifiPresenterListener(this);
    }


//method to enable hot spot of device
    public void enableHostSpot(){
        WifiBusiness.getInstance(mContext).enableHotspot();
    }

    //method to disbale hotspot of device
    public void disableHotspot(){
           WifiBusiness.getInstance(mContext).disableHotspot(mContext);
    }

    @Override //overriden method of wifi presneter to invoke the list of enabled wifi
    public void discoveryList(List<String> discoveryList, boolean flag, List<ScanResult> scanResultList) {
        mWifiView.discoveryList(discoveryList,flag,scanResultList);

    }

    @Override //overriden method of wifi presneter to invoke to conncet the hot spot
    public void hotSpotName(String hotSpotName, String password) {
        mWifiView.hotSpotName(hotSpotName,password);

    }

    @Override//overriden method of wifi presneter to invoke to disable the hotspot
    public void disableHotspot(boolean flag) {
        mWifiView.disableHotspot(flag);
    }

    @Override //overriden method of wifi presneter to scan the hot spots
    public void scanHotSpots(boolean flag) {
        WifiBusiness.getInstance(mContext).scanHotSpots(flag);
    }

    @Override //overriden method of wifi presneter to get hot spot connection status
    public void hotSpotConnectStatus(boolean flag) {
         mWifiView.hopSpotConnectionStatus(flag);
    }

    @Override //overriden method of wifi presneterto get socket status of hotspot
    public void socketStatus(String socketStatus,boolean socketFlag) {
        mWifiView.socketStatus(socketStatus,socketFlag);
    }

    @Override //overriden method of wifi presneter toremove the network
    public void removeNetwork() {
        WifiBusiness.getInstance(mContext).removeNetwork();
    }

    @Override //overriden method of wifi presneter to iupdate the progress of file
    public void updateProgress(int percentage) {
         mWifiView.updateProgress(percentage);
    }

    @Override //overriden method of wifi presneter to get the file ternsfer status
    public void fileStatus(boolean flag) {
        mWifiView.fileStatus(flag);
    }

    @Override //overriden method of wifi presneter to check wifi network
    public boolean checkWifiNetwork(String ssid) {
        return WifiBusiness.getInstance(mContext).checkWifiConnected(ssid);
    }

    @Override //overriden method of wifi presneter to get security type of wifi
    public int getSecurityType(ScanResult scanResult) {
        return WifiBusiness.getInstance(mContext).getWifiSecurityType(scanResult);
    }

    @Override //overriden method of wifi presneter to connect to a wifi
    public boolean connectToWifi(String ssid, int type, String password) {
        return WifiBusiness.getInstance(mContext).connectToNetwork(ssid,type,password);
    }

    @Override //overriden method of wifi presneter to iget connected wifi
    public void getConnectedWifi() {
         mWifiView.connectedWifi(WifiBusiness.getInstance(mContext).getConnectedNetwork());
    }

    //method to display the password connection dialog
    public void displayPasswordDialog(String hotSpot_name){
        WifiBusiness.getInstance(mContext).showPasswordDialog(hotSpot_name);
    }


    //method to invoke send file operation
    public void sendFile(String filePath){
        WifiBusiness.getInstance(mContext).sendFile(filePath);
    }

    //method to get the file list of device
    public void filesList(String fileExtension){
        List<String> filesList=WifiBusiness.getInstance(mContext).getFilesInfo(fileExtension);
        mWifiView.fileList(filesList);
    }

}
