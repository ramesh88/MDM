package com.thril.mdm.Presenter;

import android.content.Context;
import android.location.Location;

import com.thril.mdm.Global.MDMSettings;
import com.thril.mdm.business.GpsServiceBusiness;
import com.thril.mdm.view.AppView;

/**
 * Created by Ram on 1/13/2018.
 */

//Presenter class for gps operations
public class GpsPresenter extends BasePresenter implements GpsPresenterListener {
    AppView homeView;
    Context mContext;

//consturctor for the class
    public GpsPresenter(Context context, AppView homeView) {
        this.mContext = context;
        this.homeView = homeView;
        GpsServiceBusiness.getInstance(context).setGPSPresenterListener(this);
    }

    //method to start the gps service
    public void startGpsService(long timeInterval) {
        GpsServiceBusiness.getInstance(mContext).startGPSService(MDMSettings.GPS_SERVICE,timeInterval);

        //homeView.gpsServiceStatus("Gps Service has been started");
    }
//method to stop the gps service
    public void stopGpsService() {
        GpsServiceBusiness.getInstance(mContext).stopGPSService(MDMSettings.GPS_SERVICE);
      //  homeView.gpsServiceStatus("Gps Service has been stoped");
    }


    @Override //overriden method of gps presenter  to display gps location
    public void gpsLocation(Location location) {
        homeView.gpsLocation(location);

    }

    @Override//overriden method of gps presenter  to display gps status
    public void gpsStatus() {
        homeView.gpsServiceStatus("Gps Service has been started");
    }

    @Override//overriden method of gps presenter  to display current gps  location
    public void currentGpsLocation(Location location) {
        homeView.currentGpsLocation(location);
    }

    //method to get current gps location
    public void getCurrentGpsLocation() {
        GpsServiceBusiness.getInstance(mContext).getCurrentGpsLocation();
    }
}
