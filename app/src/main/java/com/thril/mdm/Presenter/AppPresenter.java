package com.thril.mdm.Presenter;

import android.content.Context;
import android.util.Log;

import com.thril.mdm.BuildConfig;
import com.thril.mdm.Global.DeviceInfo;
import com.thril.mdm.Global.MDMSettings;
import com.thril.mdm.business.AppBusiness;
import com.thril.mdm.model.AppUsage;
import com.thril.mdm.model.Device;
import com.thril.mdm.model.DeviceUsage;
import com.thril.mdm.model.NotificationData;
import com.thril.mdm.model.ResponseData;
import com.thril.mdm.model.StaffInfo;
import com.thril.mdm.model.UserControl;
import com.thril.mdm.view.AppView;

import java.util.List;

/**
 * Created by Ram on 2/4/2018.
 */
//Present class for the appview
public class AppPresenter extends BasePresenter implements AppPresenterListener {
    Context mContext;
    AppView mAppView;
    AppBusiness mAppBusiness;
    Device device;
    private static final String TAG = "Presenter";

    //COnstructor for the class
    public AppPresenter(Context context, AppView appView) {
        mContext = context;
        mAppView = appView;
        Log.d(TAG, "Presenter");
        mAppBusiness = AppBusiness.getInstance(mContext);
        mAppBusiness.setAppPresenterListener(this);
    }

    //method to retrve app list
    public void getAppList() {
        device = new Device(new DeviceInfo(), mContext, MDMSettings.getFireBaseToken(), new DeviceInfo().getDeviceIMEI(mContext), BuildConfig.VERSION_NAME);
        displayProgressDialog("Please wait...!", "First time setup is in progress...");
        mAppBusiness.getAppList(device);

    }

    //method to update database coloumn
    public void updateDBColumn(String columnName, String columnValue, boolean columnType) {
        mAppBusiness.updateDBColumn(columnName, columnValue, columnType);
    }

    //method to delete notificatiom
    public void deleteNotification(String not_id) {
        mAppBusiness.deleteNotification(not_id);
    }

    //method to delete all notifications
    public void deleteAllNotification() {
        mAppBusiness.deleteAllNotification();
    }

    //method to get notifcation count
    public void getNotificationCount() {
        mAppBusiness.getNotificationCount();
    }


    @Override
    //overriden method of app presenter which invokes when regustartion success with server
    public void onAppRegistrationSuccess(ResponseData responseData) {
        dismissProgressDialog();
        mAppView.onAppRegistrationSuccess(responseData);

    }

    @Override//overriden method of app presenter which invokes when regustartion failure with server
    public void onAppRegistrationFailure(String error) {
        dismissProgressDialog();
        mAppView.onAppRegistrationFailure(error);
    }

    @Override
//overriden method of app presenter which invokes when asset validation success with server
    public void onAssetValidationSuccess(StaffInfo staffInfo) {
        dismissProgressDialog();
        mAppView.onAssetValidationSuccess(staffInfo);

    }

    @Override
//overriden method of app presenter which invokes when asset validation failure with server
    public void onAssetValidationFailure(String error) {
        dismissProgressDialog();
        mAppView.onAssetValidationFailure(error);

    }

    //method to get notification list
    public void getNotificationList() {
        mAppBusiness.getNotificationList();
    }

    @Override //overriden method of app presenter to get notification count
    public void notificationCount(int count) {
        mAppView.notificationCount(count);
    }

    @Override//overriden method of app presenter to get notification list to view
    public void notificationList(List<NotificationData> notificationDataList) {
        mAppView.notificationList(notificationDataList);
    }

    @Override //overriden method of app presenter to delete notification
    public void notificationDelete(NotificationData notificationData) {

    }

    @Override //overriden method of app presenter to open dialog update of apk
    public void onDialogUpdateApk(String path) {
        dismissProgressDialog();
        mAppView.onDialogUpdateApk(path);
    }

    @Override //overriden method of app presenter to show exit dialog
    public void showExitDialog() {
        dismissProgressDialog();
        mAppView.showExitDialog();
    }

    @Override //overriden method of app presenter which invokes otp sending fialure with server
    public void onOTPSendingFailure(String error) {
        dismissProgressDialog();
        mAppView.onOTPSendingFailure(error);
    }

    @Override //overriden method of app presenter which invokes otp verification fialure with server
    public void onOTPVerifyingFailure(String error) {
        dismissProgressDialog();
        mAppView.onOTPVerifyFailure(error);
    }

    @Override  //overriden method of app presenter to exit the app
    public void exitApp() {
        dismissProgressDialog();
        mAppView.exitApp();
    }

    //method to verify asset identifier
    public void verifyAssetIdentifier(String assetIdentifier, String imeiNumber, String versionno, int index) {
        if (index == 0)
            displayProgressDialog("Please wait...!", "Checking asset identifier...");
        else
            displayProgressDialog("Please wait...!", "Checking for updates...");
        mAppBusiness.verifyAssetIdentifier(assetIdentifier, imeiNumber, versionno);

    }

    //method to send otp
    public void sendOTP(String assetIdentifier) {
        displayProgressDialog("Please wait...!", "Getting permission to exit...");
        mAppBusiness.sendOTP(assetIdentifier);

    }

    //method to verify the otp
    public void verifyOTP(String assetIdentifier, String otp) {
        displayProgressDialog("Please wait...!", "Validating OTP...");
        mAppBusiness.verifyOTP(assetIdentifier, otp);

    }

    //method to save device usage records
    public void saveDeviceUsage(DeviceUsage deviceUsage) {
        mAppBusiness.saveDeviceUsage(deviceUsage);
    }

    //method to save app usage records
    public void saveAppUsage(AppUsage appUsage) {
        mAppBusiness.saveAppUsage(appUsage);
    }

    //method to save user control
    public UserControl getUserControls() {
        return mAppBusiness.getUserControls();
    }
}
