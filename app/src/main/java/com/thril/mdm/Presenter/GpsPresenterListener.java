package com.thril.mdm.Presenter;

import android.location.Location;

/**
 * Created by Ram on 1/13/2018.
 */

//Interface for the GPS Presenter class
public interface GpsPresenterListener {

    void gpsLocation(Location location);
    void gpsStatus();
    void currentGpsLocation(Location location);
}
