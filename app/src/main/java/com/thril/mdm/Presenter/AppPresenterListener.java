package com.thril.mdm.Presenter;

import com.thril.mdm.model.NotificationData;
import com.thril.mdm.model.ResponseData;
import com.thril.mdm.model.StaffInfo;

import java.util.List;

/**
 * Created by Ram on 2/4/2018.
 */

//Interface listener for the app presenter class
public interface AppPresenterListener {

    void onAppRegistrationSuccess(ResponseData responseData);

    void onAppRegistrationFailure(String error);

    void onAssetValidationSuccess(StaffInfo staffInfo);

    void onAssetValidationFailure(String error);

    void notificationCount(int count);

    void notificationList(List<NotificationData> notificationDataList);

    void notificationDelete(NotificationData notificationData);

    void onDialogUpdateApk(String path);

    void showExitDialog();

    void onOTPSendingFailure(String error);

    void onOTPVerifyingFailure(String error);

    void exitApp();

}
