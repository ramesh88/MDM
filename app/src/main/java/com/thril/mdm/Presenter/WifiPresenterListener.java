package com.thril.mdm.Presenter;

import android.net.wifi.ScanResult;

import java.util.List;

/**
 * Created by Ram on 1/20/2018.
 */

//Interface listner for wifi presneter class
public interface WifiPresenterListener {
    void discoveryList(List<String> discoveryList, boolean flag, List<ScanResult> scanResultList);
    void hotSpotName(String hotSpotName, String password);
    void disableHotspot(boolean flag);
    void scanHotSpots(boolean flag);
    void hotSpotConnectStatus(boolean flag);
    void socketStatus(String socketStatus,boolean socketFlag);
    void removeNetwork();
    void updateProgress(int percentage);
    void fileStatus(boolean flag);
    boolean checkWifiNetwork(String ssid);
    int getSecurityType(ScanResult scanResult);
    boolean connectToWifi(String ssid,int type,String password);
    void getConnectedWifi();


}
