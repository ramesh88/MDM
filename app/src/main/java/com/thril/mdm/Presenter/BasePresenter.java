package com.thril.mdm.Presenter;

import android.app.ProgressDialog;

import com.thril.mdm.Network.utils.Constants;

/**
 * Created by Ram on 2/4/2018.
 */

//base presenter class for displaying and dismisisng progress dialog
public class BasePresenter {
    public ProgressDialog mProgressDialog;

    public void displayProgressDialog(String title,String message){
        mProgressDialog = new ProgressDialog(Constants.getmContext());
        mProgressDialog.setTitle(title);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }
    public void dismissProgressDialog(){
        mProgressDialog.dismiss();
    }

}
