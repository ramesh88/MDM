package com.thril.mdm.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RadioButton;

import com.thril.mdm.R;
/**
 * Created by Ram on 6/23/2018.
 */
//Custom class for making Radiobutton styleable
public class CustomRadioButton extends RadioButton {
    public CustomRadioButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomRadioButton(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/"+ getContext().getString(R.string.text_typeface));
        setTypeface(tf);
    }
}
