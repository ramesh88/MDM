package com.thril.mdm.customviews;

import android.app.AlertDialog;
import android.content.Context;

/**
 * Created by Ram on 6/23/2018.
 */

//Custom class for making alert dialog styleable
public class MyAlertDialog extends AlertDialog.Builder {
    private String tag;

    public MyAlertDialog(Context context) {
        super(context);
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String setTag) {
        this.tag = setTag;
    }
}
