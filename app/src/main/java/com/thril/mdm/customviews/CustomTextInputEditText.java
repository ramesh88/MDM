package com.thril.mdm.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;

import com.thril.mdm.R;
/**
 * Created by Ram on 6/23/2018.
 */

//Custom class for making Textinput Edittext styleable
public class CustomTextInputEditText extends TextInputEditText {

    public CustomTextInputEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomTextInputEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTextInputEditText(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/"+  getContext().getString(
                        R.string.text_typeface));
        setTypeface(tf);
    }

}
