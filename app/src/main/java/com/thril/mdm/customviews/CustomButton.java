package com.thril.mdm.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.thril.mdm.R;
/**
 * Created by Ram on 6/23/2018.
 */
//Custom class for making button styleable
public class CustomButton extends Button {
	public CustomButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public CustomButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public CustomButton(Context context) {
		super(context);
		init();
	}

	private void init() {
		Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
				"fonts/"+getContext().getString(R.string.text_typeface));
		setTypeface(tf);
	}
}
