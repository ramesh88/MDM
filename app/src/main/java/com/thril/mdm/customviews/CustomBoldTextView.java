package com.thril.mdm.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.thril.mdm.R;
/**
 * Created by Ram on 6/23/2018.
 */
//Custom class for making textview to bold style
public class CustomBoldTextView extends TextView {

    public CustomBoldTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomBoldTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/"+getContext().getString(R.string.bold_text_typeface));
        setTypeface(tf);
    }
}
