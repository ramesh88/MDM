package com.thril.mdm.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thril.mdm.Network.utils.Constants;
import com.thril.mdm.R;
import com.thril.mdm.business.WifiBusiness;
import com.thril.mdm.kiosk.MainActivity;

import java.util.List;
/**
 * Created by Ram on 2/4/2018.
 */

//Adapter class for the spinner items using base adapter
public class SpinnerAdapter extends BaseAdapter {

    private Context mContext;
    private List<String> listObject;
    private Object object;

    //Constructor for adapter with parameters context and list of strings
    public SpinnerAdapter(Context mContext, List<String> listObject) {
        this.mContext = mContext;
        this.listObject = listObject;
    }

    @Override //method which returns no.of items
    public int getCount() {
        return listObject.size();
    }

    @Override //method which returns a object based on position
    public Object getItem(int position) {
        return listObject.get(position);
    }

    @Override //method returns item id based on position
    public long getItemId(int position) {
        return 0;
    }

    //method used for appending data to view based on position in the list of strings
    @SuppressLint("SimpleDateFormat")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        object = listObject.get(position);

        final ImageView checkBox;
        final TextView textViewTitle;
        final LinearLayout ll_layout;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_spinner_checkbox, null, false);

            checkBox = (ImageView) convertView.findViewById(R.id.chk_spinner);
            textViewTitle = (TextView) convertView.findViewById(R.id.tv_title);
            ll_layout = (LinearLayout) convertView.findViewById(R.id.ll_object);
            ll_layout.setTag(position);

            convertView.setTag(new ViewHolder(checkBox, textViewTitle, ll_layout));
        } else {
            ViewHolder viewHolder = (ViewHolder) convertView.getTag();
            checkBox = viewHolder.getCheckBox();
            textViewTitle = viewHolder.getTextViewTitle();
            ll_layout = viewHolder.getLayout();
            ll_layout.setTag(position);
        }

        checkBox.setTag(object);


            checkBox.setVisibility(View.GONE);
            textViewTitle.setSingleLine(false);
            textViewTitle.setText(object.toString());
            if(WifiBusiness.getInstance(Constants.getmContext()).checkWifiConnected(object.toString())){
               checkBox.setVisibility(View.VISIBLE);
            }

        ll_layout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                    if (mContext instanceof MainActivity) {
                        int position=(Integer) ll_layout.getTag();
                        ((MainActivity) mContext).notifySpinnerData(textViewTitle.getText().toString(),position);
                    }


                notifyDataSetChanged();
            }
        });

        return convertView;
    }

    //Holder class to intialize the custom layout files
    class ViewHolder {

        ImageView checkBox;
        TextView textViewTitle;
        LinearLayout ll_layout;

        public ViewHolder(ImageView checkBox, TextView textViewTitle, LinearLayout ll_layout) {
            super();
            this.checkBox = checkBox;
            this.textViewTitle = textViewTitle;
            this.ll_layout = ll_layout;
        }

        public LinearLayout getLayout() {
            return ll_layout;
        }

        public ImageView getCheckBox() {
            return checkBox;
        }

        public TextView getTextViewTitle() {
            return textViewTitle;
        }
    }
}