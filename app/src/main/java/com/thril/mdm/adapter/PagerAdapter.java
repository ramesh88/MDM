package com.thril.mdm.adapter;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.thril.mdm.model.ResponseData;
import com.thril.mdm.view.fragments.NewContact;
import com.thril.mdm.view.fragments.PhoneContacts;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ram on 2/4/2018.
 */

//Adapter for appending fragments to the view pager in activity of tab layout
public class PagerAdapter extends FragmentStatePagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();;


//Constructor with parameter of fragment manager
    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override //method to retrun a fragmment based on position
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    //method to add fragment along with title
    public void addFrag(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
        notifyDataSetChanged();
    }

    //method to clear the added fragment and titles
    public void clear() {
        mFragmentList.clear();
        mFragmentTitleList.clear();
    }

    @Override //method retrun the title of frgament based on position
    public String getPageTitle(int position) {
        return "";
    }


    @Override//method retruns count of items
    public int getCount() {
        return mFragmentList.size();
    }

    @Override //method retruns a paracelable object
    public Parcelable saveState() {
        return null;
    }
}
