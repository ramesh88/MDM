package com.thril.mdm.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thril.mdm.R;
import com.thril.mdm.model.NotificationData;
import com.thril.mdm.view.AppView;

import java.util.List;

/**
 * Created by Ram on 2/4/2018.
 */

//Adapter for appending list of notifications to notification page
public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.MyViewHolder> {

    private List<NotificationData> notificationDataList;
    Context context;
    AppView listener;

    //Holder class to intialize the custom layout files
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_title;
        public TextView tv_date;
        public TextView tv_time;
        public ImageView iv_expand;
        public TextView notification_content;
        public LinearLayout ll;
        public CheckBox item_select;

        public MyViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            tv_title = (TextView) view.findViewById(R.id.notification_title);
            tv_date = (TextView) view.findViewById(R.id.notification_date);
            tv_time = (TextView) view.findViewById(R.id.notification_time);
            iv_expand = (ImageView) view.findViewById(R.id.iv_expand);
            notification_content = (TextView) view.findViewById(R.id.notification_content);
            ll = (LinearLayout) view.findViewById(R.id.ll);
            item_select = (CheckBox) view.findViewById(R.id.item_select);
        }


        @Override //on click action when click on notification from notifcation list
        public void onClick(View view) {

            //listener.onNotificationAction(this.getLayoutPosition());
            //   Toast.makeText(context,"Position:"+this.getLayoutPosition(),Toast.LENGTH_SHORT).show();
        }
    }

    //Constructor for the adapter with parameters context ,array list of notification and interface listener
    public NotificationListAdapter(Context context, List<NotificationData> notificationDataList, AppView listener) {
        this.notificationDataList = notificationDataList;
        this.context = context;
        this.listener = listener;
    }

    @Override//method which return custom layout file holder
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_list_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override//method used for appending data to view based on position in the list of notification
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        NotificationData notificationData = notificationDataList.get(position);
        String title = notificationData.getTitle();
        String content = notificationData.getContent();
        holder.tv_title.setText(notificationData.getTitle());
        if (title.contains("<html")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                holder.tv_title.setText(Html.fromHtml(title, Html.FROM_HTML_MODE_COMPACT));
            } else {
                holder.tv_title.setText(Html.fromHtml(title));
            }
        } else {
            holder.tv_title.setText(title);
        }
        holder.tv_date.setText(notificationData.getNotificationDate());
        holder.tv_time.setText(notificationData.getNotificationTime());
        holder.item_select.setTag(position);
        holder.iv_expand.setTag(0);
        holder.ll.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view == holder.ll) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        int obj = (int) holder.iv_expand.getTag();
                        if (obj == 0) {
                            holder.iv_expand.setImageResource(R.drawable.collapse);
                            holder.iv_expand.setTag(1);
                            holder.notification_content.setVisibility(View.VISIBLE);
                        } else {
                            holder.iv_expand.setImageResource(R.drawable.expand);
                            holder.iv_expand.setTag(0);
                            holder.notification_content.setVisibility(View.GONE);
                        }
                    }
                }
                return true;
            }
        });
        holder.item_select.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (compoundButton == holder.item_select) {
                    if (holder.item_select.isChecked()) {
                        int pos = (int) holder.item_select.getTag();
                        listener.notificationDelete(pos);
                    }
                }
            }
        });
//        holder.iv_expand.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                int obj = (int) holder.iv_expand.getTag();
//                if (obj == 0) {
//                    holder.iv_expand.setImageResource(R.drawable.collapse);
//                    holder.iv_expand.setTag(1);
//                    holder.notification_content.setVisibility(View.VISIBLE);
//                } else {
//                    holder.iv_expand.setImageResource(R.drawable.expand);
//                    holder.iv_expand.setTag(0);
//                    holder.notification_content.setVisibility(View.GONE);
//                }
//
//            }
//        });
        holder.notification_content.setVisibility(View.GONE);
        if (content.contains("<html")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                holder.notification_content.setText(Html.fromHtml(content, Html.FROM_HTML_MODE_COMPACT));
            } else {
                holder.notification_content.setText(Html.fromHtml(content));
            }
        } else {
            holder.notification_content.setText(content);
        }
    }

    @Override//returns no.of items count
    public int getItemCount() {
        return notificationDataList.size();
    }


}
