package com.thril.mdm.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thril.mdm.Global.ApkDownloader;
import com.thril.mdm.Global.MDMSettings;
import com.thril.mdm.Network.utils.Constants;
import com.thril.mdm.R;
import com.thril.mdm.model.App;
import com.thril.mdm.view.AppView;

import java.util.List;

/**
 * Created by Ram on 2/4/2018.
 */

//Adapter for appending apps to the list page1,page2 in home page
public class AppListAdapter extends RecyclerView.Adapter<AppListAdapter.MyViewHolder> {

    private List<App> appsList;
    Context context;
    AppView listener;

    //Holder class to intialize the custom layout files
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_title;
        public ImageView imageView;

        public MyViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            imageView = (ImageView) view.findViewById(R.id.image_view);
        }


        @Override  //on click action when click on app from app  list
        public void onClick(View view) {

            listener.onAction(this.getLayoutPosition());
            //   Toast.makeText(context,"Position:"+this.getLayoutPosition(),Toast.LENGTH_SHORT).show();
        }
    }


    //Constructor for the adapter with parameters context, array list of apps and interface listener
    public AppListAdapter(Context context, List<App> appsList, AppView listener) {
        this.appsList = appsList;
        this.context = context;
        this.listener = listener;
    }

    @Override //method which return custom layout file holder
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_app_list_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override //method used for appending data to view based on position in the list of apps
    public void onBindViewHolder(MyViewHolder holder, int position) {
        App app = appsList.get(position);
        String packageName = app.getPackageName();
        String type = app.getType();
        if (type.trim().equalsIgnoreCase("new") && ApkDownloader.appInstalledOrNot(context, packageName)) {
            holder.tv_title.setTextColor(Color.parseColor("#000000"));
        } else if (type.trim().equalsIgnoreCase("appupdate")) {
            holder.tv_title.setTextColor(Color.parseColor("#ffa500"));
        }else if (type.trim().equalsIgnoreCase("update")) {
            holder.tv_title.setTextColor(Color.parseColor("#000000"));
        } else {
            holder.tv_title.setTextColor(Color.RED);
        }
        holder.tv_title.setText(app.getTitle());
        // Bitmap bitmap= BitmapFactory.decodeByteArray(profile.getProfile_image(),0,profile.getProfile_image().length);
        // holder.imageView.setImageBitmap(Constants.drawableFromAssetFilename(app.getIconPath()));
        holder.imageView.setImageBitmap(Constants.getImageBitmap(app.getIconPath()));
        //   bitmap.recycle();
        //  bitmap=null;

    }

    @Override //returns no.of items count
    public int getItemCount() {
        return appsList.size();
    }


    //listener interface to perform any action on click on apps in a list
    public interface Listener {
        void addAction(int position);
    }
}
