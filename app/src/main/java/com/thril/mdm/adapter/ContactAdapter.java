package com.thril.mdm.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.thril.mdm.R;
import com.thril.mdm.model.Contacts;

import java.util.List;

/**
 * Created by Ram on 6/24/2018.
 */

//Adapter for appending list of contacts to contact page
public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.MyViewHolder> {

    private List<Contacts> contactsList;
    PhoneCallListener phoneCallListener;

    //Holder class to intialize the custom layout files
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        ImageView phoneButton, cancelButton;
        TextView phoneNumber;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.tv_contact_name);
            phoneButton = (ImageView) view.findViewById(R.id.btn_phone_call);
            cancelButton = (ImageView) view.findViewById(R.id.btn_cancel_contact);
            phoneNumber = (TextView) view.findViewById(R.id.tv_phone_number);
        }
    }


    //Constructor for the adapter with parameters array list of contacts and interface listener
    public ContactAdapter(List<Contacts> contactsList, PhoneCallListener phoneCallListener) {
        this.contactsList = contactsList;
        this.phoneCallListener = phoneCallListener;
    }

    @Override //method which return custom layout file holder
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_contacts, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override  //method used for appending data to view based on position in the list of contacts
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Contacts contacts = contactsList.get(position);
        holder.title.setText(contacts.getName());
        holder.phoneNumber.setText(contacts.getPhoneNumber());

        holder.phoneButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view == holder.phoneButton) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        phoneCallListener.callNumber(contacts.getPhoneNumber());
                    }
                }
                return true;
            }
        });
        holder.cancelButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view == holder.cancelButton) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        phoneCallListener.deleteNumber(contacts.getPhoneNumber(), position);
                    }
                }
                return true;
            }
        });
    }

    @Override //returns no.of items count
    public int getItemCount() {
        return contactsList.size();
    }


    //listener interface to perform any action like call number and delete number on click on contacts in a list
    public interface PhoneCallListener {
        void callNumber(String phoneNumber);

        void deleteNumber(String phoneNumber, int position);


    }
}
