package com.thril.mdm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.thril.mdm.Global.DateUtils;
import com.thril.mdm.Global.MDMSettings;
import com.thril.mdm.databaselayer.databasemanager.DatabaseMgr;
import com.thril.mdm.model.DeviceUsage;

/**
 * Created by Ram on 6/12/2018.
 */

//Receiver class to know the event of shut down
public class ShutDownReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("ShutDownReceiver","ShutDown event Triggered");
        DeviceUsage deviceUsage=new DeviceUsage();
        deviceUsage.setSwitchOnDateTime("");
        deviceUsage.setSwitchOffDateTime(Long.toString(DateUtils.getCurrentDateGMT()));
        DatabaseMgr.getInstance(context).insertDeviceUsageRecords(deviceUsage);
        MDMSettings.saveFirstDeviceTime(0);
    }
}
