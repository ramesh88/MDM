package com.thril.mdm.AppWSRepository;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.gson.Gson;
import com.thril.mdm.Network.ApiInterface;
import com.thril.mdm.Network.RetrofitApi;
import com.thril.mdm.Network.utils.Constants;
import com.thril.mdm.business.AppBusinessListener;
import com.thril.mdm.databaselayer.databasemanager.DatabaseMgr;
import com.thril.mdm.model.Device;
import com.thril.mdm.model.DeviceResponse;
import com.thril.mdm.model.OTPResponse;
import com.thril.mdm.model.ResponseData;
import com.thril.mdm.model.StaffInfo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Ram on 2/4/2018.
 */

//Class used to call web service using retrofit api and api interface
public class AppRegistration {
    private static Retrofit retrofitApi;
    private static ApiInterface apiInterface;

    private static AppRegistration mInstance = null;

//constructor for the class
    private AppRegistration() {

    }

    //Method returns the instance and creates a instance for the class
    public static AppRegistration getmInstance() {
        if (mInstance == null) {
            mInstance = new AppRegistration();
            retrofitApi = RetrofitApi.getRetrofit();
            Log.e("API", retrofitApi.baseUrl().toString());
            apiInterface = retrofitApi.create(ApiInterface.class);
        }
        return mInstance;
    }

    //method for calling registation rest API with paramaters listener and deviceinfo model class
    public void appRegistration(final AppBusinessListener appBusinessListener, Device deviceInfo) {
        // Call<ResponseData> call=apiInterface.appRegistration(deviceInfo);
        Log.d("input :", new Gson().toJson(deviceInfo));
        Call<ResponseData> call = apiInterface.deviceRegistration(deviceInfo);
        call.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                Log.d("Response Code :", "" + response.code());
                Log.d("Response json:", new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        if (appBusinessListener != null) {
                            appBusinessListener.onSuccess(response.body());
                        } else {
                            //DatabaseMgr.getInstance(Constants.getmContext()).updateResponseToDB(response.body());
                            //sendBroadCast(1,"Success");
                        }
                    } else if (response.body().getStatus() == 400) {
                        if (response.body().getApkPath() != null) {
                            appBusinessListener.onDialogUpdate(response.body().getApkPath());
                        }
                    } else if (response.body().getStatus() == 500) {
                        appBusinessListener.onFailure(response.body().getMessage());
                    }

                } else {
                    if (appBusinessListener != null) {
                        appBusinessListener.onFailure("Server Internal error :" + response.code());
                        //appBusinessListener.onSuccess(new Gson().fromJson(Constants.loadData("json/Response.json"), ResponseData.class));
                    } else {
                        //sendBroadCast(2,"Failure");
                    }
                }


            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Log.e("Response code:", t.getLocalizedMessage());
                if (appBusinessListener != null) {
                    appBusinessListener.onFailure(t.getLocalizedMessage());

                } else {
                    //sendBroadCast(3,"error");
                }

            }
        });

    }

    //method for calling sync rest API with paramaters  deviceResponse model class
    public void deviceResponse(final DeviceResponse deviceResponse) {
        Log.d("input :", new Gson().toJson(deviceResponse));
        Call<ResponseData> call = apiInterface.deviceResponse(deviceResponse);
        call.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                Log.d("Response Code :", "" + response.code());
                Log.d("Response json:", new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        DatabaseMgr.getInstance(Constants.getmContext()).deleteDeviceResponseTable();
                        DatabaseMgr.getInstance(Constants.getmContext()).updateResponseToDB(response.body());
                        sendBroadCast(1, response.body().getMessage());
                    } else if (response.body().getStatus() == 400) {
                        if (response.body().getApkPath() != null) {
                            sendBroadCast(5, response.body().getApkPath());
                        }
                    } else if (response.body().getStatus() == 500) {
                        sendBroadCast(2, response.body().getMessage());
                    }
                } else {
                    sendBroadCast(3, "Server Internal Error:" + response.code());
                }


            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
//                Log.e("Response code:",t.getLocalizedMessage());
                if (t != null)
                    sendBroadCast(4, t.getMessage());


            }
        });

    }

    //method for calling to verify asset identifier rest API with parameters listener, asset identifier , imei number and version no
    public void verifyAssetIdentifier(final AppBusinessListener appBusinessListener, String assetIdentifier, String imeiNumber,String versionNo) {
        Call<StaffInfo> call = apiInterface.verifyAssetIdentifier(assetIdentifier, imeiNumber,versionNo);
        call.enqueue(new Callback<StaffInfo>() {
            @Override
            public void onResponse(Call<StaffInfo> call, Response<StaffInfo> response) {
                Log.d("Response Code :", "" + response.code());
                Log.d("Response json:", new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                    StaffInfo staffInfo = response.body();
                    if (staffInfo.getStatus() == 200) {
                        appBusinessListener.onAssetValidationSuccess(response.body());

                    } else if (staffInfo.getStatus() == 400) {
                        if (staffInfo.getApkPath() != null) {
                            appBusinessListener.onDialogUpdate(response.body().getApkPath());
                        }
                    } else if (staffInfo.getStatus() == 500) {
                        appBusinessListener.onAssetValidationFailure(response.body().getMessage());
                    }

                } else { // Server internal error
                    appBusinessListener.onAssetValidationFailure("Server Internal Error:" + response.code());
                }
            }

            @Override
            public void onFailure(Call<StaffInfo> call, Throwable t) {
                if (t != null)
                    t.printStackTrace();
                appBusinessListener.onAssetValidationFailure(t.getMessage());
            }
        });
    }

//method to send any broadcast events to main activity class from this class
    public void sendBroadCast(int status, String message) {
        Intent intent = new Intent(Constants.SERVER_UPDATE_ACTION);
        intent.putExtra("status", status);
        intent.putExtra("message", message);
        LocalBroadcastManager.getInstance(Constants.getmContext()).sendBroadcast(intent);
    }

    //method for calling to send OTP rest API with parameters listener, asset identifier
    public void sendOTP(final AppBusinessListener appBusinessListener, String assetIdentifier) {
        Call<OTPResponse> call = apiInterface.sendOTP(assetIdentifier);
        call.enqueue(new Callback<OTPResponse>() {
            @Override
            public void onResponse(Call<OTPResponse> call, Response<OTPResponse> response) {
                Log.d("Response Code :", "" + response.code());
                Log.d("Response json:", new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                    OTPResponse staffInfo = response.body();
                    if (staffInfo.getStatus() == 200) {
                        appBusinessListener.showExitDialog();

                    } else if (staffInfo.getStatus() == 500) {
                        appBusinessListener.onOTPSendingFailure(response.body().getData());
                    }

                } else { // Server internal error
                    appBusinessListener.onOTPSendingFailure("Server Internal Error:" + response.code());
                }
            }

            @Override
            public void onFailure(Call<OTPResponse> call, Throwable t) {
                appBusinessListener.onOTPSendingFailure(t.getMessage());
            }
        });
    }

    //method for calling to verify OTP rest API with parameters listener, asset identifier, otp
    public void verifyOTP(final AppBusinessListener appBusinessListener, String assetIdentifier, String otp) {
        Call<OTPResponse> call = apiInterface.verifyOTP(assetIdentifier, otp);
        call.enqueue(new Callback<OTPResponse>() {
            @Override
            public void onResponse(Call<OTPResponse> call, Response<OTPResponse> response) {
                Log.d("Response Code :", "" + response.code());
                Log.d("Response json:", new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                    OTPResponse staffInfo = response.body();
                    if (staffInfo.getStatus() == 200) {
                        appBusinessListener.exitApp();

                    } else if (staffInfo.getStatus() == 500) {
                        appBusinessListener.onOTPVerifyingFailure(response.body().getData());
                    }

                } else { // Server internal error
                    appBusinessListener.onOTPVerifyingFailure("Server Internal Error:" + response.code());
                }
            }

            @Override
            public void onFailure(Call<OTPResponse> call, Throwable t) {
                appBusinessListener.onOTPVerifyingFailure(t.getMessage());
            }
        });
    }


}
