package com.thril.mdm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.thril.mdm.Global.DateUtils;
import com.thril.mdm.Network.utils.Constants;
import com.thril.mdm.kiosk.MainActivity;

/**
 * Created by Ram on 1/30/2018.
 */

//Receiver class used for know the event of prower off and restart
public class BroadcastReceiverOnBootComplete extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED)) {
            Constants.Device_Switch_On_Date_Time= Long.toString(DateUtils.getCurrentDateGMT());


//            Intent startMain = new Intent(context,MainActivity.class);
//            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startMain.addCategory(Intent.ACTION_MAIN);
//            startMain.addCategory(Intent.CATEGORY_HOME);
//            startMain.addCategory(Intent.CATEGORY_LAUNCHER);
//            context.startActivity(startMain);
            Log.d("Broadcast receiver ", "Inside");

//            PackageManager packageManager = context.getPackageManager();
//            ComponentName componentName = new ComponentName(context, MainActivity.class);
//            packageManager.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
//
//            Intent selector = new Intent(Intent.ACTION_MAIN);
//            selector.addCategory(Intent.CATEGORY_HOME);
//            selector.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            context.startActivity(selector);
//            packageManager.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_DEFAULT, PackageManager.DONT_KILL_APP);
//            Intent selector = new Intent(
//                    "android.intent.action.MAIN");
//            selector.addCategory("android.intent.category.HOME");
//            selector.setComponent(new ComponentName(
//                    "android",
//                    "com.android.internal.app.ResolverActivity"));
//            context.startActivity(selector);

        }
    }
}
