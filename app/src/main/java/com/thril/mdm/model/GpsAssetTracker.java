package com.thril.mdm.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram on 5/25/2018.
 */

//Model class for gps asset tarcker
public class GpsAssetTracker {

    @SerializedName("latitude")
    private String latitude;
    @SerializedName("longitude")
    private String longitude;
    @SerializedName("dateTime")
    private String dateTime;

    public GpsAssetTracker(){

    }
    public GpsAssetTracker(String latitude, String longitude, String dateTime){
        this.latitude=latitude;
        this.longitude=longitude;
        this.dateTime=dateTime;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String lon) {
        this.longitude = lon;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }


}
