package com.thril.mdm.model;

/**
 * Created by Ram on 6/24/2018.
 */

//Model class for contacts
public class Contacts {

    private String name;
    private String phoneNumber;

    public void Contacts(String name,String phoneNumber){
        this.name=name;
        this.phoneNumber=phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
