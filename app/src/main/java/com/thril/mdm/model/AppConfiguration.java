package com.thril.mdm.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram on 2/3/2018.
 */

//Model class for app configuration
public class AppConfiguration {
    @SerializedName("devicekey")
    private String devicekey;
    @SerializedName("gps")
    private String gps;
    @SerializedName("mobiledata")
    private String mobiledata;
    @SerializedName("bluetooth")
    private String bluetooth;
    @SerializedName("flightmode")
    private String flightmode;
    @SerializedName("wifi")
    private String wifi;
    @SerializedName("usb")
    private String usb;
    @SerializedName("otg")
    private String otg;
    @SerializedName("gpsmode")
    private int gpsmode;
    @SerializedName("commandcenterphno")
    private String commandcenterphno;
    @SerializedName("synctimeinterval")
    private int synctimeinterval;
    @SerializedName("assignedmobilenumber")
    private String assignedmobilenumber;
    @SerializedName("isdualsimallowed")
    private String isdualsimallowed;
    @SerializedName("restrictdevicemnc")
    private String restrictdevicemnc;
    @SerializedName("screenorientation")
    private String screenorientation;

    @SerializedName("commandcenteremail")
    private String commandcenteremail;

    @SerializedName("gpstimeinterval")
    private long gpstimeinterval;

    @SerializedName("devicePhoneNumber")
    private String devicePhoneNumber;

    private PhotoResolution photoresolution;
    private VideoResolution videoresolution;
    private GeoFence geofence;

    public String getGps() {
        return gps;
    }

    public void setGps(String gps) {
        this.gps = gps;
    }

    public String getMobileData() {
        return mobiledata;
    }

    public void setMobileData(String mobileData) {
        this.mobiledata = mobileData;
    }

    public String getBluetooth() {
        return bluetooth;
    }

    public void setBluetooth(String bluetooth) {
        this.bluetooth = bluetooth;
    }

    public String getWifi() {
        return wifi;
    }

    public void setWifi(String wifi) {
        this.wifi = wifi;
    }

    public String getFlightMode() {
        return flightmode;
    }

    public void setFlightMode(String flightMode) {
        this.flightmode = flightMode;
    }

    public String getUsb() {
        return usb;
    }

    public void setUsb(String usb) {
        this.usb = usb;
    }

    public String getOtg() {
        return otg;
    }

    public void setOtg(String otg) {
        this.otg = otg;
    }

    public int getGpsMode() {
        return gpsmode;
    }

    public void setGpsMode(int gpsMode) {
        this.gpsmode = gpsMode;
    }

    public String getCommandCenterPhNo() {
        return commandcenterphno;
    }

    public void setCommandCenterPhNo(String commandCenterPhNo) {
        this.commandcenterphno = commandCenterPhNo;
    }

    public int getSyncTimeInterval() {
        return synctimeinterval;
    }

    public void setSyncTimeInterval(int syncTimeInterval) {
        this.synctimeinterval = syncTimeInterval;
    }

    public String getAssignedMobileNo() {
        return assignedmobilenumber;
    }

    public void setAssignedMobileNo(String assignedMobileNo) {
        this.assignedmobilenumber = assignedMobileNo;
    }

    public String getIsDualSimAllowed() {
        return isdualsimallowed;
    }

    public void setIsDualSimAllowed(String isDualSimAllowed) {
        this.isdualsimallowed = isDualSimAllowed;
    }

    public String getRestrictDeviceMNC() {
        return restrictdevicemnc;
    }

    public void setRestrictDeviceMNC(String restrictDeviceMNC) {
        this.restrictdevicemnc = restrictDeviceMNC;
    }

    public String getScreenOrientation() {
        return screenorientation;
    }

    public void setScreenOrientation(String screenOrientation) {
        this.screenorientation = screenOrientation;
    }

    public PhotoResolution getPhotoResolution() {
        return photoresolution;
    }

    public void setPhotoResolution(PhotoResolution photoResolution) {
        this.photoresolution = photoResolution;
    }

    public VideoResolution getVideoResolution() {
        return videoresolution;
    }

    public void setVideoResolution(VideoResolution videoResolution) {
        this.videoresolution = videoResolution;
    }

    public GeoFence getGeoFence() {
        return geofence;
    }

    public void setGeoFence(GeoFence geoFence) {
        this.geofence = geoFence;
    }

    public String getDevicekey() {
        return devicekey;
    }

    public void setDevicekey(String devicekey) {
        this.devicekey = devicekey;
    }

    public String getCommandcenteremail() {
        return commandcenteremail;
    }

    public void setCommandcenteremail(String commandcenteremail) {
        this.commandcenteremail = commandcenteremail;
    }

    public long getGpstimeinterval() {
        return gpstimeinterval;
    }

    public void setGpstimeinterval(int gpstimeinterval) {
        this.gpstimeinterval = gpstimeinterval;
    }

    public String getDevicePhoneNumber() {
        return devicePhoneNumber;
    }

    public void setDevicePhoneNumber(String devicePhoneNumber) {
        this.devicePhoneNumber = devicePhoneNumber;
    }
}
