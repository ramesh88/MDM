package com.thril.mdm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram on 4/14/2018.
 */

//Model class for dataconsumption
public class DataConsumption {

    @SerializedName("dataType")
    private String dataType;
    @SerializedName("dataSource")
    private String dataSource;
    @SerializedName("dataUpload")
    private String dataUpload;
    @SerializedName("dataDownload")
    private String dataDownload;
    @SerializedName("dateTime")
    private String dateTime;


    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDataSource() {
        return dataSource;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    public String getDataUpload() {
        return dataUpload;
    }

    public void setDataUpload(String dataUpload) {
        this.dataUpload = dataUpload;
    }

    public String getDataDownload() {
        return dataDownload;
    }

    public void setDataDownload(String dataDownload) {
        this.dataDownload = dataDownload;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public DataConsumption(String deviceId, String dataType, String dataSource, String dataUpload, String dataDownload, String dateTime){

        this.dataType=dataType;
        this.dataSource=dataSource;
        this.dataUpload=dataUpload;
        this.dataDownload=dataDownload;
        this.dateTime=dateTime;
    }
    public DataConsumption(){

    }
}
