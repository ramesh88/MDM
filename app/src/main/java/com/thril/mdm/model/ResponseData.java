package com.thril.mdm.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ram on 2/3/2018.
 */

//Model class for Response data
public class ResponseData {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("apkPath")
    private String apkPath;
    private List<App> app;
    private AppConfiguration appConfiguration;

    public List<App> getApp() {
        return app;
    }

    public void setApp(List<App> app) {
        this.app = app;
    }

    public AppConfiguration getAppConfiguration() {
        return appConfiguration;
    }

    public void setAppConfiguration(AppConfiguration appConfiguration) {
        this.appConfiguration = appConfiguration;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getApkPath() {
        return apkPath;
    }

    public void setApkPath(String ApkPath) {
        this.apkPath = ApkPath;
    }
}
