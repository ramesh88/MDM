package com.thril.mdm.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram on 5/29/2018.
 */

//Model class for staff data
public class StaffData {

    @SerializedName("staffKey")
    private String staffKey;
    @SerializedName("staffName")
    private String staffName;
    @SerializedName("otherInformation")
    private String otherInformation;
    @SerializedName("deviceKey")
    private String deviceKey;

    public String getStaffKey() {
        return staffKey;
    }

    public void setStaffKey(String staffKey) {
        this.staffKey = staffKey;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getOtherInformation() {
        return otherInformation;
    }

    public void setOtherInformation(String otherInformation) {
        this.otherInformation = otherInformation;
    }

    public String getDeviceKey() {
        return deviceKey;
    }

    public void setDeviceKey(String deviceKey) {
        this.deviceKey = deviceKey;
    }
}
