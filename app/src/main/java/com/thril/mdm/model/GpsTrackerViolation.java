package com.thril.mdm.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram on 4/14/2018.
 */

//Model class for gps tracker violation
public class GpsTrackerViolation {

    @SerializedName("latitude")
    private String latitude;
    @SerializedName("longitude")
    private String longitude;
    @SerializedName("dateTime")
    private String dateTime;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDeviatedEndDateTime() {
        return dateTime;
    }

    public void setDeviatedEndDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public GpsTrackerViolation(String latitude, String longitude, String dateTime){
        this.latitude=latitude;
        this.longitude=longitude;
        this.dateTime=dateTime;

    }
    public GpsTrackerViolation(){

    }
}
