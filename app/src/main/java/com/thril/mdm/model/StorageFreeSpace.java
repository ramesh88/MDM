package com.thril.mdm.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram on 5/26/2018.
 */

//Model class for storage free space
public class StorageFreeSpace {

    @SerializedName("dataInGbsPrimary")
    private String dataInGbsPrimary;
    @SerializedName("dataInGbsSecondary")
    private String dataInGbsSecondary;
    @SerializedName("dateTime")
    private String dateTime;

    public StorageFreeSpace(){

    }

    public StorageFreeSpace(String dataInGbsPrimary,String dataInGbsSecondary,String dataTime){
        this.dataInGbsPrimary=dataInGbsPrimary;
        this.dataInGbsSecondary=dataInGbsSecondary;
        this.dateTime=dataTime;
    }

    public String getDataInGbsPrimary() {
        return dataInGbsPrimary;
    }

    public void setDataInGbsPrimary(String dataInGbsPrimary) {
        this.dataInGbsPrimary = dataInGbsPrimary;
    }

    public String getDataInGbsSecondary() {
        return dataInGbsSecondary;
    }

    public void setDataInGbsSecondary(String dataInGbsSecondary) {
        this.dataInGbsSecondary = dataInGbsSecondary;
    }

    public String getCapturedDataTime() {
        return dateTime;
    }

    public void setCapturedDataTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
