package com.thril.mdm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram on 4/14/2018.
 */

//Model class for device usage
public class DeviceUsage {

    @SerializedName("switchOnDate")
    private String switchOnDateTime;
    @SerializedName("switchOffDateTime")
    private String switchOffDateTime;


    public String getSwitchOnDateTime() {
        return switchOnDateTime;
    }

    public void setSwitchOnDateTime(String switchOnDateTime) {
        this.switchOnDateTime = switchOnDateTime;
    }

    public String getSwitchOffDateTime() {
        return switchOffDateTime;
    }

    public void setSwitchOffDateTime(String switchOffDateTime) {
        this.switchOffDateTime = switchOffDateTime;
    }

    public DeviceUsage(String deviceId, String switchOnDateTime, String switchOffDateTime){

        this.switchOnDateTime=switchOnDateTime;
        this.switchOffDateTime=switchOffDateTime;

    }
    public DeviceUsage(){

    }
}
