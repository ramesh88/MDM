package com.thril.mdm.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram on 5/29/2018.
 */

//Model class for otp response
public class OTPResponse {

    @SerializedName("status")
    private int status;
    @SerializedName("data")
    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
