package com.thril.mdm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram on 4/14/2018.
 */

//Model class for app usage
public class AppUsage {

    @SerializedName("appID")
    private String appID;
    @SerializedName("appStartDateTime")
    private String appStartDateTime;
    @SerializedName("appStopDateTime")
    private String appStopDateTime;


    public String getAppID() {
        return appID;
    }

    public void setAppID(String appID) {
        this.appID = appID;
    }

    public String getAppStartDateTime() {
        return appStartDateTime;
    }

    public void setAppStartDateTime(String appStartDateTime) {
        this.appStartDateTime = appStartDateTime;
    }

    public String getAppStopDateTime() {
        return appStopDateTime;
    }

    public void setAppStopDateTime(String appStopDateTime) {
        this.appStopDateTime = appStopDateTime;
    }

    public AppUsage(String deviceId, String appID, String appStartDateTime, String appStopDateTime) {
        this.appID = appID;
        this.appStartDateTime = appStartDateTime;
        this.appStopDateTime = appStopDateTime;
    }

    public AppUsage() {

    }
}
