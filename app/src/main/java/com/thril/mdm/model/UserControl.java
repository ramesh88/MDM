package com.thril.mdm.model;

/**
 * Created by Ram on 6/28/2018.
 */

//model class for user control
public class UserControl {

    public String btUser="";
    public String wifiUser="";
    public String aeroUser="";
    public String locationUser="";
    public String mobileUser="";

    public String getBtUser() {
        return btUser;
    }

    public void setBtUser(String btUser) {
        this.btUser = btUser;
    }

    public String getWifiUser() {
        return wifiUser;
    }

    public void setWifiUser(String wifiUser) {
        this.wifiUser = wifiUser;
    }

    public String getAeroUser() {
        return aeroUser;
    }

    public void setAeroUser(String aeroUser) {
        this.aeroUser = aeroUser;
    }

    public String getLocationUser() {
        return locationUser;
    }

    public void setLocationUser(String locationUser) {
        this.locationUser = locationUser;
    }

    public String getMobileUser() {
        return mobileUser;
    }

    public void setMobileUser(String mobileUser) {
        this.mobileUser = mobileUser;
    }

    public UserControl(String btUser,String wifiUser,String locationUser,String mobileUser,String aeroUser){
        this.btUser=btUser;
        this.wifiUser=wifiUser;
        this.locationUser=locationUser;
        this.mobileUser=mobileUser;
        this.aeroUser=aeroUser;
    }

    public UserControl(){

    }
}
