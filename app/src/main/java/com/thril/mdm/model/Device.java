package com.thril.mdm.model;

import android.content.Context;

import com.google.gson.annotations.SerializedName;
import com.thril.mdm.Global.DeviceInfo;

/**
 * Created by Ram on 2/3/2018.
 */

//Model class for device
public class Device {
    @SerializedName("deviceKey")
    private String deviceKey;
    @SerializedName("deviceModel")
    private String deviceModel;
    @SerializedName("devicePlatform")
    private String devicePlatform;
    @SerializedName("deviceVersion")
    private String deviceVersion;
    @SerializedName("deviceManufacturer")
    private String deviceManufacturer;
    @SerializedName("deviceSerial")
    private String deviceSerial;
    @SerializedName("isDeviceVirtual")
    private String isDeviceVirtual;
    @SerializedName("isDeviceActive")
    private String isDeviceActive;
    @SerializedName("assestIdentifier")
    private String assetIdentifier;
    @SerializedName("fcm_token")
    private String fcm_token;
    @SerializedName("deviceImeiNumber")
    private String deviceImeiNumber;

    @SerializedName("MDMAppVersion")
    private String MDMAppVersion;

    public String getDeviceKey() {
        return deviceKey;
    }

    public void setDeviceKey(String deviceKey) {
        this.deviceKey = deviceKey;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getDevicePlatform() {
        return devicePlatform;
    }

    public void setDevicePlatform(String devicePlatform) {
        this.devicePlatform = devicePlatform;
    }

    public String getDeviceVersion() {
        return deviceVersion;
    }

    public void setDeviceVersion(String deviceVersion) {
        this.deviceVersion = deviceVersion;
    }

    public String getDeviceManufacturer() {
        return deviceManufacturer;
    }

    public void setDeviceManufacturer(String deviceManufacturer) {
        this.deviceManufacturer = deviceManufacturer;
    }

    public String getDeviceSerial() {
        return deviceSerial;
    }

    public void setDeviceSerial(String deviceSerial) {
        this.deviceSerial = deviceSerial;
    }

    public String getIsDeviceVirtual() {
        return isDeviceVirtual;
    }

    public void setIsDeviceVirtual(String isDeviceVirtual) {
        this.isDeviceVirtual = isDeviceVirtual;
    }

    public String getIsDeviceActive() {
        return isDeviceActive;
    }

    public void setIsDeviceActive(String isDeviceActive) {
        this.isDeviceActive = isDeviceActive;
    }

    public String getAssetIdentifier() {
        return assetIdentifier;
    }

    public void setAssetIdentifier(String assetIdentifier) {
        this.assetIdentifier = assetIdentifier;
    }

    public String getFcm_token() {
        return fcm_token;
    }

    public void setFcm_token(String fcm_token) {
        this.fcm_token = fcm_token;
    }

    public Device(){

    }
    public Device(DeviceInfo deviceInfo, Context context,String fcmToken,String deviceImeiNumber,String mdmappVersion){
        setDeviceKey(deviceInfo.getUUID(context));
        setDeviceModel(deviceInfo.getModel());
        setDevicePlatform(deviceInfo.getPlatformName());
        setDeviceVersion(deviceInfo.getVersion());
        setDeviceManufacturer(deviceInfo.getManufacturer());
        setDeviceSerial(deviceInfo.getSerial());
        setIsDeviceVirtual(deviceInfo.getIsVirtual());
        setIsDeviceActive(deviceInfo.getIsActive());
        setAssetIdentifier(deviceInfo.getAssetIdentifier());
        if(fcmToken!=null && !fcmToken.equals("")){
            setFcm_token(fcmToken);
        }
        if(deviceImeiNumber!=null && !deviceImeiNumber.equals("")){
            setDeviceImeiNumber(deviceImeiNumber);
        }
        if(mdmappVersion!=null && !mdmappVersion.equals("")){
            setMDMAppVersion(mdmappVersion);
        }
    }

    public String getDeviceImeiNumber() {
        return deviceImeiNumber;
    }

    public void setDeviceImeiNumber(String deviceImeiNumber) {
        this.deviceImeiNumber = deviceImeiNumber;
    }

    public String getMDMAppVersion() {
        return MDMAppVersion;
    }

    public void setMDMAppVersion(String MDMAppVersion) {
        this.MDMAppVersion = MDMAppVersion;
    }
}
