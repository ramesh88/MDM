package com.thril.mdm.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram on 5/29/2018.
 */

//Model class for staff info
public class StaffInfo {

    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("apkPath")
    private String apkPath;
    private StaffData staffData;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public StaffData getStaffData() {
        return staffData;
    }

    public void setStaffData(StaffData staffData) {
        this.staffData = staffData;
    }

    public String getApkPath() {
        return apkPath;
    }

    public void setApkPath(String apkPath) {
        this.apkPath = apkPath;
    }
}
