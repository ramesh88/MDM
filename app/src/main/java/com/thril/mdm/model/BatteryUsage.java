package com.thril.mdm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram on 4/14/2018.
 */

//Model class for battery usage
public class BatteryUsage {

    @SerializedName("batteryStatus")
    private String batteryStatus;
    @SerializedName("batteryPercentage")
    private String batteryPercentage;
    @SerializedName("dateTime")
    private String dateTime;


    public String getBatteryStatus() {
        return batteryStatus;
    }

    public void setBatteryStatus(String batteryStatus) {
        this.batteryStatus = batteryStatus;
    }

    public String getBatteryPercentage() {
        return batteryPercentage;
    }

    public void setBatteryPercentage(String batteryPercentage) {
        this.batteryPercentage = batteryPercentage;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public BatteryUsage(String deviceId, String batteryStatus, String batteryPercentage, String dateTime){
        this.batteryStatus=batteryStatus;
        this.batteryPercentage=batteryPercentage;
        this.dateTime=dateTime;
    }

    public BatteryUsage(){

    }
}
