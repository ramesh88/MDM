package com.thril.mdm.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram on 2/3/2018.
 */

//Model class for app list
public class App {
    @SerializedName("title")
    private String title;
    @SerializedName("appid")
    private String appid;
    @SerializedName("versionNo")
    private String versionNo;
    @SerializedName("type")
    private String type;
    @SerializedName("packageName")
    private String packageName;
    @SerializedName("iconpath")
    private String iconpath;

    @SerializedName("apkpath")
    private String apkpath;
    @SerializedName("activeFrom")
    private long activeFrom;
    @SerializedName("activeTo")
    private long activeTo;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(String versionNo) {
        this.versionNo = versionNo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getApkPath() {
        return apkpath;
    }

    public void setApkPath(String apkPath) {
        this.apkpath = apkPath;
    }

    public long getActiveFrom() {
        return activeFrom;
    }

    public void setActiveFrom(long activeFrom) {
        this.activeFrom = activeFrom;
    }

    public long getActiveTo() {
        return activeTo;
    }

    public void setActiveTo(long activeTo) {
        this.activeTo = activeTo;
    }

    public String getIconPath() {
        return iconpath;
    }

    public void setIconPath(String iconPath) {
        this.iconpath = iconPath;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }
}
