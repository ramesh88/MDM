package com.thril.mdm.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram on 6/9/2018.
 */

//Model class for Notification data
public class NotificationData {

    @SerializedName("id")
    private String id;
    @SerializedName("title")
    private String title;
    @SerializedName("content")
    private String content;
    @SerializedName("notificationDate")
    private String notificationDate;
    @SerializedName("notificationTime")
    private String notificationTime;

    public NotificationData(){

    }

    public NotificationData(String id,String title,String content,String notificationDate,String notificationTime){
        this.id=id;
        this.title=title;
        this.content=content;
        this.notificationDate=notificationDate;
        this.notificationTime=notificationTime;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }

    public String getNotificationTime() {
        return notificationTime;
    }

    public void setNotificationTime(String notificationTime) {
        this.notificationTime = notificationTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
