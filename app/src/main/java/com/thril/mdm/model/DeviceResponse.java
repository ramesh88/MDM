package com.thril.mdm.model;

import com.google.gson.annotations.SerializedName;
import com.thril.mdm.databaselayer.contractor.Contractor;

import java.util.List;

/**
 * Created by Ram on 5/25/2018.
 */

//Model class for device response
public class DeviceResponse {

    @SerializedName("deviceKey")
    private String deviceKey;
    @SerializedName("fcm_token")
    private String fcm_token;
    @SerializedName("MDMAppVersion")
    private String MDMAppVersion;
    private List<BatteryUsage> batteryUsage;
    private List<SignalStrength> signalStrength;
    private List<DataConsumption> dataConsumption;
    private List<DeviceUsage> deviceUsage;
    private List<AppUsage> appUsage;
    private List<GpsAssetTracker> gpsAssetTracker;
    private List<GpsTrackerViolation> gpsTrackerViolation;
    private List<StorageFreeSpace> storageFreeSpace;

    public String getFcm_token() {
        return fcm_token;
    }

    public void setFcm_token(String fcm_token) {
        this.fcm_token = fcm_token;
    }

    public String getDeviceKey() {
        return deviceKey;
    }

    public void setDeviceKey(String deviceKey) {
        this.deviceKey = deviceKey;
    }

    public List<BatteryUsage> getBatteryUsage() {
        return batteryUsage;
    }

    public void setBatteryUsage(List<BatteryUsage> batteryUsage) {
        this.batteryUsage = batteryUsage;
    }

    public List<SignalStrength> getSignalStrength() {
        return signalStrength;
    }

    public void setSignalStrength(List<SignalStrength> signalStrength) {
        this.signalStrength = signalStrength;
    }

    public List<DataConsumption> getDataConsumption() {
        return dataConsumption;
    }

    public void setDataConsumption(List<DataConsumption> dataConsumption) {
        this.dataConsumption = dataConsumption;
    }

    public List<DeviceUsage> getDeviceUsage() {
        return deviceUsage;
    }

    public void setDeviceUsage(List<DeviceUsage> deviceUsage) {
        this.deviceUsage = deviceUsage;
    }

    public List<AppUsage> getAppUsage() {
        return appUsage;
    }

    public void setAppUsage(List<AppUsage> appUsage) {
        this.appUsage = appUsage;
    }

    public List<GpsAssetTracker> getGpsAssetTracker() {
        return gpsAssetTracker;
    }

    public void setGpsAssetTracker(List<GpsAssetTracker> gpsAssetTracker) {
        this.gpsAssetTracker = gpsAssetTracker;
    }

    public List<GpsTrackerViolation> getGpsTrackerViolation() {
        return gpsTrackerViolation;
    }

    public void setGpsTrackerViolation(List<GpsTrackerViolation> gpsTrackerViolation) {
        this.gpsTrackerViolation = gpsTrackerViolation;
    }

    public List<StorageFreeSpace> getStorageFreeSpace() {
        return storageFreeSpace;
    }

    public void setStorageFreeSpace(List<StorageFreeSpace> storageFreeSpace) {
        this.storageFreeSpace = storageFreeSpace;
    }

    public String getMDMAppVersion() {
        return MDMAppVersion;
    }

    public void setMDMAppVersion(String MDMAppVersion) {
        this.MDMAppVersion = MDMAppVersion;
    }
}
