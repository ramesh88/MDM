package com.thril.mdm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram on 4/14/2018.
 */

//Model class for Signal Strength
public class SignalStrength {

    @SerializedName("gmsProvider")
    private String gmsProvider;
    @SerializedName("signalStrength")
    private String signalStrength;
    @SerializedName("dateTime")
    private String dateTime;

    public String getGmsProvider() {
        return gmsProvider;
    }

    public void setGmsProvider(String gmsProvider) {
        this.gmsProvider = gmsProvider;
    }

    public String getSignalStrength() {
        return signalStrength;
    }

    public void setSignalStrength(String signalStrength) {
        this.signalStrength = signalStrength;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }



    public SignalStrength(String deviceId,String gmsProvider,String signalStrength,String dateTime){

        this.gmsProvider=gmsProvider;
        this.signalStrength=signalStrength;
        this.dateTime=dateTime;
    }

    public SignalStrength(){

    }
}
