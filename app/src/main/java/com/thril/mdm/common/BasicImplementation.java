package com.thril.mdm.common;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.thril.mdm.service.ApiPendingService;
import com.thril.mdm.service.DeviceUsage;
import com.thril.mdm.service.StopSettingsService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Ram on 8/11/2018.
 */

//General class to call device usage , server calling services
public class BasicImplementation {

    public static final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_FULLSCREEN
            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

    public static final List blockedKeys = new ArrayList(Arrays.asList(KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_VOLUME_UP, KeyEvent.KEYCODE_POWER));



//method to check play services are avialable or not
    public static boolean isGooglePlayServicesAvailable(Activity context) {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, context, 0).show();
            return false;
        }
    }

//method to generate a alarm to call device usage report
    public static void startDeviceUsagePendingIntent(int minutes,Activity activity) {
        Calendar cal = Calendar.getInstance();
        Intent intent = new Intent(activity, DeviceUsage.class);
        PendingIntent pintent = PendingIntent.getService(activity, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager deviceUsageAlarm = (AlarmManager)activity.getSystemService(Context.ALARM_SERVICE);
        long timeToWakeUP = (minutes * (60 * 1000));
        long timeInterval = (cal.getTimeInMillis() + timeToWakeUP);
        deviceUsageAlarm.setRepeating(AlarmManager.RTC_WAKEUP, timeInterval, timeToWakeUP, pintent);
    }
//method to genearte a alarm to call server service
    public static void startServerPendingIntent(int minutes,Activity activity) {
        Calendar cal = Calendar.getInstance();
        Intent intent = new Intent(activity, ApiPendingService.class);
        PendingIntent pintent = PendingIntent.getService(activity, 2, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        long timeToWakeUP = (minutes * (60 * 1000));
        long timeInterval = (cal.getTimeInMillis() + timeToWakeUP);
        AlarmManager serviceCallAlarm = (AlarmManager)activity.getSystemService(Context.ALARM_SERVICE);
        serviceCallAlarm.setRepeating(AlarmManager.RTC_WAKEUP, timeInterval, timeToWakeUP, pintent);
    }

//method to cancel device usage alarm
    public  static void cancelDeviceUsageAlarm(Activity activity) {
        Intent intent = new Intent(activity, DeviceUsage.class);

        if (PendingIntent.getService(activity, 1,
                intent, PendingIntent.FLAG_NO_CREATE) != null) {
            PendingIntent pintent = PendingIntent.getService(activity, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager deviceUsageAlarm = (AlarmManager)activity.getSystemService(Context.ALARM_SERVICE);
            if (deviceUsageAlarm != null) {
                deviceUsageAlarm.cancel(pintent);
            }
        }
    }

    //method to cancel service usage alarm
    public static void cancelServiceCallAlarm(Activity context) {
        Intent intent = new Intent(context, ApiPendingService.class);
        if (PendingIntent.getService(context, 2,
                intent, PendingIntent.FLAG_NO_CREATE) != null) {
            PendingIntent pintent = PendingIntent.getService(context, 2, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager serviceCallAlarm = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
            if (serviceCallAlarm != null) {
                serviceCallAlarm.cancel(pintent);
            }
        }
    }

    //method to cancel stopsetting service
    public static void cancelStopSettingService(Activity activity) {
        activity.stopService(new Intent(activity, StopSettingsService.class));
    }



}
