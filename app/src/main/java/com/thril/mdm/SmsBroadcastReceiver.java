package com.thril.mdm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsMessage;

import com.thril.mdm.Global.MDMSettings;
import com.thril.mdm.Network.utils.Constants;
import com.thril.mdm.kiosk.MainActivity;

/**
 * Created by Ram on 6/12/2018.
 */

//Receiver class to know the event of SMS
public class SmsBroadcastReceiver extends BroadcastReceiver {

    public static final String SMS_BUNDLE = "pdus";

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void onReceive(Context context, Intent intent) {
        Bundle intentExtras = intent.getExtras();
        if (intentExtras != null) {
            Object[] sms = (Object[]) intentExtras.get(SMS_BUNDLE);
            String smsMessageStr = "";
            String smsBody = "";
            String address = "";
            for (int i = 0; i < sms.length; ++i) {
                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) sms[i]);

                smsBody = smsMessage.getMessageBody().toString();
                address = smsMessage.getOriginatingAddress();
                smsMessageStr += smsBody + "\n";
            }
           // Toast.makeText(context, smsMessageStr, Toast.LENGTH_SHORT).show();

            //this will update the UI with message
            MainActivity inst = MainActivity.instance();
            if (smsBody.contains("MDM")) {
                String split[] = smsBody.split("\\s+");
                int smsEvent = 0;
                if (split[1].trim().equals(MDMSettings.smsArray[0])) {
                    smsEvent = 1;
                } else if (split[1].trim().equals(MDMSettings.smsArray[1])) {
                    smsEvent = 2;
                } else if (split[1].trim().equals(MDMSettings.smsArray[2])) {
                    smsEvent = 3;
                } else if (split[1].trim().equals(MDMSettings.smsArray[3])) {
                    smsEvent = 4;
                } else if (split[1].trim().equals(MDMSettings.smsArray[4])) {
                    smsEvent = 5;
                }
                sendBroadCast(smsEvent);

            }
        }
    }

    public void sendBroadCast(int status) {
        Intent intent = new Intent(Constants.SMS_UPDATES);
        intent.putExtra("status", status);
        LocalBroadcastManager.getInstance(Constants.getmContext()).sendBroadcast(intent);
    }

}