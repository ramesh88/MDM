package com.thril.mdm.business;

import android.location.Location;

/**
 * Created by Ram on 1/13/2018.
 */

//interface for the gps business class
public interface GpsBusinessListener {

    void gpsLocation(Location location);
    void currentGpsLocation(Location location);
    void outOfRange(Location location);
}
