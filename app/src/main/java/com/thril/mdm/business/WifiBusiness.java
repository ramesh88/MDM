package com.thril.mdm.business;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.thril.mdm.Global.MDMSettings;
import com.thril.mdm.R;
import com.thril.mdm.Presenter.WifiPresenterListener;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * Created by Ram on 1/20/2018.
 */

//Wifi business class to make all business logic for wifi related
public class WifiBusiness {
    private static final String TAG = "WifiBusiness";
    private static WifiBusiness instance;
    static Context mContext;
    private static final String HOTSPOT_PREFIX = "MDM_";
    private static final String HOTSPOT_OREO_PREFIX = "AndroidShare";
    private static IntentFilter wifi_scan_filter = new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
    private static WifiManager wifiManager = null;
    private static List<ScanResult> wifiList;
    private static List<String> passableHotsPot;
    private static List<ScanResult> scanResultList;
    private static String hot_spot_name = "";
    private static String hot_spot_password = "";

    private static WifiReceiver wifiReceiver = null;
    private static WifiPresenterListener mWifiPresenterListener;
    private static WifiManager.LocalOnlyHotspotReservation localOnlyHotspotReservation;
    ServerSocket serverSocket;
    public static Socket client = null;
    private static final int PORT = 8080;
    private static final String IP_ADDRESS = "192.168.43.1";
    private InputStream inputStream;
    private OutputStream outputStream;
    private static int mNetworkId = -1;
    private static boolean serverFlag = true;
    private static boolean clientFlag = true;
    private static boolean wifiFlag = false;
    static final String SECURITY_NONE = "OPEN";
    static final String SECURITY_WEP = "WEP";
    static final String SECURITY_PSK = "WPA";
    static final String SECURITY_EAP = "EAP";


    private static final String STORAGE_PATH = Environment.getExternalStorageDirectory() + File.separator + ".MDM/";

    //Constructor class
    private WifiBusiness() {

    }

    //Method returns the instance and creates a instance for the class
    public static WifiBusiness getInstance(Context context) {
        if (instance == null) {
            instance = new WifiBusiness();

        }
        mContext = context;
        if (wifiManager == null) {
            wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        }
        return instance;
    }

    //method to set the listener for the class with parameters wifi present listner
    public void setWifiPresenterListener(WifiPresenterListener wifiPresenterListener) {
        mWifiPresenterListener = wifiPresenterListener;
    }


    //method to scan  all enabled wifi's
    public void scanHotSpots(boolean flag) {
        wifiFlag = flag;

        if (MDMSettings.isMobileDataEnable(mContext)) {
            MDMSettings.toggleMobileDataConnection(false, mContext);
        }

        if (!wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(true);
        }

        try {
            if (wifiReceiver != null) {
                mContext.unregisterReceiver(wifiReceiver);
                wifiReceiver = null;
            }
        } catch (Exception e) {

        }
        wifiReceiver = new WifiReceiver();
        mContext.registerReceiver(wifiReceiver, wifi_scan_filter);
        wifiManager.startScan();
        Log.d("WifiBusiness", "Started Scanning");

    }

    //method to enable hotspot
    public void enableHotspot() {

        if (wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(false);

        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            turnOnHotspot();
            listenServerSocket();
        } else {
            if (setWifiApEnabled(true)) {
                Log.d(TAG, "Enabled");
                listenServerSocket();
                mWifiPresenterListener.hotSpotName(hot_spot_name, hot_spot_password);
            } else {
                mWifiPresenterListener.hotSpotName("", "");
            }
        }
    }


    //wifi reciever class to list all enabled wifi class using broadcast reciever
    private final class WifiReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            wifiList = wifiManager.getScanResults();
            Log.d("WifiBusiness", "wifiReceiver");
            if (wifiList == null || wifiList.size() == 0)
                return;
            onReceiveNewNetworks(wifiList);
        }
    }

    //method to add new network wifi to the list with parameters of list of scanresult
    public void onReceiveNewNetworks(List<ScanResult> wifiList) {

        passableHotsPot = new ArrayList<>();
        scanResultList = new ArrayList<>();
        scanResultList.addAll(wifiList);
        for (ScanResult result : wifiList) {
            System.out.println(result.SSID);
            if ((result.SSID).startsWith(HOTSPOT_PREFIX) || result.SSID.startsWith(HOTSPOT_OREO_PREFIX)) {
                Log.d(TAG, result.SSID);
                passableHotsPot.add(result.SSID);
            } else {
                if (wifiFlag) {
                    passableHotsPot.add(result.SSID);
                }
            }
        }
        if (!passableHotsPot.isEmpty()) {
            Log.d(TAG, "not empty");
            mWifiPresenterListener.discoveryList(passableHotsPot, wifiFlag, scanResultList);
        }
        try {
            if (wifiReceiver != null) {
                mContext.unregisterReceiver(wifiReceiver);
                wifiReceiver = null;
            }
        } catch (Exception e) {

        }

    }

    //method to retrun random ss id
    public String generateRandomSSID() {
        String randomSSID = "";
        Random rand = new Random();
        int rand_int = rand.nextInt(1000);
        randomSSID = HOTSPOT_PREFIX + rand_int;
        return randomSSID;


    }

    //method to know wifi is enabled or not
    public static boolean isApOn(Context context) {
        WifiManager wifimanager = (WifiManager) context.getSystemService(context.WIFI_SERVICE);
        try {
            Method method = wifimanager.getClass().getDeclaredMethod("isWifiApEnabled");
            method.setAccessible(true);
            return (Boolean) method.invoke(wifimanager);
        } catch (Throwable ignored) {
        }
        return false;
    }

    //method to disbale hotspot with parameters context
    public void disableHotspot(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (localOnlyHotspotReservation != null) {
                localOnlyHotspotReservation.close();
                mWifiPresenterListener.disableHotspot(true);
                try {
                    if (wifiReceiver != null) {
                        context.unregisterReceiver(wifiReceiver);
                        wifiReceiver = null;
                    }
                } catch (Exception e) {

                }
            }
        } else {
            WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
            WifiConfiguration apConfig = null;
            Method method = null;
            try {
                method = wifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, Boolean.TYPE);
                if (isApOn(context)) {
                    method.invoke(wifiManager, apConfig, false);
                }
                mWifiPresenterListener.disableHotspot(true);
                try {
                    if (wifiReceiver != null) {
                        context.unregisterReceiver(wifiReceiver);
                        wifiReceiver = null;
                    }
                }catch (Exception e){

                }
            } catch (NoSuchMethodException e) {
                mWifiPresenterListener.disableHotspot(false);
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                mWifiPresenterListener.disableHotspot(false);
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                mWifiPresenterListener.disableHotspot(false);
                e.printStackTrace();
            }
        }
        closeSocket();
    }

    //method to generate random password
    public static String generateRandomPassword() {

        String randomUUID = UUID.randomUUID().toString();
        randomUUID = randomUUID.substring(0, 8) + randomUUID.substring(9, 13);
        return randomUUID;

    }

    //method to set wifi enabled with parameters flag
    public boolean setWifiApEnabled(boolean enabled) {
        WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);

        try {

            hot_spot_name = generateRandomSSID();
            hot_spot_password = generateRandomPassword();
            // Hot configuration class
            WifiConfiguration apConfig = new WifiConfiguration();
            // Configuration hot name ( You can add a random number behind the name )
            apConfig.SSID = hot_spot_name;
            // Configuration hot password
            apConfig.preSharedKey = hot_spot_password;
            apConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            // Set hotspot by reflection call
            Method method = wifiManager.getClass().getMethod(
                    "setWifiApEnabled", WifiConfiguration.class, Boolean.TYPE);
            // Return to hot open state
            return (Boolean) method.invoke(wifiManager, apConfig, enabled);
        } catch (Exception e) {
            return false;
        }
    }


    //method to connect to hotspot with parameters selected_hotspot name and password
    public void connectToHotpot(String selected_Hotspot, String password) {
        if (selected_Hotspot == null)
            return;
        WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);

        WifiConfiguration wifiConfig = this.setWifiParams(selected_Hotspot, password);
        mNetworkId = wifiManager.addNetwork(wifiConfig);
        boolean flag = wifiManager.enableNetwork(mNetworkId, true);
        if (flag) {
            Log.d(TAG, "before connection");

            connectClientSocket();
        }
        mWifiPresenterListener.hotSpotConnectStatus(flag);
        Log.d(TAG, " " + flag);
    }

    //method to remove connetced network
    public void removeNetwork() {
        if (mNetworkId > -1) {
            WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
            wifiManager.removeNetwork(mNetworkId);
        }
        closeSocket();
    }

    // Set the parameters of the hotspot to connect with parameters ssid and password
    public WifiConfiguration setWifiParams(String ssid, String password) {
        WifiConfiguration apConfig = new WifiConfiguration();
        apConfig.SSID = "\"" + ssid + "\"";
        apConfig.preSharedKey = "\"" + password + "\"";
        apConfig.hiddenSSID = true;
        apConfig.status = WifiConfiguration.Status.ENABLED;
        apConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        apConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        if (ssid.startsWith(HOTSPOT_OREO_PREFIX)) {
            Log.d("EifiParams", "if");
            apConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        } else {
            Log.d("EifiParams", "else");
            apConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
        }
        apConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        apConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        apConfig.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        return apConfig;
    }

    @RequiresApi(api = Build.VERSION_CODES.O) //method to on the hotspot
    private void turnOnHotspot() {
        WifiManager manager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);

        manager.startLocalOnlyHotspot(new WifiManager.LocalOnlyHotspotCallback() {

            @Override
            public void onStarted(WifiManager.LocalOnlyHotspotReservation reservation) {
                super.onStarted(reservation);
                localOnlyHotspotReservation = reservation;

                WifiConfiguration wifiConfiguration = reservation.getWifiConfiguration();
                hot_spot_name = wifiConfiguration.SSID;
                hot_spot_password = wifiConfiguration.preSharedKey;
                Log.d("SSID", hot_spot_name);
                Log.d("PhraseKey", hot_spot_password);
                Log.d("Random password:", generateRandomPassword());
//                Log.d("Friendly Name",wifiConfiguration.providerFriendlyName);
                Log.d("nnStarted", "Wifi Hotspot is on now");
                mWifiPresenterListener.hotSpotName(hot_spot_name, hot_spot_password);


            }

            @Override
            public void onStopped() {
                super.onStopped();
                Log.d("OnStopped", "onStopped: ");
            }

            @Override
            public void onFailed(int reason) {
                super.onFailed(reason);
                Log.d("OnFailed", "onFailed: ");
            }
        }, new Handler());
    }

    //method to show password dialog wth parameters hot spot name
    public void showPasswordDialog(final String hot_spot_name) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        LayoutInflater inflater = (LayoutInflater)
                mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.password_layout, null);
        dialogBuilder.setTitle("Hot spot:" + hot_spot_name);
        dialogBuilder.setView(dialogView);

        final EditText editText = (EditText) dialogView.findViewById(R.id.et_password);
        dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (editText.getText().toString().trim().length() > 0) {
                    connectToHotpot(hot_spot_name, editText.getText().toString().trim());
                }


            }
        });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }


    //method to transfer file
    public void sendFile(final String fileName) {

        new Thread() {
            @Override
            public void run() {
                super.run();
                int totalByteToRead = 16 * 1024;// 64 kb
                DataOutputStream dos = null;
                File f = new File(fileName);
                FileInputStream fis = null;
                Log.d(TAG, fileName + "  | " + f.length());
                String sendFileName = "";
                try {
                    sendFileName = fileName.substring(fileName.lastIndexOf('/') + 1).trim();
                    sendFileName = "FileInfo|" + sendFileName + "|" + f.length() + "|";

                    Log.d(TAG, "Sending File  " + sendFileName);
                    fis = new FileInputStream(f);
                    if (outputStream == null) {
                        outputStream = client.getOutputStream();
                    }
                    outputStream.write(sendFileName.getBytes());
                    outputStream.flush();
                    long numSent = 0;
                    long fileLength = f.length();
                    int numToSend = (int) fileLength;
                    while (numSent < numToSend) {
                        System.gc();
                        long numThisTime = numToSend - numSent;
                        numThisTime = numThisTime < totalByteToRead ? numThisTime
                                : totalByteToRead;
                        byte[] buffer = new byte[(int) numThisTime];
                        int numRead = fis.read(buffer);
                        /*if (numRead == -1)
                            break;*/
                        outputStream.write(buffer);
                        outputStream.flush();
                        numSent += numRead;
                        calculateProgressPercentage(fileLength, numSent);
                        System.gc();
                        Log.d(TAG, "Sending count " + numSent);
                        //updateFields(tv_sendbytes, "Sending bytes:" + numSent);
                        // updateConversationHandler .post(new updateUIThread("Sending bytes:"+ numSent,tv_sendbytes));
                    }
                    if (numSent == fileLength) {
                        mWifiPresenterListener.fileStatus(true);
                    }


                } catch (FileNotFoundException e) {
                    Log.d(TAG, "File not found exception");
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.d(TAG, "IOException");
                    e.printStackTrace();
                }
            }
        }.start();

    }

    //Thread class to create a communication between devices
    public class ServerThread implements Runnable {

        public void run() {
            try {
                serverSocket = new ServerSocket(PORT);
                Log.d(TAG, "Inside server thread");
                mWifiPresenterListener.socketStatus("Listening...", false);
                while (serverFlag) {
                    // LISTEN FOR INCOMING CLIENTS
                    Log.d(TAG, "Listening");


                    client = serverSocket.accept();
                    Log.d(TAG, "connected");
                    if (client.isConnected()) {
                        serverFlag = false;
                        Log.d(TAG, "Server socket connected");
                        mWifiPresenterListener.socketStatus("Connected", true);
                        CommunicationThread commThread = new CommunicationThread(
                                client);
                        new Thread(commThread).start();
                    }


                }

            } catch (Exception e) {
                closeSocket();
                e.printStackTrace();
            }
        }
    }

    //Thread class to create a communication between devices
    class CommunicationThread implements Runnable {

        private Socket clientSocket;
        String fileName = "";
        long fileLength = 0;
        long count = 0;
        DataInputStream dis;
        FileOutputStream out;

        public CommunicationThread(Socket clientSocket) {

            Log.d(TAG, "Inside communication thread");
            this.clientSocket = clientSocket;
            try {
                inputStream = this.clientSocket.getInputStream();
                outputStream = this.clientSocket.getOutputStream();
                dis = new DataInputStream(inputStream);


            } catch (final IOException e) {


            }
        }

        public void run() {

            try {

                while (true) {
                    count = 0;
                    boolean secondLoop = true;
                    byte[] buffer = new byte[16 * 1024]; // 8 kb
                    int read;
                    while (secondLoop) {
                        read = dis.read(buffer);
                        if (read > 0) {
                            String data = new String(buffer, 0, read);
                            if (data.startsWith("FileInfo")) {

                                String[] splitStrings = data.split("\\|");
                                fileName = splitStrings[1];
                                fileLength = Integer.parseInt(splitStrings[2]);
                                Log.d(TAG, fileName + " " + fileLength);
                                String dirFile = STORAGE_PATH;
                                File fileDir = new File(dirFile);
                                if (!fileDir.exists()) {
                                    fileDir.mkdir();
                                }
                                fileName = dirFile + fileName;
                                File f = new File(fileName);
                                if (f.exists()) {
                                    f.delete();
                                }
                                f.createNewFile();
                                out = new FileOutputStream(f);

                            } else {
                                count += read;
                                out.write(buffer, 0, read);
                                out.flush();
                                Log.d(TAG, "Received byte " + count);
                                calculateProgressPercentage(fileLength, count);
                                if (count == fileLength) {
                                    // buffer = new byte[8192];
                                    count = 0;
                                    mWifiPresenterListener.fileStatus(true);
                                    Log.d(TAG, "File written and closed");
                                    out.close();
                                    secondLoop = false;
                                }
                            }
                        }
                    }
                }


            } catch (final IOException e) {
                // TODO Auto-generated catch block
                Log.e(TAG, " inside comm thread " + e.getLocalizedMessage());
                closeSocket();

            }

        }
    }


    //method to create a client socket connection
    public void connectClientSocket() {
        if (client == null) {
            Log.d(TAG, "Inside connectClietnSocket");
            clientFlag = true;
            ClientThread clientThread = new ClientThread();
            new Thread(clientThread).start();
        } else {
            Log.d(TAG, "Socket already connected");
        }
    }

    //method to create a listen to server socket connection
    public void listenServerSocket() {
        if (client == null) {
            serverFlag = true;
            Log.d(TAG, "Listening server socket");
            Thread serverThread = new Thread(new ServerThread());
            serverThread.start();
        } else {
            Log.d(TAG, "Socket is listening");
        }
    }


    //class for crearting a client thread
    public class ClientThread implements Runnable {
        @Override
        public void run() {
            mWifiPresenterListener.socketStatus("Listening...", false);
            while (clientFlag) {
                try {
                    Log.d(TAG, "Trying to connect");

                    client = new Socket(IP_ADDRESS, PORT);
                    if (client.isConnected()) {
                        Log.d(TAG, "client connected");
                        clientFlag = false;
                        mWifiPresenterListener.socketStatus("Connected", true);

                        CommunicationThread communicationThread = new CommunicationThread(client);
                        new Thread(communicationThread).start();
                    }
                } catch (IOException e) {
                    //  closeSocket();
                    e.printStackTrace();
                }
            }
        }
    }

    //method to close the connect socket
    public void closeSocket() {
        try {
            if (outputStream != null) {
                outputStream.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
            if (client != null) {
                client.close();

                Log.d(TAG, "Socket got closed");
            }
            if (serverSocket != null) {
                serverSocket.close();

                Log.d(TAG, "Socket got closed");
            }
            client = null;
            serverSocket = null;
            serverFlag = false;
            clientFlag = false;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //method to list all the files from MDM folder with parameters file format name
    public List<String> getFilesInfo(String fileFormat) {
        List<String> files = new ArrayList<>();
        String dir = Environment.getExternalStorageDirectory() + File.separator + "MDM/";
        File folder = new File(dir);
        if (folder.exists()) {
            File[] listOfFiles = folder.listFiles();

            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    Log.d("File list", listOfFiles[i].getAbsolutePath());
                    if (!fileFormat.equals("")) {
                        if (listOfFiles[i].getAbsolutePath().endsWith(fileFormat)) {
                            files.add(listOfFiles[i].getAbsolutePath());
                        }
                    } else {
                        if (!listOfFiles[i].getAbsolutePath().endsWith(".apk")) {
                            files.add(listOfFiles[i].getAbsolutePath());
                        }
                    }

                    /*else {
                        files.add(listOfFiles[i].getAbsolutePath());
                    }*/

                } else if (listOfFiles[i].isDirectory()) {
                    System.out.println("Directory " + listOfFiles[i].getName());
                }
            }
        }
        return files;
    }

    //method to calculate progees perenctage
    public void calculateProgressPercentage(long total, long uploaded) {
        mWifiPresenterListener.updateProgress((int) (100 * uploaded / total));
    }


    //method to get the connect network name
    public String getConnectedNetwork() {
        WifiManager wifiManager = (WifiManager) mContext.getSystemService(mContext.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        String SSID = removeDoubleQuotes(wifiInfo.getSSID());
        return SSID;
    }

    //method to remove double quotes in a string
    public static String removeDoubleQuotes(String string) {
        if (string == null)
            return null;
        final int length = string.length();
        if ((length > 1) && (string.charAt(0) == '"')
                && (string.charAt(length - 1) == '"')) {
            return string.substring(1, length - 1);
        }
        return string;
    }

    //method to check wifi connected or not with parameters ssid
    public boolean checkWifiConnected(String ssid) {
        WifiManager wifiManager = (WifiManager) mContext.getSystemService(mContext.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        String sssid = wifiInfo.getSSID();
        if (sssid.equalsIgnoreCase("\"" + ssid + "\"")) {
            return true;
        }
        return false;
    }


    //method to retrun the wifi security type with paramaters scanresult
    public int getWifiSecurityType(ScanResult scanResult) {
        int securityType = 0;
        String security = scanResult.capabilities;
        Log.d("security:", security);
        if (security.contains(SECURITY_WEP)) {
            securityType = 1;
        } else if (security.contains(SECURITY_EAP)) {
            securityType = 2;
        } else if (security.contains(SECURITY_PSK)) {
            securityType = 3;
        }
        return securityType;
    }


    //method to retrun a quoted string
    public static String quoted(String s) {
        return "\"" + s + "\"";
    }


    //method to get the security for the wifi ocnfiguration wwith parameter wifi ocnfiguration
    static String getSecurity(@NonNull WifiConfiguration config) {
        String security = SECURITY_NONE;


        if (config.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.NONE)) {
            // If we never set group ciphers, wpa_supplicant puts all of them.
            // For open, we don't set group ciphers.
            // For WEP, we specifically only set WEP40 and WEP104, so CCMP
            // and TKIP should not be there.
            if (config.wepKeys[0] != null)
                security = SECURITY_WEP;
            else
                security = SECURITY_NONE;
        }
        if (config.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.WPA_EAP) ||
                config.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.IEEE8021X)) {
            security = SECURITY_EAP;
        }
        if (config.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.WPA_PSK)) {
            security = SECURITY_PSK;
        }
        Log.d("Security", security);
        return security;
    }

    //method to set the security for the wifi ocnfiguration with parameters  ssid,security and password
    static WifiConfiguration setupSecurity(String ssid, int security, @NonNull final String password) {
        WifiConfiguration config = new WifiConfiguration();
        config.SSID = quoted(ssid);
        config.status = WifiConfiguration.Status.ENABLED;
        config.priority = 40;
        switch (security) {
            case 0:
                Log.v("rht", "Configuring OPEN network");
                config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                config.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                config.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                config.allowedAuthAlgorithms.clear();
                config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
                config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                break;
            case 1:
                Log.v("rht", "Configuring WEP");

                config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                config.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                config.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
                config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);
                config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);

                if (password.matches("^[0-9a-fA-F]+$")) {
                    config.wepKeys[0] = password;
                } else {
                    config.wepKeys[0] = "\"".concat(password).concat("\"");
                }

                config.wepTxKeyIndex = 0;

               /* config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                config.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                config.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
                config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);
                config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
                // WEP-40, WEP-104, and 256-bit WEP (WEP-232?)
                if (isHexWepKey(password))
                    config.wepKeys[0] = password;
                else
                    config.wepKeys[0] = convertToQuotedString(password);*/
                break;
            case 3:

                Log.v("rht", "Configuring WPA");


                config.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                config.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
                config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                config.preSharedKey = "\"" + password + "\"";
                break;
            case 2:
                Log.v("rht", "Configuring EAP");
                config.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                config.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
                config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_EAP);
                config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.IEEE8021X);
                config.preSharedKey = convertToQuotedString(password);
                break;

            default:
                break;
        }
        return config;
    }

    //method to connect to network with parameters ssid, security and password
    public boolean connectToNetwork(String ssid, int security, String password) {
        try {
            WifiManager wifiManager = (WifiManager) mContext
                    .getSystemService(Context.WIFI_SERVICE);
            int networkId = wifiManager.getConnectionInfo().getNetworkId();
            wifiManager.removeNetwork(networkId);
            wifiManager.saveConfiguration();
            // wifiManager.disconnect();
            WifiConfiguration conf = setupSecurity(ssid, security, password);
            wifiManager.addNetwork(conf);
            List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();

            for (WifiConfiguration i : list) {
                if (i.SSID != null && i.SSID.equals("\"" + ssid + "\"")) {

                    Log.v("rht", "WifiConfiguration SSID " + i.SSID);

                    boolean isDisconnected = wifiManager.disconnect();
                    Log.v("rht", "isDisconnected : " + isDisconnected);

                    boolean isEnabled = wifiManager.enableNetwork(i.networkId, true);
                    Log.v("rht", "isEnabled : " + isEnabled);

                    boolean isReconnected = wifiManager.reconnect();
                    Log.v("rht", "isReconnected : " + isReconnected);

                    /*wifiManager.disconnect();
                    wifiManager.enableNetwork(i.networkId, true);
                    wifiManager.reconnect();*/
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    static boolean isHexWepKey(@Nullable String wepKey) {
        final int passwordLen = wepKey == null ? 0 : wepKey.length();
        return passwordLen != 0 && (passwordLen == 10 || passwordLen == 26 || passwordLen == 58) && wepKey.matches("[0-9A-Fa-f]*");
    }


    @NonNull //method to ocnvert the string to quoted string
    static String convertToQuotedString(@NonNull String string) {
        if (TextUtils.isEmpty(string))
            return "";

        final int lastPos = string.length() - 1;
        if (lastPos < 0 || (string.charAt(0) == '"' && string.charAt(lastPos) == '"'))
            return string;

        return "\"" + string + "\"";
    }

}