package com.thril.mdm.business;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.IBinder;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.thril.mdm.Global.DateUtils;
import com.thril.mdm.Global.MDMSettings;
import com.thril.mdm.Network.utils.Constants;
import com.thril.mdm.Presenter.GpsPresenterListener;
import com.thril.mdm.databaselayer.databasemanager.DatabaseMgr;
import com.thril.mdm.model.GeoFence;
import com.thril.mdm.model.GpsAssetTracker;
import com.thril.mdm.model.GpsTrackerViolation;
import com.thril.mdm.service.GoogleClientService;
import com.thril.mdm.service.GpsService;


/**
 * Created by Admin on 1/13/2018.
 */

//class for calling gps service and all gps business logic
public class GpsServiceBusiness implements GpsBusinessListener {

    private static final String TAG = "GPSBusiness";

    private static GpsServiceBusiness instance = null;
    private static GpsPresenterListener gpsPresenterListener;
    static Context mContext;
    GpsService gpsService;
    private static long UPDATE_INTERVAL = 0;
    private static long FASTEST_INTERVAL = UPDATE_INTERVAL;

    //constructor for the class
    private GpsServiceBusiness() {

    }
    //Static Method returns the instance and creates a instance for the class
    public static GpsServiceBusiness getInstance(Context context) {

        if (instance == null) {
            instance = new GpsServiceBusiness();
        }
        mContext = context;
        return instance;

    }

    //method to set the gps present listener for the class with parameter gps presenter listener object
    public void setGPSPresenterListener(GpsPresenterListener gpsPresenterListener) {
        this.gpsPresenterListener = gpsPresenterListener;
    }

    //method to call gps service with parameters gps_service flag and time interval
    public void startGPSService(boolean gps_service, long timeInterval) {
        UPDATE_INTERVAL = timeInterval;
        if (gps_service) {
            GpsService service = new GpsService(timeInterval);
            Intent gpsIntent = new Intent(mContext, service.getClass());
            mContext.bindService(gpsIntent, mConnection, Context.BIND_AUTO_CREATE);
        } else {
            GoogleClientService googleClientService = GoogleClientService.getInstance(mContext);
            googleClientService.setGPSBusinessListener(this);
            googleClientService.buildGoogleClient();
            gpsSettings(googleClientService.mLocationClient, gps_service, timeInterval);


        }
    }

    //method to create geo fence with parameters gps_service flag
    public void addGeoFence(boolean gps_service) {
        if (!gps_service) {
            GeoFence geoFence = DatabaseMgr.getInstance(mContext).retrieveGeoFence();
            if (geoFence != null) {
                GoogleClientService googleClientService = GoogleClientService.getInstance(mContext);
                googleClientService.createGeoFence(geoFence.getLatitude(), geoFence.getLatitude(), geoFence.getRange());
            }

        }
    }
    //method to remove geo fence with parameters gps_service flag
    public void removeGeoFence(boolean gps_service) {
        if (!gps_service) {
            GoogleClientService googleClientService = GoogleClientService.getInstance(mContext);
            googleClientService.removeGeoFence();
        }
    }

    //method to stop gps service with parameters gps_service flag
    public void stopGPSService(boolean gps_service) {
        if (gps_service) {
            if (gpsService != null) {
                gpsService.removeUpdates();
            }
            if (mConnection != null) {
                mContext.unbindService(mConnection);
            }
        } else {
            //removeGeoFence(gps_service);
            GoogleClientService.getInstance(mContext).removeUpdates();

        }


    }

//method to set listnere for the gps service with paramters gps service object
    void setListener(GpsService gpsService) {
        gpsService.setGPSBusinessListener(this);

    }


     // Defines callbacks for service binding, passed to bindService()

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            GpsService.LocalBinder binder = (GpsService.LocalBinder) service;
            gpsService = binder.getService();
            gpsService.setContext(mContext);
            setListener(gpsService);
            Log.d(TAG, "Service Binded");

        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            Log.d(TAG, "Service Unbinded");
            gpsService = null;


        }
    };

    @Override // overridden method of gps business listener to insert gps location to database
    public void gpsLocation(Location location) {
        GpsAssetTracker gpsAssetTracker = new GpsAssetTracker(Double.toString(location.getLatitude()), Double.toString(location.getLongitude()), "" + DateUtils.getCurrentDateGMT());
        DatabaseMgr.getInstance(mContext).insertGpsAssetTrackerPosition(gpsAssetTracker);
        gpsPresenterListener.gpsLocation(location);

    }
//method to get the current location from gps service
    public void getCurrentGpsLocation() {
        GoogleClientService.getInstance(mContext).getCurrentGpsLocation();
    }

    @Override // overridden method of gps business listener to get the current location from gps service
    public void currentGpsLocation(Location location) {
        gpsPresenterListener.currentGpsLocation(location);
    }

    @Override// overridden method of gps business listener to insert gps vilotion reprot to database
    public void outOfRange(Location location) {
        GpsTrackerViolation gpsTrackerViolation = new GpsTrackerViolation();
        //  Log.d("Location GPSService",""+location.getLatitude());
        gpsTrackerViolation.setLatitude(Double.toString(location.getLatitude()));
        gpsTrackerViolation.setLongitude(Double.toString(location.getLongitude()));
        gpsTrackerViolation.setDeviatedEndDateTime(Long.toString(DateUtils.getCurrentDateGMT()));
        DatabaseMgr.getInstance(mContext).insertGpsTrackerViolationRecords(gpsTrackerViolation);
    }

    //method to call gps settings to enable with parameters googleapiclient, gpsservice flag and time
    public void gpsSettings(final GoogleApiClient googleApiClient, final boolean gps_servic, final long time) {

        LocationRequest locationRequest = LocationRequest.create();
        Log.d("GPS_Mode", "  " + Constants.gps_mode);
        if (Constants.gps_mode == 1 || Constants.gps_mode == 2) {
            //gps
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);//Setting priotity of Location request to high
            UPDATE_INTERVAL = time;
            FASTEST_INTERVAL = UPDATE_INTERVAL;
        } else {
            //network
            locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);//Setting priotity of Location request to high
            UPDATE_INTERVAL = time;
            FASTEST_INTERVAL = UPDATE_INTERVAL;
        }
        Log.e("UPDATE_INTERVAL", "  " + UPDATE_INTERVAL);

        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient to show dialog always when GPS is off

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //  gpsPresenterListener.gpsStatus();
                        GoogleClientService googleClientService = GoogleClientService.getInstance(mContext);
                        if (googleApiClient != null && googleApiClient.isConnected())
                            googleClientService.requestForLocationUpdates(UPDATE_INTERVAL);
                        //addGeoFence(gps_service);
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult((Activity) mContext, MDMSettings.REQUEST_GPS_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });


    }
}
