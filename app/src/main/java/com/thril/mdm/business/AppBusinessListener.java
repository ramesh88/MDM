package com.thril.mdm.business;

import com.thril.mdm.model.ResponseData;
import com.thril.mdm.model.StaffInfo;

/**
 * Created by Ram on 2/4/2018.
 */

//interface for the app business class
public interface AppBusinessListener {
    void onSuccess(ResponseData responseData);

    void onFailure(String error);

    void onAssetValidationSuccess(StaffInfo staffInfo);

    void onAssetValidationFailure(String error);

    void onDialogUpdate(String appLink);

    void showExitDialog();

    void onOTPSendingFailure(String error);

    void onOTPVerifyingFailure(String error);

    void exitApp();

}
