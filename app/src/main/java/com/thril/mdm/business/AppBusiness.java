package com.thril.mdm.business;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.thril.mdm.AppWSRepository.AppRegistration;
import com.thril.mdm.Global.ApkDownloader;
import com.thril.mdm.Presenter.AppPresenterListener;
import com.thril.mdm.databaselayer.databasemanager.DatabaseMgr;
import com.thril.mdm.model.App;
import com.thril.mdm.model.AppUsage;
import com.thril.mdm.model.Device;
import com.thril.mdm.model.DeviceUsage;
import com.thril.mdm.model.ResponseData;
import com.thril.mdm.model.StaffInfo;
import com.thril.mdm.model.UserControl;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by Ram on 2/4/2018.
 */
//App business class which used to all business operations like calling app registration class methods
public class AppBusiness implements AppBusinessListener {
    private static AppBusiness instance = null;
    private static Context mContext;
    private static AppPresenterListener mAppPresenterListener;
    private static DatabaseMgr mDatabaseMgr;
    private static final String TAG = "AppBusiness";

    //constructor for the class
    private AppBusiness() {

    }
    //Static Method returns the instance and creates a instance for the class
    public static AppBusiness getInstance(Context context) {
        mContext = context;
        if (instance == null) {
            instance = new AppBusiness();
            Log.d(TAG, "AppBusiness");
            mDatabaseMgr = DatabaseMgr.getInstance(mContext);
        }
        return instance;
    }

    //method to set the app present listener for the class with parameter app presenter listener object
    public void setAppPresenterListener(AppPresenterListener appPresenterListener) {
        mAppPresenterListener = appPresenterListener;
    }

    //method to list the apps from database if database records exists or from rest API
    public void getAppList(Device deviceInfo) {
        if (mDatabaseMgr.getAppListRecordCount() > 0) {
            Log.d(TAG, "From DB");
            ResponseData responseData = new ResponseData();
            responseData.setApp(mDatabaseMgr.retrieveAppList());
            responseData.setAppConfiguration(mDatabaseMgr.retrieveAppConfiguration());
            mAppPresenterListener.onAppRegistrationSuccess(responseData);
        } else {
            AppRegistration.getmInstance().appRegistration(this, deviceInfo);
        }
    }

    //method for verifying asset identifier with parameters asset identifier , iemi number and version no
    public void verifyAssetIdentifier(String assetIdentifier, String imeiNumber, String versionNo) {
        AppRegistration.getmInstance().verifyAssetIdentifier(this, assetIdentifier, imeiNumber, versionNo);
    }

    //method for send otp with parameters asset identifier
    public void sendOTP(String assetIdentifier) {
        AppRegistration.getmInstance().sendOTP(this, assetIdentifier);
    }

    //method for verifying otp with parameters asset identifier and otp
    public void verifyOTP(String assetIdentifier, String otp) {
        AppRegistration.getmInstance().verifyOTP(this, assetIdentifier, otp);
    }

    //Async task class  to download the file from server
    public class FileDownloader extends AsyncTask<Void, Void, String> {
        String urlPath = "";
        boolean flag;


        public FileDownloader(String url, boolean fileFlag) {
            urlPath = url;
            flag = fileFlag;
            Log.d("Url path:", urlPath);
            Log.d("flag", "" + flag);
        }

        @Override
        protected String doInBackground(Void... params) {
            return ApkDownloader.getFileOrApk(urlPath, flag);
        }
    }

    //Method retruns list of modified apps from server with parameters of list of apps
    public List<App> loadFileFromServer(List<App> appList) {
        List<App> modifiedAppList = appList;
        int count = 0;
        while (count < appList.size()) {
            try {
                App app = appList.get(count);
                if (app != null && (app.getType() != null && app.getType().equals("New"))) {

                    modifiedAppList.get(count).setIconPath(new FileDownloader(appList.get(count).getIconPath(), true).execute().get());
                } else {
                    modifiedAppList.get(count).setIconPath(new FileDownloader(appList.get(count).getIconPath(), true).execute().get());
                }
                //modifiedAppList.get(count).setApkPath(new FileDownloader(appList.get(count).getApkPath(),false).execute().get());
                count++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        return modifiedAppList;
    }

    //method to update database coloumn with parameters column name, column value and column type
    public void updateDBColumn(String columnName, String columnValue, boolean columnType) {
        mDatabaseMgr.updateColumn(columnName, columnValue, columnType);
    }

    //method to delete notification from database with parameters of notification id
    public void deleteNotification(String not_id) {
        mDatabaseMgr.deleteNotification(not_id);
    }

    //method to delete all notifications from database
    public void deleteAllNotification() {
        mDatabaseMgr.deleteAllNotification();
    }


    //overridden method of appbusiness listener with parameters response data from server
    @Override
    public void onSuccess(ResponseData responseData) {
        if (mDatabaseMgr.getAppListRecordCount() == 0) {
            Log.d(TAG, "From Service");
            List<App> app = responseData.getApp();
            List<App> modifiedAppList = loadFileFromServer(app);
            mDatabaseMgr.insertAppList(modifiedAppList);
            mDatabaseMgr.insertAppConfiguration(responseData.getAppConfiguration());
        } else {

            // update should be here
        }
        ResponseData responseData_db = new ResponseData();
        responseData_db.setApp(mDatabaseMgr.retrieveAppList());
        responseData_db.setAppConfiguration(mDatabaseMgr.retrieveAppConfiguration());
        mAppPresenterListener.onAppRegistrationSuccess(responseData_db);
        // mAppPresenterListener.onAppRegistrationSuccess(responseData);

    }

    @Override //overridden method of appbusiness listener with parameters error response form server
    public void onFailure(String error) {
        mAppPresenterListener.onAppRegistrationFailure(error);
    }

    @Override //overridden method of appbusiness listener with parameters staff info response form server
    public void onAssetValidationSuccess(StaffInfo staffInfo) {
        mAppPresenterListener.onAssetValidationSuccess(staffInfo);

    }

    @Override//overridden method of appbusiness listener with parameters error response form server
    public void onAssetValidationFailure(String error) {
        mAppPresenterListener.onAssetValidationFailure(error);
    }

    @Override //overridden method of appbusiness listener with parameters of app link when app update is found
    public void onDialogUpdate(String appLink) {
        mAppPresenterListener.onDialogUpdateApk(appLink);

    }

    @Override //overridden method of appbusiness listener which shows exit dialog when otp is verified
    public void showExitDialog() {
        mAppPresenterListener.showExitDialog();
    }

    @Override //overridden method of appbusiness listener which shows an alert on otp seing failure with parameters error message form server
    public void onOTPSendingFailure(String error) {
        mAppPresenterListener.onOTPSendingFailure(error);
    }

    @Override //overridden method of appbusiness listener which shows an alert on otp verifying failure with parameters error message form server
    public void onOTPVerifyingFailure(String error) {
        mAppPresenterListener.onOTPVerifyingFailure(error);
    }

    @Override //overridden method of appbusiness listener which calls exit
    public void exitApp() {
        mAppPresenterListener.exitApp();
    }

    //method to set the notification to view of main activity page
    public void getNotificationCount() {
        mAppPresenterListener.notificationCount(mDatabaseMgr.getNotificationDataCount());
    }
//method to list the notification to view of main activity page
    public void getNotificationList() {
        mAppPresenterListener.notificationList(mDatabaseMgr.getNotificationData());
    }
//method to insert the device usage report to database
    public void saveDeviceUsage(DeviceUsage deviceUsage) {
        mDatabaseMgr.insertDeviceUsageRecords(deviceUsage);
    }

    //method to insert the app usage report to database
    public void saveAppUsage(AppUsage appUsage) {
        mDatabaseMgr.insertAppUsageRecords(appUsage);
    }

    //method to fetch the user control from database
    public UserControl getUserControls() {
        return mDatabaseMgr.getUserControl();
    }
}
