package com.thril.mdm.Network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Ram on 9/13/2017.
 */
//class to create a retrofit api object for the given url
public class RetrofitApi {

    private static Retrofit retrofit = null;
    // private static final String BASE_URL = "http://182.18.164.29:2425/V1/";

    //http://125.62.194.36:2425/V1/AppCommandCenter/DeviceRegistration/
    private static final String BASE_URL = "http://aprntcp.dmxapp.in/V1/";
    // private static final String BASE_URL= "http://rntcp.dmxapp.in/V1/";
    private static final long CONNECT_TIMEOUT = 60000;   // 2 seconds
    private static final long READ_TIMEOUT = 60000;      // 2 seconds

//method to create a retrofit object
    public static Retrofit getRetrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getOkHttpClient())
                    .build();
        }
        return retrofit;
    }
    //method to create a http client object
    public static OkHttpClient getOkHttpClient() {

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.MILLISECONDS)
                .addInterceptor(new OkHttpInterceptor()).build();
        return okHttpClient;


    }


}
