package com.thril.mdm.Network;

import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Ram on 2/4/2018.
 */
//Class to create intercept object for retrofit
public class OkHttpInterceptor implements Interceptor{
    private static final String TAG="Interceptor";
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request=chain.request();
        /*if(!Constants.isNetworkAvailable(Constants.getmContext())){
            Log.d(TAG,"No Network");
            Response response=new ResponseBuilder().getResponseBuilder(Constants.RESPONSE_CODE,Constants.DESCRIPTION,Constants.loadData("json/Response.json"),request);
            Log.d("Response",new Gson().toJson(response.body()));
            return response;
        }else{*/
           /* Log.d(TAG,"Network available");
            Response response=chain.proceed(request);
            Log.d("Response",response.code()+" "+new Gson().toJson(response.body()));
            return chain.proceed(request);*/
            Log.d(TAG,"Network available");
            //Response response=new ResponseBuilder().getResponseBuilder(Constants.RESPONSE_CODE,Constants.DESCRIPTION,Constants.loadData("json/Response.json"),request);
            Response response=chain.proceed(request);
            return response;
     //   }

       // return chain.proceed(request);
    }
}
