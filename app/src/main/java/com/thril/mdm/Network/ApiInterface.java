package com.thril.mdm.Network;

import com.thril.mdm.model.Device;
import com.thril.mdm.model.DeviceResponse;
import com.thril.mdm.model.OTPResponse;
import com.thril.mdm.model.ResponseData;
import com.thril.mdm.model.StaffInfo;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Ram on 2/4/2018.
 */

//Interface for all API services
public interface ApiInterface {
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("AppCommandCenter/DeviceRegistration")
    Call<ResponseData> deviceRegistration(@Body Device input);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("mobile/AppRegistration")
    Call<ResponseData> appRegistration(@Body Device input);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("AppCommandCenter/DeviceResponse")
    Call<ResponseData> deviceResponse(@Body DeviceResponse deviceResponse);

    @GET("AppCommandCenter/DeviceValidation/{assetIdentifier}/{imeiNumber}/{versionNo}")
    Call<StaffInfo> verifyAssetIdentifier(@Path("assetIdentifier") String assetIdentifier, @Path("imeiNumber") String imeiNumber, @Path("versionNo") String versionNo);


    @GET("AppCommandCenter/sendotp/{assetIdentifier}")
    Call<OTPResponse> sendOTP(@Path("assetIdentifier") String assetIdentifier);


    @GET("AppCommandCenter/verifyotp/{assetIdentifier}/{otp}")
    Call<OTPResponse> verifyOTP(@Path("assetIdentifier") String assetIdentifier, @Path("otp") String otp);

}
