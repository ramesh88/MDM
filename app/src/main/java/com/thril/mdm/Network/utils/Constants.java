package com.thril.mdm.Network.utils;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.thril.mdm.Global.ApkDownloader;
import com.thril.mdm.Global.DateUtils;
import com.thril.mdm.Global.MDMSettings;
import com.thril.mdm.model.StorageFreeSpace;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import static android.telephony.CellSignalStrength.SIGNAL_STRENGTH_GOOD;
import static android.telephony.CellSignalStrength.SIGNAL_STRENGTH_GREAT;
import static android.telephony.CellSignalStrength.SIGNAL_STRENGTH_MODERATE;
import static android.telephony.CellSignalStrength.SIGNAL_STRENGTH_NONE_OR_UNKNOWN;
import static android.telephony.CellSignalStrength.SIGNAL_STRENGTH_POOR;

/**
 * Created by Ram on 2/4/2018.
 */

//Class to declare any constants objects and methods
public class Constants {

    private static Context mContext;
    public static final int RESPONSE_CODE = 200;
    public static final String DESCRIPTION = "SUCCESS";
    public static final String APP_FOLDER = ".MDM";
    private static final String TAG = "Constant";
    public static String Device_Switch_On_Date_Time = "";
    public static String Device_Switch_Off_Date_Time = "";
    public static String app_start_date_time = "";
    public static String app_stop_date_time = "";
    public static int mSignalStrength = 0;
    public static int mSignalLevel = 0;
    public static final String SERVER_UPDATE_ACTION = "com.thril.mdm.SERVER_UPDATE";
    public static final String NOTIFICATION_DATA_UPDATE = "com.thril.mdm.NOTIFICATION_COUNT";
    public static final String STOP_SETTINGS = "com.thril.mdm.STOP_SETTINGS";
    public static final String SMS_UPDATES = "com.thrill.mdm.SMS_UPDATES";

    public static final String PHONE_CALL_UPDATES = "com.thrill.mdm.PHONE_CALL_UPDATE";
    public static final String[] PERMISSION = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.READ_SMS, Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS, Manifest.permission.CALL_PHONE
    };
    public static final String WIFI = "Wi-Fi";
    public static final String MOBILE = "Mobile";
    public static final String NO_NETWORK = "No Network";
    public static String asset_identifier = "";
    public static int gps_mode = 0;
    public static int settings_index = 1;
    public static boolean frgndFlag = true;
    public static boolean phoneCallFlag = false;

    //method to get context
    public static Context getmContext() {
        return mContext;
    }

    //method to set context
    public static void setmContext(Context mContext) {
        Constants.mContext = mContext;
    }

    //method to check network avialbale or not
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityMgr.getActiveNetworkInfo();
        /// if no network is available networkInfo will be null
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    //class for phone state lsitner to get signal strength
    public static class MyPhoneStateListener extends PhoneStateListener {

        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);
            mSignalStrength = signalStrength.getGsmSignalStrength();
            mSignalLevel = signalStrength.getLevel();
            Log.d("mSignalStrength before", "" + mSignalStrength);
            mSignalStrength = (2 * mSignalStrength) - 113; // -> dBm
            Log.d("mSignalStrength after", "" + mSignalStrength);
        }
    }

    //method to get signal strength
    @RequiresApi(api = Build.VERSION_CODES.M)
    public static int getSignalStrength(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        MyPhoneStateListener myPhoneStateListener = new MyPhoneStateListener();
// for example value of first element
        telephonyManager.listen(myPhoneStateListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
        //  CellInfoGsm cellinfogsm = (CellInfoGsm)telephonyManager.getAllCellInfo().get(0);
        // CellSignalStrengthGsm cellSignalStrengthGsm = cellinfogsm.getCellSignalStrength();
        Log.d("mSignalStrength", "" + mSignalStrength);
        Log.d("MSignalLevel", "" + mSignalLevel);
        return mSignalLevel;
    }

    //method to get drawable for asset file name
    public static Bitmap drawableFromAssetFilename(String filename) {
        AssetManager assetManager = getmContext().getAssets();
        InputStream inputStream = null;
        try {
            inputStream = assetManager.open(filename);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
        return bitmap;
    }

    //method to load data from file
    public static String loadData(String inFile) {
        String tContents = "";
        try {
            InputStream stream = getmContext().getAssets().open(inFile);
            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            tContents = new String(buffer);
        } catch (IOException e) {
            // Handle exceptions here
        }

        return tContents;

    }

    //method to choos launcher
    public static void chooseLauncher(Context ctx) {
        Intent selector = new Intent(
                "android.intent.action.MAIN");
        selector.addCategory("android.intent.category.HOME");
        selector.setComponent(new ComponentName(
                "android",
                "com.android.internal.app.ResolverActivity"));
        ((Activity) ctx).startActivityForResult(selector, MDMSettings.REQUEST_LAUNCHER);
    }

    //method get the name of home app
    public static String getNameOfHomeApp(Context context) {
        try {
            Intent i = new Intent(Intent.ACTION_MAIN);
            i.addCategory(Intent.CATEGORY_HOME);
            PackageManager pm = context.getPackageManager();
            final ResolveInfo mInfo = pm.resolveActivity(i, PackageManager.MATCH_DEFAULT_ONLY);
            Log.d("Constants pkg name ", mInfo.activityInfo.packageName);
            return mInfo.activityInfo.packageName;
        } catch (Exception e) {
            return "";
        }

    }

//method to check application is set to default launcher or not
    public static boolean isMyAppLauncherDefault(Context context) {
        final IntentFilter filter = new IntentFilter(Intent.ACTION_MAIN);
        filter.addCategory(Intent.CATEGORY_HOME);

        List<IntentFilter> filters = new ArrayList<IntentFilter>();
        filters.add(filter);

        final String myPackageName = context.getPackageName();
        List<ComponentName> activities = new ArrayList<ComponentName>();
        final PackageManager packageManager = (PackageManager) context.getPackageManager();

        packageManager.getPreferredActivities(filters, activities, null);

        for (ComponentName activity : activities) {
            Log.d("launcher name:", activity.getPackageName());
            if (myPackageName.equals(activity.getPackageName())) {
                return true;
            }
        }
        return false;
    }

//method to get network name
    public static String getNetworkCarrierName(Context context) {
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String carrierName = manager.getNetworkOperatorName();
        return carrierName;
    }

//method check network status of device
    public static String checkNetworkStatus(final Context context) {

        String networkStatus = "";

        // Get connect mangaer
        final ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);

        // check for wifi
        final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        // check for mobile data
        final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);


        if (wifi.isAvailable()) {
            networkStatus = WIFI;


        } else if (mobile.isAvailable()) {
            networkStatus = MOBILE;

        } else {
            networkStatus = NO_NETWORK;
        }

        return networkStatus;

    }  // end checkNetworkStatus

    //method to set bytes to string format
    public static String bytesHumanReadable(long bytes, boolean showInByte) {

        if (!showInByte) {
            long bits = bytes * 8;

            long Kbit = 1024;
            long Mbit = Kbit * 1024;
            long Gbit = Mbit * 1024;

            if (bits < Kbit)
                return String.valueOf(bits) + " bit";
            if (bits > Kbit && bits < Mbit)
                return String.valueOf(bits / Kbit) + " Kilobit";
            if (bits > Mbit && bits < Gbit)
                return String.valueOf(bits / Mbit) + " Megabit";
            if (bits > Gbit)
                return String.valueOf(bits / Gbit) + " Gigabit";
            return "???";
        } else {

            long bits = bytes;

            long Kbit = 1024;
            long Mbit = Kbit * 1024;
            long Gbit = Mbit * 1024;

          /*  if (bits < Kbit)
                return String.valueOf(bits) + " Byte";
            if (bits > Kbit && bits < Mbit)
                return String.valueOf(bits / Kbit) + " KiloByte";
            if (bits > Mbit && bits < Gbit)*/
            return String.valueOf(bits / Mbit);// + " MegaByte";
            /*if (bits > Gbit)
                return String.valueOf(bits / Gbit) + " GigaByte";
            return "???";*/

        }
    }

    //method to get connected wifi name
    public static String getWifiName(Context context) {
        WifiManager manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (manager.isWifiEnabled()) {
            WifiInfo wifiInfo = manager.getConnectionInfo();
            if (wifiInfo != null) {
                NetworkInfo.DetailedState state = WifiInfo.getDetailedStateOf(wifiInfo.getSupplicantState());
                if (state == NetworkInfo.DetailedState.CONNECTED || state == NetworkInfo.DetailedState.OBTAINING_IPADDR) {
                    return wifiInfo.getSSID();
                }
            }
        }
        return null;
    }

    //method to get bitmap from file path
    public static Bitmap getImageBitmap(String filePath) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, 80, 80);
        options.inJustDecodeBounds = false;
        bitmap = BitmapFactory.decodeFile(filePath, options);

        return bitmap;
    }

    //method to set size of bitmap
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    //method to check apk exists in file or not
    public static String checkApkExist(String apkName) {
        String filePath = Environment.getExternalStorageDirectory() + ApkDownloader.APKPATH + apkName;
        File f = new File(filePath);
        if (f.exists()) {
            return filePath;
        }
        return "";
    }

    //method to get the signallevel of  sim in a device
    public static int getSignalLevel(int signalStrength) {
        int level;
        if (signalStrength <= 2 || signalStrength == 99)
            level = SIGNAL_STRENGTH_NONE_OR_UNKNOWN;
        else if (signalStrength >= 12)
            level = SIGNAL_STRENGTH_GREAT;

        else if (signalStrength >= 8)
            level = SIGNAL_STRENGTH_GOOD;

        else if (signalStrength >= 5)
            level = SIGNAL_STRENGTH_MODERATE;

        else
            level = SIGNAL_STRENGTH_POOR;
        return level;
    }

    //method get signal quality of sim
    public static String getSignalQuality(int signalStrength) {
        //int signalLevel = getSignalLevel(signalStrength);
        //String quality = "" + (signalLevel + 1);
        if (signalStrength == 0)
            signalStrength = 1;
        String quality = "" + (signalStrength);
      /*  if(signalLevel == 0){
            quality = "No Signal";
        }else if(signalLevel == 1){
            quality = "Poor";
        }else if(signalLevel == 2){
            quality = "Moderate";
        }else if(signalLevel == 3){
            quality = "Good";
        }else if(signalLevel == 4){
            quality = "Excellent";
        }*/
        return quality;

    }

//method to get free space of a device
    public static StorageFreeSpace getStorageFreeSpace() {
        File file = Environment.getExternalStorageDirectory();
        long externalFreeSpace = file.getFreeSpace();

        File internal = Environment.getDataDirectory();
        long internalFreeSpace = internal.getFreeSpace();

        StorageFreeSpace storageFreeSpace = new StorageFreeSpace(getFileSize(externalFreeSpace), getFileSize(internalFreeSpace), Long.toString(DateUtils.getCurrentDateGMT()));

        return storageFreeSpace;
    }

    //method to get file size
    public static String getFileSize(long size) {
        if (size <= 0)
            return "0";
        /*final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];*/
        return Float.toString((float) Math.round((size / (1024 * 1024 * 1024)) * 10) / 10);
    }

    //method to get current date in string
    public static String getCurrentDateToString() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        df.setTimeZone(TimeZone.getDefault());

        return df.format(c.getTime());
    }


}
