package com.thril.mdm.decorator;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Ram on 5/4/2017.
 */

//Class to create a vertical space for recylcer views
public class VerticalSpace extends RecyclerView.ItemDecoration {

    private int bottomSpace;

    public VerticalSpace(int space){
        bottomSpace=space;

    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.bottom=bottomSpace;

    }
}
