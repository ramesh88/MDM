package com.thril.mdm.view.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.thril.mdm.Global.MDMSettings;
import com.thril.mdm.Global.PhoneContact;
import com.thril.mdm.Network.utils.Constants;
import com.thril.mdm.R;
/**
 * Created by Ram on 6/13/2018.
 */

//Fragment class to open custom dialpad
public class OpenDialpad extends Fragment implements View.OnClickListener, View.OnLongClickListener {

    TextView tv_phone_number, tv_0, tv_1, tv_2, tv_3, tv_4, tv_5, tv_6, tv_7, tv_8, tv_9, tv_plus, tv_ash;
    ImageView dial, cancel_characters;
    String phoneNumber = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dial_pad, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tv_phone_number = (TextView) view.findViewById(R.id.tv_phone_number);

        tv_0 = (TextView) view.findViewById(R.id.tv_0);
        tv_0.setOnClickListener(this);
        tv_0.setOnLongClickListener(this);
        tv_1 = (TextView) view.findViewById(R.id.tv_1);
        tv_1.setOnClickListener(this);
        tv_2 = (TextView) view.findViewById(R.id.tv_2);
        tv_2.setOnClickListener(this);
        tv_3 = (TextView) view.findViewById(R.id.tv_3);
        tv_3.setOnClickListener(this);
        tv_4 = (TextView) view.findViewById(R.id.tv_4);
        tv_4.setOnClickListener(this);
        tv_5 = (TextView) view.findViewById(R.id.tv_5);
        tv_5.setOnClickListener(this);
        tv_6 = (TextView) view.findViewById(R.id.tv_6);
        tv_6.setOnClickListener(this);
        tv_7 = (TextView) view.findViewById(R.id.tv_7);
        tv_7.setOnClickListener(this);
        tv_8 = (TextView) view.findViewById(R.id.tv_8);
        tv_8.setOnClickListener(this);
        tv_9 = (TextView) view.findViewById(R.id.tv_9);
        tv_9.setOnClickListener(this);
        tv_plus = (TextView) view.findViewById(R.id.tv_plus);
        tv_plus.setOnClickListener(this);
        tv_ash = (TextView) view.findViewById(R.id.tv_ash);
        tv_ash.setOnClickListener(this);

        dial = (ImageView) view.findViewById(R.id.dail);
        dial.setOnClickListener(this);
        cancel_characters = (ImageView) view.findViewById(R.id.cancel_characters);
        cancel_characters.setOnClickListener(this);


    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.cancel_characters:
                phoneNumber = tv_phone_number.getText().toString().trim();
                if (phoneNumber.length() > 0) {
                    phoneNumber = phoneNumber.substring(0, phoneNumber.length() - 1);
                    tv_phone_number.setText(phoneNumber);
                }
                break;
            case R.id.dail:
                phoneNumber = tv_phone_number.getText().toString().trim();
                Constants.frgndFlag = false;
                Constants.phoneCallFlag = true;
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + phoneNumber));
                startActivity(callIntent);
                break;
           /* case R.id.dail_close:
                closeDialPad();
                break;*/
            default:
                phoneNumber = tv_phone_number.getText() + ((TextView) view).getText().toString().trim();
                tv_phone_number.setText(phoneNumber);
                break;
        }
    }

    @Override
    public boolean onLongClick(View view) {
        if (view == tv_0) {
            phoneNumber = tv_phone_number.getText() + "+".trim();
            tv_phone_number.setText(phoneNumber);
        }
        return true;
    }

//    public void closeDialPad() {
//            tv_phone_number.setText("");
//
//    }

//    public void openDialpad() {
//            tv_phone_number.setText("");
//            dialPadLayout.setVisibility(View.VISIBLE);
//        }
//    }


}
