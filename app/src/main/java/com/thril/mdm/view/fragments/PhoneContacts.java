package com.thril.mdm.view.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.thril.mdm.Global.DeviceInfo;
import com.thril.mdm.Global.PhoneContact;
import com.thril.mdm.Network.utils.Constants;
import com.thril.mdm.R;
import com.thril.mdm.adapter.ContactAdapter;
import com.thril.mdm.kiosk.MainActivity;
import com.thril.mdm.model.Contacts;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ram on 6/13/2018.
 */

//Fragment class to show phoen contacts
public class PhoneContacts extends Fragment implements ContactAdapter.PhoneCallListener {

    RecyclerView recyclerView;
    public List<Contacts> contactsList = new ArrayList<>();
    ContactAdapter contactAdapter;
    EditText input_search;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.contacts_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        input_search = (EditText) view.findViewById(R.id.input_search);
        input_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (input_search.getText().toString().length() > 0) {
                    LoadContacts(input_search.getText().toString());
                } else {
                    LoadContacts("");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        LoadContacts("");


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (input_search != null)
                input_search.setText("");
            if (getActivity() != null)
                LoadContacts("");
        }

    }


    public void LoadContacts(String str) {
        contactsList = new ArrayList<>();
        if (str.length() == 0) {
            contactsList = PhoneContact.getContactList(getActivity());
        } else {
            List<Contacts> contacts = PhoneContact.getContactList(getActivity());
            for (int i = 0; i < contacts.size(); i++) {
                Contacts contacts1 = contacts.get(i);
                if (contacts1.getName().toLowerCase().indexOf(str.toLowerCase()) != -1) {
                    contactsList.add(contacts1);
                } else if (contacts1.getPhoneNumber().toLowerCase().indexOf(str.toLowerCase()) != -1) {
                    contactsList.add(contacts1);
                }

            }
        }


        contactAdapter = new ContactAdapter(contactsList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(contactAdapter);
    }

    @Override
    public void callNumber(final String phoneNumber) {
        Constants.frgndFlag = false;
        Constants.phoneCallFlag = true;
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phoneNumber));
        startActivity(callIntent);
    }

    @Override
    public void deleteNumber(String phoneNumber, int position) {
        showDeleteAlert(phoneNumber, position);

    }


    public void showDeleteAlert(final String phoneNumber, final int position) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
        builder.setTitle("Alert..!")
                .setMessage("Do you want to delete this contact")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        PhoneContact.deleteContact(phoneNumber, getActivity());
                        contactsList.remove(position);
                        contactAdapter.notifyDataSetChanged();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
