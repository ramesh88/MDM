package com.thril.mdm.view.fragments;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.net.wifi.ScanResult;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.thril.mdm.BaseActivity;
import com.thril.mdm.Global.ApkDownloader;
import com.thril.mdm.Global.MDMSettings;
import com.thril.mdm.Network.utils.Constants;
import com.thril.mdm.R;
import com.thril.mdm.adapter.AppListAdapter;
import com.thril.mdm.databaselayer.databasemanager.DatabaseMgr;
import com.thril.mdm.kiosk.MainActivity;
import com.thril.mdm.model.App;
import com.thril.mdm.model.NotificationData;
import com.thril.mdm.model.ResponseData;
import com.thril.mdm.model.StaffInfo;
import com.thril.mdm.service.StopSettingsService;
import com.thril.mdm.view.AppView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ram on 6/13/2018.
 */

//Fragment class to list important apps in page 1
public class AppListPage1 extends Fragment implements AppView {

    RecyclerView page_1_recyclerView;
    TextView tv_no_apps;
    // public static ResponseData responseData;

    List<App> page_1_list;
    public static AppListAdapter appListAdapter1;
    Activity mA = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_page_1, container, false);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (page_1_recyclerView != null)
                LoadData();
        }

    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        mA = activity;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (page_1_recyclerView != null)
            LoadData();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        page_1_recyclerView = (RecyclerView) view.findViewById(R.id.page_1_recyclerView);
        tv_no_apps = (TextView) view.findViewById(R.id.tv_no_apps);
        tv_no_apps.setVisibility(View.GONE);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mA, 3);
        page_1_recyclerView.setLayoutManager(gridLayoutManager);
        LoadData();
    }


    @Override
    public void onAppRegistrationSuccess(ResponseData responseData) {

    }

    //method to load the apps in page 1
    public void LoadData() {
        //this.responseData = responseData;
        page_1_list = new ArrayList<>();
        DatabaseMgr databaseMgr = DatabaseMgr.getInstance(mA);
        List<App> list = databaseMgr.retrieveAppList();
        for (int i = 0; i < list.size(); i++) {
            App app = list.get(i);
            // Log.e("title", app.getTitle());
            if (app.getTitle().toLowerCase().indexOf("nikshay") != -1) {
                page_1_list.add(app);
            } else if (app.getTitle().toLowerCase().indexOf("nikshay aushadi") != -1) {
                page_1_list.add(app);
            } else if (app.getTitle().toLowerCase().indexOf("99") != -1) {
                page_1_list.add(app);
            } else if (app.getTitle().toLowerCase().indexOf("adivasi") != -1) {
                page_1_list.add(app);
            } else if (app.getTitle().toLowerCase().indexOf("identify") != -1) {
                page_1_list.add(app);
            } else if (app.getTitle().toLowerCase().indexOf("anm digi") != -1) {
                page_1_list.add(app);
            }
        }

        if (page_1_list.size() == 0) {
            page_1_recyclerView.setVisibility(View.GONE);
            tv_no_apps.setVisibility(View.VISIBLE);
        } else {
            appListAdapter1 = new AppListAdapter(mA, page_1_list, this);
            page_1_recyclerView.setAdapter(appListAdapter1);
            page_1_recyclerView.setVisibility(View.VISIBLE);
            tv_no_apps.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAppRegistrationFailure(String error) {

    }

    @Override
    public void onAction(int position) {
        Log.d("onAction", "Clicked " + position);
        Constants.frgndFlag = false;
        App app = page_1_list.get(position);
        String packageName = app.getPackageName();
        Constants.settings_index = 1;
//        mA.stopService(new Intent(mA, StopSettingsService.class));
//        mA.startService(new Intent(mA, StopSettingsService.class));

        MDMSettings.appPackage = packageName;
        MDMSettings.appId = app.getAppid();
        String apkPath = app.getApkPath();
        String type = app.getType();
        Log.d("package name", "Clicked " + packageName);
        if (type.trim().equalsIgnoreCase("new") && ApkDownloader.appInstalledOrNot(mA, packageName)) {
            Log.i("", "Application is already installed.");
            String path = Constants.checkApkExist(apkPath.substring(apkPath.lastIndexOf("/") + 1).trim());
            if (path.length() > 0) {
                File f = new File(path);
                if (f.exists()) {
                    f.delete();
                    f.deleteOnExit();

                }
            }
            ApkDownloader.launchApk(mA, packageName);
        } else if (type.trim().equalsIgnoreCase("update") && ApkDownloader.appInstalledOrNot(mA, packageName)) {
            Log.i("", "Application is already installed.");
            String path = Constants.checkApkExist(apkPath.substring(apkPath.lastIndexOf("/") + 1).trim());
            if (path.length() > 0) {
                File f = new File(path);
                if (f.exists()) {
                    f.delete();
                    f.deleteOnExit();

                }
            }
            ApkDownloader.launchApk(mA, packageName);
        } else {
            if (apkPath.toLowerCase().indexOf("play") != -1) {
                if (ApkDownloader.appInstalledOrNot(mA, packageName)) {
                    ApkDownloader.launchApk(mA, packageName);
                } else {
                    Toast.makeText(mA, app.getTitle() + " is not downloaded", Toast.LENGTH_LONG).show();
                }
            } else {
                String version = ApkDownloader.appInstalledAppVersion(mA, packageName);
                if (app.getVersionNo() != null && version != null && version.trim().equalsIgnoreCase(app.getVersionNo().trim())) {
                    String path = Constants.checkApkExist(apkPath.substring(apkPath.lastIndexOf("/") + 1).trim());
                    if (path.length() > 0) {
                        File f = new File(path);
                        if (f.exists()) {
                            f.delete();
                            f.deleteOnExit();

                        }
                    }
                    ApkDownloader.launchApk(mA, packageName);
                } else {
                    String path = Constants.checkApkExist(apkPath.substring(apkPath.lastIndexOf("/") + 1).trim());
                    Log.e("Path", path);
                    if (!path.equals("")) {
                        ApkDownloader.installApk(mA, path);

                    } else {
                        if (Constants.isNetworkAvailable(mA)) {
                            ((MainActivity) mA).new ApkFileDownloader(apkPath, false, app.getTitle()).execute();
                        } else {
                            Toast.makeText(mA, "Please check your network", Toast.LENGTH_LONG).show();
                        }
                    }

                }


            }

        }
    }

    @Override
    public void onNotificationAction(int position) {

    }

    @Override
    public void gpsLocation(Location location) {

    }

    @Override
    public void gpsServiceStatus(String msg) {

    }

    @Override
    public void discoveryList(List<String> discoveryList, boolean flag, List<ScanResult> scanResultList) {

    }

    @Override
    public void hotSpotName(String hotSpotName, String password) {

    }

    @Override
    public void disableHotspot(boolean flag) {

    }

    @Override
    public void hopSpotConnectionStatus(boolean flag) {

    }

    @Override
    public void fileList(List<String> llist) {

    }

    @Override
    public void currentGpsLocation(Location location) {

    }

    @Override
    public void socketStatus(String socketStatus, boolean socketFlag) {

    }

    @Override
    public void updateProgress(int percentage) {

    }

    @Override
    public void fileStatus(boolean flag) {

    }

    @Override
    public void onAssetValidationSuccess(StaffInfo staffInfo) {

    }

    @Override
    public void onAssetValidationFailure(String error) {

    }

    @Override
    public void connectedWifi(String ssid) {

    }

    @Override
    public void notificationCount(int count) {

    }

    @Override
    public void notificationList(List<NotificationData> notificationDataList) {

    }

    @Override
    public void notificationDelete(int position) {

    }

    @Override
    public void onDialogUpdateApk(String appLink) {

    }

    @Override
    public void showExitDialog() {

    }

    @Override
    public void exitApp() {

    }

    @Override
    public void onOTPSendingFailure(String error) {

    }

    @Override
    public void onOTPVerifyFailure(String error) {

    }
}
