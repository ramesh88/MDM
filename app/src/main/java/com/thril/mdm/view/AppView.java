package com.thril.mdm.view;

import android.location.Location;
import android.net.wifi.ScanResult;

import com.thril.mdm.model.NotificationData;
import com.thril.mdm.model.ResponseData;
import com.thril.mdm.model.StaffInfo;

import java.util.List;

/**
 * Created by Ram on 2/4/2018.
 */
//Interface listnere for main atcivity
public interface AppView {
    void onAppRegistrationSuccess(ResponseData responseData);
    void onAppRegistrationFailure(String error);
    void onAction(int position);
    void onNotificationAction(int position);

    void gpsLocation(Location location);
    void gpsServiceStatus(String msg);

    void discoveryList(List<String> discoveryList, boolean flag, List<ScanResult> scanResultList);
    void hotSpotName(String hotSpotName,String password);
    void disableHotspot(boolean flag);
    void hopSpotConnectionStatus(boolean flag);

    void fileList(List<String> llist);
    void currentGpsLocation(Location location);

    void socketStatus(String socketStatus,boolean socketFlag);
    void updateProgress(int percentage);
    void fileStatus(boolean flag);

    void onAssetValidationSuccess(StaffInfo staffInfo);
    void onAssetValidationFailure(String error);
    void connectedWifi(String ssid);
    void notificationCount(int count);
    void notificationList(List<NotificationData> notificationDataList);
    void notificationDelete(int position);
    void onDialogUpdateApk(String appLink);
    void showExitDialog();

    void exitApp();
    void onOTPSendingFailure(String error);

    void onOTPVerifyFailure(String error);
}
