package com.thril.mdm.view.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.thril.mdm.Global.MDMSettings;
import com.thril.mdm.Global.PhoneContact;
import com.thril.mdm.Network.utils.Constants;
import com.thril.mdm.R;
import com.thril.mdm.kiosk.MainActivity;

import java.util.regex.Pattern;
/**
 * Created by Ram on 6/13/2018.
 */

//Fragment class to save new contact to phone
public class NewContact extends Fragment {
    EditText et_name, et_mobile;
    Button btn_save;
    FloatingActionButton floatingActionButton;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.new_contact_fragment, container, false);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

            et_name.setError(null);
            et_mobile.setError(null);
        }else{
            if(et_name!=null){
                et_name.setError(null);
            }

            if(et_mobile!=null){
                et_mobile.setError(null);
            }

        }

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        et_name = (EditText) view.findViewById(R.id.input_name);
        et_mobile = (EditText) view.findViewById(R.id.input_mobile);
        btn_save = (Button) view.findViewById(R.id.btn_save);
        floatingActionButton = (FloatingActionButton) view.findViewById(R.id.fab_button);
        et_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (et_name.getText().toString().length() > 0){
                    et_name.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        et_mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (et_mobile.getText().toString().length() > 0){
                    et_mobile.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (et_name.getText().toString().trim().length() == 0) {
                    et_name.setError("Please enter name");
                    et_name.requestFocus();
                    et_name.requestFocusFromTouch();
                } else if (et_mobile.getText().toString().length() == 0) {
                    et_mobile.setError("Please enter mobile number");
                    et_mobile.requestFocus();
                    et_mobile.requestFocusFromTouch();
                } else if (MDMSettings.getcheckPhoneno(et_mobile.getText().toString())) {
                    et_mobile.setError("Please enter valid mobile number");
                    et_mobile.requestFocus();
                    et_mobile.requestFocusFromTouch();
                } else {
                    PhoneContact.addContacts(et_name.getText().toString().toUpperCase(), et_mobile.getText().toString(), getActivity());


                    et_name.setText("");
                    et_mobile.setText("");
                    Toast.makeText(getActivity(), "Contact has been saved..", Toast.LENGTH_SHORT).show();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(et_name.getWindowToken(), 0);
                }

            }
        });

    }


}
